package com.joinbesocial.candc;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.joinbesocial.candc.activities.MainNavActivity;

import net.danlew.android.joda.JodaTimeAndroid;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class CandCApplication extends Application implements Application.ActivityLifecycleCallbacks {

    private final static String TAG = CandCApplication.class.getSimpleName();

    private static Context mAppContext;
    private static boolean mActivityVisible;
    private MainNavActivity mMainNavActivity = null;

    public static boolean isActivityVisible() {
        return mActivityVisible;
    }

    public static Context getAppContext() {
        return mAppContext;
    }

    public void setMainNavActivity(MainNavActivity mainNavActivity) {
        this.mMainNavActivity = mainNavActivity;
    }
    public MainNavActivity getMainNavActivity() {
        return mMainNavActivity;
    }

    @Override
    public void onCreate() {

        super.onCreate();

        mAppContext = this;

        registerActivityLifecycleCallbacks(this);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/OpenSans-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        // PlayfairDisplay-Bold.ttf
        // Montserrat-Bold.otf
        // OpenSans-Regular.ttf

        JodaTimeAndroid.init(this);
        Cloud.getInstance().init();
        CloudAnalytics.getInstance().init();
        Utility.getInstance().init();

        if(com.joinbesocial.candc.BuildConfig.DEBUG) {
            // Don't do this in production! This is just so cold launches take some
            // time and we can see the splash screen.
// TEST ONLY
            //SystemClock.sleep(TimeUnit.SECONDS.toMillis(3));
        }
    }

    public void onActivityCreated(Activity var1, Bundle var2) {}
    public void onActivityStarted(Activity var1) {}

    public void onActivityResumed(Activity var1) {
        Log.d(TAG, "onActivityResumed");
        mActivityVisible = true;
    }

    public void onActivityPaused(Activity var1) {
        Log.d(TAG, "onActivityPaused");
        mActivityVisible = false;
    }

    public void onActivityStopped(Activity var1) {}
    public void onActivitySaveInstanceState(Activity var1, Bundle var2) {}
    public void onActivityDestroyed(Activity var1) {}
}