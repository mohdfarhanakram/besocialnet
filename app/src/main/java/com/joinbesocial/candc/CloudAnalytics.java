package com.joinbesocial.candc;

import android.util.Base64;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.Seconds;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class CloudAnalytics {

    private final static String TAG = CloudAnalytics.class.getSimpleName();

    private String MIXPANEL_PROJECT_TOKEN = "";
    private String MIXPANEL_SECRET_TOKEN = "";
    private final static String MIXPANEL_JQL_URL = "https://mixpanel.com/api/2.0/jql";

    private MixpanelAPI mMixpanel;
    private DateTimeFormatter mDateTimeFormat = DateTimeFormat.forPattern("yyyy-MM-dd"); // Mixpanel wants dates to look like this!

    private static class Holder {
        static final CloudAnalytics INSTANCE = new CloudAnalytics();
    }

    public static CloudAnalytics getInstance() {
        return Holder.INSTANCE;
    }

    public interface ResponseAnalyticsCallback {
        void onResponse(ReportsHolder reportsHolder);
    }

    public interface ReportsResponseCallback {
        void onResponse(Reports reports);
    }

    public interface JQLCallback {
        void onResponse(String jsonString);
    }

    public interface FaultCallback {
        void onFault();
    }

    public void init() {

        if(com.joinbesocial.candc.BuildConfig.DEBUG) {
            Log.d(TAG, "DEBUG");
            // Project: CandC_develop
            MIXPANEL_PROJECT_TOKEN = "a7945b80f7410925f1de05390f224c81";
            MIXPANEL_SECRET_TOKEN = "f869abc6923d98e7efc9a978a387237c";
        } else {
            Log.d(TAG, "RELEASE");
            // Project: CandC
            MIXPANEL_PROJECT_TOKEN = "dc0f7074ae58396f2d4f7d77edcdddc5";
            MIXPANEL_SECRET_TOKEN = "f7ff907ac7aa838096226289f929cf4c";
        }

        mMixpanel = MixpanelAPI.getInstance(CandCApplication.getAppContext(), MIXPANEL_PROJECT_TOKEN);
    }

    public void registerVenueNameSuperProperty(String venueCode) {

        try {
            JSONObject props = new JSONObject();
            props.put("venue_name", venueCode);
            mMixpanel.registerSuperProperties(props);
        } catch(JSONException e) {
            Log.e(TAG, "Unable to add properties to JSONObject for analytics.", e);
        }
    }

    public void unregisterVenueNameSuperProperty() {

        mMixpanel.unregisterSuperProperty("venue_name");
    }

    /**
     * Event: sign_up
     * Properties: method (email, facebook, twitter)
     */
    public void sendEventSignUp(String method) {

        try {
            JSONObject props = new JSONObject();
            props.put("method", method);
            mMixpanel.track("sign_up", props);
        } catch(JSONException e) {
            Log.e(TAG, "Unable to add properties to JSONObject for analytics.", e);
        }
    }

    /**
     * Event: app_open
     * Properties: venue_name (Super Property, e.g. "NTXIOT")
     */
    public void sendEventAppOpen() {

        mMixpanel.track("app_open");
    }

    /**
     * Event: venue_selected
     * Properties: method (search, find_by_id)
     * venue_name (e.g. "NTXIOT")
     */
    public void sendEventSelectedEvent(String method, String venueCode) {

        try {
            registerVenueNameSuperProperty(venueCode);

            JSONObject props = new JSONObject();
            props.put("method", method);
            props.put("venue_name", venueCode);
            mMixpanel.track("venue_selected", props);
        } catch(JSONException e) {
            Log.e(TAG, "Unable to add properties to JSONObject for analytics.", e);
        }
    }

    /**
     * Event: venue_item_selected
     * Properties: venue_item_name (e.g. "Microsoft Cloud")
     * venue_name (Super Property, e.g. "NTXIOT")
     * venue_item_type (e.g. "internal", "external")
     */
    public void sendEventSelectedVenueItem(String venueItemName, String venueItemType) {

        try {
            JSONObject props = new JSONObject();
            props.put("venue_item_name", venueItemName);
            props.put("venue_item_type", venueItemType);
            mMixpanel.track("venue_item_selected", props);
        } catch(JSONException e) {
            Log.e(TAG, "Unable to add properties to JSONObject for analytics.", e);
        }
    }

    /**
     * Event: chat_started
     * Properties: venue_name (Super Property, e.g. "NTXIOT")
     */
    public void sendEventChatStarted() {

        mMixpanel.track("chat_started");
    }

    /**
     * Event: link_viewed
     * Properties: url (e.g. "https://cloud.microsoft.com")
     * page (venue, venue_item)
     * venue_name (Super Property, e.g. "NTXIOT")
     * venue_item_name OPTIONAL (e.g. "Microsoft Cloud")
     * venue_item_type OPTIONAL (e.g. "internal", "external")
     */
    public void sendEventLinkViewed(String url, String page, String venueItemName, String venueItemType) {

        try {
            JSONObject props = new JSONObject();
            props.put("url", url);
            props.put("page", page);

            if(venueItemName != null) {
                props.put("venue_item_name", venueItemName);
            }

            if(venueItemType != null) {
                props.put("venue_item_type", venueItemType);
            }

            mMixpanel.track("link_viewed", props);
        } catch(JSONException e) {
            Log.e(TAG, "Unable to add properties to JSONObject for analytics.", e);
        }
    }

    /**
     * Event: geofence_enter
     * Properties: geofence_name (e.g. "NTXIOT", "Microsoft Booth")
     * venue_name (Super Property, e.g. "NTXIOT")
     */
    public void sendEventEnterGeofence(String geofenceName) {

        try {
            JSONObject props = new JSONObject();
            props.put("geofence_name", geofenceName);
            mMixpanel.track("geofence_enter", props);
        } catch(JSONException e) {
            Log.e(TAG, "Unable to add properties to JSONObject for analytics.", e);
        }
    }

    /**
     * Event: geofence_exit
     * Properties: geofence_name (e.g. "NTXIOT", "Microsoft Booth")
     * venue_name (Super Property, e.g. "NTXIOT")
     * length_of_stay (in seconds)
     */
    public void sendEventExitGeofence(String geofenceName, int lengthOfStay) {

        try {
            JSONObject props = new JSONObject();
            props.put("geofence_name", geofenceName);
            props.put("length_of_stay", lengthOfStay);
            mMixpanel.track("geofence_exit", props);
        } catch(JSONException e) {
            Log.e(TAG, "Unable to add properties to JSONObject for analytics.", e);
        }
    }

    //        # This uses a test project API secret, replace ce08d087255d5ceec741819a57174ce5 with your own API secret
    //        curl https://mixpanel.com/api/2.0/jql \
    //        -u ce08d087255d5ceec741819a57174ce5: \
    //        --data-urlencode params='{"from_date":"2016-01-01", "to_date": "2016-01-07"}' \
    //        --data-urlencode script='function main(){ return Events(params).groupBy(["name"], mixpanel.reducer.count()) }'
    //
    //        let parameters: Parameters = [
    //            "params": "{\"from_date\":\"2017-03-05\", \"to_date\": \"2017-03-10\"}",
    //            "script": "function main(){ return Events(params).groupBy([\"name\"], mixpanel.reducer.count()) }"
    //        ]

    private void requestJQLReport(final String jsScript, final JQLCallback jqlCallback, final FaultCallback faultCallback) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, MIXPANEL_JQL_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //Log.e("XXX", "response = " + response);
                jqlCallback.onResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("XXX", "error = " + error.toString());
                faultCallback.onFault();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {

                Map<String, String> params = new HashMap<>();
                params.put("script", jsScript);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");

                String credentials = MIXPANEL_SECRET_TOKEN + ":" + "";
                String encodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Authorization", "Basic " + encodedCredentials);

                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(CandCApplication.getAppContext());
        requestQueue.add(stringRequest);
    }

    public void collectReports(final ReportsResponseCallback reportsResponseCallback) {

        ReportsHolder reportsHolder = new ReportsHolder(new Reports());

        loadSimpleCountReports(reportsHolder, new ResponseAnalyticsCallback() {
            @Override
            public void onResponse(ReportsHolder reportsHolder) {

                loadTopVenueInternalReports(reportsHolder, new ResponseAnalyticsCallback() {
                    @Override
                    public void onResponse(ReportsHolder reportsHolder) {

                        loadTopVenueExternalReports(reportsHolder, new ResponseAnalyticsCallback() {
                            @Override
                            public void onResponse(ReportsHolder reportsHolder) {

                                loadExits(reportsHolder, new ResponseAnalyticsCallback() {
                                    @Override
                                    public void onResponse(ReportsHolder reportsHolder) {

                                        loadAvgUserVisits(reportsHolder, new ResponseAnalyticsCallback() {
                                            @Override
                                            public void onResponse(ReportsHolder reportsHolder) {

                                                reportsResponseCallback.onResponse(reportsHolder.getReports());
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }

    public void loadSimpleCountReports(final ReportsHolder reportsHolder, final ResponseAnalyticsCallback responseAnalyticsCallback) {

        String fileString = Utility.getInstance().readFileFromAssets("jql/simple_count.js");

        if(fileString != null && !fileString.isEmpty()) {

            DateTime todaysDate = new DateTime();
            DateTime lastWeeksDate = todaysDate.minusDays(7);

            String todaysDateString = mDateTimeFormat.print(todaysDate);
            String lastWeeksDateString = mDateTimeFormat.print(lastWeeksDate);

            String venueCode = Utility.getInstance().getVenueCode();

            String temp1 = fileString.replace("FROM_DATE", lastWeeksDateString);
            String temp2 = temp1.replace("TO_DATE", todaysDateString);
            final String jsStringWithDates = temp2.replace("VENUE_NAME", venueCode);

            //Log.e("XXX", "collectReports: fileString = " + jsStringWithDates);

            requestJQLReport(jsStringWithDates, new JQLCallback() {
                @Override
                public void onResponse(String jsonString) {

                    //Log.e("XXX", "onResponse: " + jsonString);

                    /*
                    [
                        {
                            "key": [
                                "chat_started"
                            ],
                            "value": 5
                        },
                        {
                            "key": [
                                "venue_selected"
                            ],
                            "value": 46
                        }
                    ]
                    */

                    JsonParser parser = new JsonParser();
                    JsonArray rootArray = parser.parse(jsonString).getAsJsonArray();

                    for(JsonElement element : rootArray) {

                        JsonObject jsonObject = element.getAsJsonObject();

                        //Log.e("XXX", "extraObj: " + extraObj.toString());

                        if(!jsonObject.get("key").getAsJsonArray().get(0).isJsonNull()) {

                            if(jsonObject.get("key").getAsJsonArray().get(0).getAsString().equals("venue_selected")) {
                                reportsHolder.getReports().activeUsers = jsonObject.get("value").getAsInt();
                            } else if(jsonObject.get("key").getAsJsonArray().get(0).getAsString().equals("chat_started")) {
                                reportsHolder.getReports().activeChats = jsonObject.get("value").getAsInt();
                            }
                        }
                    }

                    responseAnalyticsCallback.onResponse(reportsHolder);
                }
            }, new FaultCallback() {
                @Override
                public void onFault() {
                    responseAnalyticsCallback.onResponse(reportsHolder);
                }
            });

        } else {
            responseAnalyticsCallback.onResponse(reportsHolder);
        }
    }

    public void loadTopVenueInternalReports(final ReportsHolder reportsHolder, final ResponseAnalyticsCallback responseAnalyticsCallback) {

        String fileString = Utility.getInstance().readFileFromAssets("jql/venue_items_internal.js");

        if(fileString != null && !fileString.isEmpty()) {

            DateTime todaysDate = new DateTime();
            DateTime lastWeeksDate = todaysDate.minusDays(7);

            String todaysDateString = mDateTimeFormat.print(todaysDate);
            String lastWeeksDateString = mDateTimeFormat.print(lastWeeksDate);

            String venueCode = Utility.getInstance().getVenueCode();

            String temp1 = fileString.replace("FROM_DATE", lastWeeksDateString);
            String temp2 = temp1.replace("TO_DATE", todaysDateString);
            final String jsStringWithDates = temp2.replace("VENUE_NAME", venueCode);

            //Log.e("XXX", "collectReports: fileString = " + jsStringWithDates);

            requestJQLReport(jsStringWithDates, new JQLCallback() {
                @Override
                public void onResponse(String jsonString) {

                    //Log.e("XXX", "onResponse: " + jsonString);

                    /*
                    [
                        {
                            "key": [
                            "ThingLogix"
                            ],
                            "value": 8
                        },
                        {
                            "key": [
                            "Microsoft Cloud"
                            ],
                            "value": 6
                        }
                    ]
                    */

                    JsonParser parser = new JsonParser();
                    JsonArray rootArray = parser.parse(jsonString).getAsJsonArray();

                    for(JsonElement element : rootArray) {

                        JsonObject jsonObject = element.getAsJsonObject();

                        //Log.e("XXX", "jsonObject: " + jsonObject.toString());

                        if(jsonObject.get("key").getAsJsonArray().get(0).isJsonNull()) {
                            responseAnalyticsCallback.onResponse(reportsHolder); // Just return what we have so far.
                            return;
                        }

                        reportsHolder.getReports().top5VenueItems.add(jsonObject.get("key").getAsJsonArray().get(0).getAsString());

                        if(reportsHolder.getReports().top5VenueItems.size() >= 5){
                            break;
                        }

                    }

                    responseAnalyticsCallback.onResponse(reportsHolder);
                }
            }, new FaultCallback() {
                @Override
                public void onFault() {
                    responseAnalyticsCallback.onResponse(reportsHolder);
                }
            });

        } else {
            responseAnalyticsCallback.onResponse(reportsHolder);
        }
    }

    public void loadTopVenueExternalReports(final ReportsHolder reportsHolder, final ResponseAnalyticsCallback responseAnalyticsCallback) {

        String fileString = Utility.getInstance().readFileFromAssets("jql/venue_items_external.js");

        if(fileString != null && !fileString.isEmpty()) {

            DateTime todaysDate = new DateTime();
            DateTime lastWeeksDate = todaysDate.minusDays(7);

            String todaysDateString = mDateTimeFormat.print(todaysDate);
            String lastWeeksDateString = mDateTimeFormat.print(lastWeeksDate);

            String venueCode = Utility.getInstance().getVenueCode();

            String temp1 = fileString.replace("FROM_DATE", lastWeeksDateString);
            String temp2 = temp1.replace("TO_DATE", todaysDateString);
            final String jsStringWithDates = temp2.replace("VENUE_NAME", venueCode);

            //Log.e("XXX", "collectReports: fileString = " + jsStringWithDates);

            requestJQLReport(jsStringWithDates, new JQLCallback() {
                @Override
                public void onResponse(String jsonString) {

                    //Log.e("XXX", "onResponse: " + jsonString);

                    /*
                    [
                        {
                            "key": [
                            "Chilli's"
                            ],
                            "value": 8
                        },
                        {
                            "key": [
                            "Logan's"
                            ],
                            "value": 6
                        }
                    ]
                    */

                    JsonParser parser = new JsonParser();
                    JsonArray rootArray = parser.parse(jsonString).getAsJsonArray();

                    for(JsonElement element : rootArray) {

                        JsonObject jsonObject = element.getAsJsonObject();

                        //Log.e("XXX", "jsonObject: " + jsonObject.toString());

                        if(jsonObject.get("key").getAsJsonArray().get(0).isJsonNull()) {
                            responseAnalyticsCallback.onResponse(reportsHolder); // Just return what we have so far.
                            return;
                        }

                        reportsHolder.getReports().top5OutSideVenueItems.add(jsonObject.get("key").getAsJsonArray().get(0).getAsString());

                        if(reportsHolder.getReports().top5OutSideVenueItems.size() >= 5){
                            break;
                        }

                    }

                    responseAnalyticsCallback.onResponse(reportsHolder);
                }
            }, new FaultCallback() {
                @Override
                public void onFault() {
                    responseAnalyticsCallback.onResponse(reportsHolder);
                }
            });

        } else {
            responseAnalyticsCallback.onResponse(reportsHolder);
        }
    }

    public void loadExits(final ReportsHolder reportsHolder, final ResponseAnalyticsCallback responseAnalyticsCallback) {

        String fileString = Utility.getInstance().readFileFromAssets("jql/exits.js");

        if(fileString != null && !fileString.isEmpty()) {

            DateTime todaysDate = new DateTime();
            DateTime anHourAgoDate = todaysDate.minusHours(1);

            String todaysDateString = mDateTimeFormat.print(todaysDate);
            String anHourAgoDateString = mDateTimeFormat.print(anHourAgoDate);

            String venueCode = Utility.getInstance().getVenueCode();

            String temp1 = fileString.replace("FROM_DATE", anHourAgoDateString);
            String temp2 = temp1.replace("TO_DATE", todaysDateString);
            final String jsStringWithDates = temp2.replace("VENUE_NAME", venueCode);

            //Log.e("XXX", "collectReports: fileString = " + jsStringWithDates);

            requestJQLReport(jsStringWithDates, new JQLCallback() {
                @Override
                public void onResponse(String jsonString) {

                    //Log.e("XXX", "onResponse: " + jsonString);

                    /*
                    [
                        {
                            "key" : [
                            "geofence_exit"
                            ],
                            "value" : 13
                        }
                    ]
                    */

                    JsonParser parser = new JsonParser();
                    JsonArray rootArray = parser.parse(jsonString).getAsJsonArray();

                    for(JsonElement element : rootArray) {

                        JsonObject jsonObject = element.getAsJsonObject();

                        if(!jsonObject.get("key").getAsJsonArray().get(0).isJsonNull()) {

                            if(jsonObject.get("key").getAsJsonArray().get(0).getAsString().equals("geofence_exit")) {
                                reportsHolder.getReports().exitsPrevHour = jsonObject.get("value").getAsInt();
                            }
                        }
                    }

                    responseAnalyticsCallback.onResponse(reportsHolder);
                }
            }, new FaultCallback() {
                @Override
                public void onFault() {
                    responseAnalyticsCallback.onResponse(reportsHolder);
                }
            });

        } else {
            responseAnalyticsCallback.onResponse(reportsHolder);
        }
    }

    public void loadAvgUserVisits(final ReportsHolder reportsHolder, final ResponseAnalyticsCallback responseAnalyticsCallback) {

        String fileString = Utility.getInstance().readFileFromAssets("jql/avg_user_visit.js");

        if(fileString != null && !fileString.isEmpty()) {

            DateTime todaysDate = new DateTime();
            DateTime lastWeeksDate = todaysDate.minusDays(7);

            String todaysDateString = mDateTimeFormat.print(todaysDate);
            String lastWeeksDateString = mDateTimeFormat.print(lastWeeksDate);

            String venueCode = Utility.getInstance().getVenueCode();

            String temp1 = fileString.replace("FROM_DATE", lastWeeksDateString);
            String temp2 = temp1.replace("TO_DATE", todaysDateString);
            final String jsStringWithDates = temp2.replace("GEOFENCE_NAME", venueCode);

            //Log.e("XXX", "collectReports: fileString = " + jsStringWithDates);

            requestJQLReport(jsStringWithDates, new JQLCallback() {
                @Override
                public void onResponse(String jsonString) {

                    //Log.e("XXX", "onResponse: " + jsonString);

                    /*
                    [
                        265.333333
                    ]
                    */

                    reportsHolder.getReports().avgUserVisits = "?h ?min";

                    JsonParser parser = new JsonParser();
                    JsonArray rootArray = parser.parse(jsonString).getAsJsonArray();

                    if(rootArray.size() > 0 ) {

                        //Log.e("XXX", "avgUserVisits: " + rootArray.get(0).getAsInt());

                        Seconds elapsedSeconds = Seconds.seconds(rootArray.get(0).getAsInt());
                        Period period = new Period(elapsedSeconds);

                        PeriodFormatter dhm = new PeriodFormatterBuilder()
                                .appendHours()
                                .appendSuffix("h", "h")
                                .appendSeparator(" ")
                                .appendMinutes()
                                .appendSuffix("min", "min")
                                .toFormatter();

                        reportsHolder.getReports().avgUserVisits = dhm.print(period.normalizedStandard());
                    }

                    responseAnalyticsCallback.onResponse(reportsHolder);
                }
            }, new FaultCallback() {
                @Override
                public void onFault() {
                    responseAnalyticsCallback.onResponse(reportsHolder);
                }
            });

        } else {
            responseAnalyticsCallback.onResponse(reportsHolder);
        }
    }
}