package com.joinbesocial.candc;

// The ReportsHolder class is designed to remain immutable while keeping the internal Reports object
// mutable during the process of being passed into a series of Anonymous Classes.
// http://eclipsesource.com/blogs/2013/08/19/mutable-variable-capture-in-anonymous-java-classes/
public class ReportsHolder {

    private Reports mReports;

    ReportsHolder(Reports reports) {
        setReports(reports);
    }

    public Reports getReports() {
        return mReports;
    }

    public void setReports(Reports reports) {
        this.mReports = reports;
    }
}
