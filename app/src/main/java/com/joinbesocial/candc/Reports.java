package com.joinbesocial.candc;

import java.util.ArrayList;

public class Reports {

    public int activeUsers = 0;
    public int activeChats = 0;

    public String avgUserVisits; // Example: "1h 5min"
    public int exitsPrevHour = 0;

    public ArrayList<String> top5VenueItems = new ArrayList<>();
    public ArrayList<String> top5OutSideVenueItems = new ArrayList<>();
}
