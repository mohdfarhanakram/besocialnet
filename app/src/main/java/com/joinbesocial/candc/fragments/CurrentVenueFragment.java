package com.joinbesocial.candc.fragments;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.Target;
import com.joinbesocial.candc.CandCApplication;
import com.joinbesocial.candc.Cloud;
import com.joinbesocial.candc.CloudAnalytics;
import com.joinbesocial.candc.R;
import com.joinbesocial.candc.Utility;
import com.joinbesocial.candc.models.Venue;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import uk.co.chrisjenx.calligraphy.TypefaceUtils;

public class CurrentVenueFragment extends BaseFragment {

    private final static String TAG = CurrentVenueFragment.class.getSimpleName();

    private static final int RC_LOCATION_PERM = 124;

    private Venue mVenue;

    private ImageView mLogoImage;
    private TextView mTitleText;
    private TextView mSubTitleText;
    private TextView mDescText;
    private TextView mAddressText;
    private ImageView mPhoneImage;
    private ImageView mEmailImage;
    private ImageView mWebUrlImage;
    private String mPhoneText;
    private String mEmailText;
    private String mWebUrlText;
    private LinearLayout mExtrasLayout;
    private Button mVenueItemsBtn;
    private ScrollView mScrollView;

    public static CurrentVenueFragment newInstance(int instance) {

        Bundle args = new Bundle();
        args.putInt(ARGS_INSTANCE, instance);
        CurrentVenueFragment fragment = new CurrentVenueFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if(mVenue != null) {
            getNavigationHostActivity().setNavTitle(mVenue.getTitle());
        } else {
            getNavigationHostActivity().setNavTitle("");
        }

        View rootView = inflater.inflate(R.layout.fragment_current_venue, container, false);

        mLogoImage = (ImageView) rootView.findViewById(R.id.iv_curvenue_logo);
        mTitleText = (TextView) rootView.findViewById(R.id.tv_curvenue_title);
        mSubTitleText = (TextView) rootView.findViewById(R.id.tv_curvenue_subtitle);
        mDescText = (TextView) rootView.findViewById(R.id.tv_curvenue_desc);
        mAddressText = (TextView) rootView.findViewById(R.id.tv_curvenue_address);
        mPhoneImage = (ImageView) rootView.findViewById(R.id.iv_curvenue_phone);
        mEmailImage = (ImageView) rootView.findViewById(R.id.iv_curvenue_email);
        mWebUrlImage = (ImageView) rootView.findViewById(R.id.iv_curvenue_weburl);
        mExtrasLayout = (LinearLayout) rootView.findViewById(R.id.ll_curvenue_extras);
        mScrollView = (ScrollView) rootView.findViewById(R.id.sv_curvenue);

        mScrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {

                if(mScrollView != null) {

                    // If we hit the end of the scoll view - remove the badge count for the venue message.
                    if(mScrollView.getChildAt(0).getBottom() <= (mScrollView.getHeight() + mScrollView.getScrollY())) {

                        if(!Utility.getInstance().isLatestVenueMessageViewed()) {

                            Utility.getInstance().setLatestVenueMessageViewed(true);

                            int newVenuesBadgeCount = Utility.getInstance().getVenuesBadgeCount() - 1;

                            if(newVenuesBadgeCount > 0) {
                                getNavigationHostActivity().setBadgeCountForTabAtPosition(newVenuesBadgeCount, 0);
                            } else {
                                getNavigationHostActivity().setBadgeCountForTabAtPosition(0, 0);
                            }

                            Utility.getInstance().setVenuesBadgeCount(newVenuesBadgeCount);
                        }
                    }
                }
            }
        });

        mAddressText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MapFragment fragment = new MapFragment();
                fragment.setVenue(mVenue);
                getNavigationHostActivity().pushFragment(fragment);
            }
        });

        mPhoneImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(mPhoneText == null || mPhoneText.isEmpty()) {
                    Utility.getInstance().showOkDialog(getContext(), "Phone Number", "I'm sorry, but no phone number has been provided for " + mTitleText.getText() + " yet.");
                    return;
                }

                Utility.getInstance().showOkCancelDialog(getContext(), "Place Call",
                        "Do you wish to call " + mPhoneText + " now?",
                        new Utility.OkCallback() {
                            @Override
                            public void onOk() {

                                String phoneNumbersOnly = mPhoneText.replaceAll("[^0-9]", "");

                                Uri number = Uri.parse("tel:" + phoneNumbersOnly);
                                Intent dialIntent = new Intent(Intent.ACTION_DIAL, number);
                                startActivity(dialIntent);
                            }
                        });
            }
        });

        mEmailImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(mEmailText == null || mEmailText.isEmpty()) {
                    Utility.getInstance().showOkDialog(getContext(), "Email", "I'm sorry, but no email address has been provided for " + mTitleText.getText() + " yet.");
                    return;
                }

                Intent sendEmailIntent = new Intent(Intent.ACTION_SEND);
                sendEmailIntent.setType("message/rfc822");
                sendEmailIntent.putExtra(Intent.EXTRA_EMAIL  , new String[]{mEmailText});

                try {
                    startActivity(Intent.createChooser(sendEmailIntent, "Send Email..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Utility.getInstance().showErrorDialog(getContext(), "Email Failure", "There are no email clients installed.");
                }
            }
        });

        mWebUrlImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(mWebUrlText == null || mWebUrlText.isEmpty()) {
                    Utility.getInstance().showOkDialog(getContext(), "Web Address", "I'm sorry, but no website has been provided for " + mTitleText.getText() + " yet.");
                    return;
                }

                CloudAnalytics.getInstance().sendEventLinkViewed(mWebUrlText, "venue", null, null);

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(mWebUrlText));
                startActivity(browserIntent);
            }
        });

        mVenueItemsBtn = (Button) rootView.findViewById(R.id.btn_curvenue_venitems);

        if(mVenue != null) {
            mVenueItemsBtn.setText(mVenue.getVenueItemName());
        } else {
            mVenueItemsBtn.setText("");
        }

        mVenueItemsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                VenueItemsFragment fragment = new VenueItemsFragment();
                fragment.setVenueItemName(mVenue.getVenueItemName());
                getNavigationHostActivity().pushFragment(fragment);
            }
        });

        Button leaveEvent = (Button) rootView.findViewById(R.id.btn_curvenue_leave);
        leaveEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Utility.getInstance().showOkCancelDialog(CurrentVenueFragment.this.getContext(), "Leave Venue",
                        "Are you sure you wish to leave this venue?",
                        new Utility.OkCallback() {
                            @Override
                            public void onOk() {

                                Utility.getInstance().showLoadSpinner(CurrentVenueFragment.this.getContext(), "Leaving Venue");

                                Cloud.getInstance().removeVenueCodeFromUser(new Cloud.ResponseCallback() {
                                    @Override
                                    public void onResponse() {

                                        Utility.getInstance().leaveCurrentEvent();

                                        getNavigationHostActivity().setNavTitle("");
                                        mTitleText.setText("");
                                        mSubTitleText.setText("");
                                        mDescText.setText("");
                                        mAddressText.setText("");

                                        if(mExtrasLayout.getChildCount() > 0) {
                                            mExtrasLayout.removeAllViews();
                                            mExtrasLayout.setVisibility(View.GONE);
                                        }

                                        VenueFragment fragment = new VenueFragment();
                                        getNavigationHostActivity().replaceFragment(fragment);

                                        Utility.getInstance().hideLoadSpinner();
                                    }
                                }, new Cloud.FaultCallback() {
                                    @Override
                                    public void onFault(String code, String message) {
                                        Utility.getInstance().hideLoadSpinner();
                                    }
                                });
                            }
                        });
            }
        });

        loadVenue();

        promptForPermissions();

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        Cloud.getInstance().registerDeviceForPush();
    }

    private void loadVenue() {

        if(mExtrasLayout.getChildCount() > 0) {
            mExtrasLayout.removeAllViews();
            mExtrasLayout.setVisibility(View.GONE);
        }

        if(Utility.getInstance().getVenueObjectId() != null) {

            Utility.getInstance().showLoadSpinner(CurrentVenueFragment.this.getContext(), "Loading Venue");

            Cloud.getInstance().findVenueByObjectId(Utility.getInstance().getVenueObjectId(), new Cloud.VenueResponseCallback() {

                @Override
                public void onData(Venue venue) {

                    Utility.getInstance().hideLoadSpinner();

                    mVenue = venue;

                    getNavigationHostActivity().setNavTitle(venue.getTitle());

                    mTitleText.setText(venue.getTitle());
                    mSubTitleText.setText(venue.getSubTitle());
                    mDescText.setText(venue.getDesc().replace("\\n", System.getProperty("line.separator")));
                    mAddressText.setText(venue.getAddress().replace("\\n", System.getProperty("line.separator")));
                    mPhoneText = venue.getPhone();
                    mEmailText = venue.getEmail();
                    mWebUrlText = venue.getWebUrl();

                    mVenueItemsBtn.setText(mVenue.getVenueItemName());

                    // If we have extras to load or a venue-wide message, observe the layout of
                    // the button above our extras layout for its onPreDraw. Once we see the
                    // onPreDraw we will know exactly how wide the button is and we can force
                    // the extras layout to be the same width.
                    ViewTreeObserver vto = mVenueItemsBtn.getViewTreeObserver();
                    vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {

                        public boolean onPreDraw() {

                            mVenueItemsBtn.getViewTreeObserver().removeOnPreDrawListener(this);

                            final int widthOfVenueItemBtn = mVenueItemsBtn.getMeasuredWidth();

                            if(mVenue.getLogoUrl() != null) {

                                Utility.getInstance().showLoadSpinner(getContext(), "Loading");

                                Glide.with(getContext())
                                        .load(mVenue.getLogoUrl())
                                        .asBitmap()
                                        .placeholder(Utility.getInstance().getVenuePlaceHolderImage())
                                        .fitCenter()
                                        .listener(new RequestListener<String, Bitmap>() {
                                            @Override
                                            public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                                Utility.getInstance().hideLoadSpinner();
                                                return false;
                                            }
                                            @Override
                                            public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                                return false;
                                            }
                                        }).into(new BitmapImageViewTarget(mLogoImage) {
                                            @Override
                                            protected void setResource(Bitmap resource) {
                                                // Force the image to match the width of the buttons.
                                                Bitmap bitmap = Utility.getInstance().resizeBitmapToWidth(resource, widthOfVenueItemBtn);
                                                Drawable drawable = new BitmapDrawable(getResources(), bitmap);
                                                mLogoImage.setImageDrawable(drawable);
                                                Utility.getInstance().hideLoadSpinner();
                                            }
                                        });
                            }

                            if(mVenue.getExtras() != null) {
                                Utility.getInstance().addExtrasToLayout(getContext(), mExtrasLayout, widthOfVenueItemBtn, mVenue.getExtras(), "venue", null, null);
                            }

                            addVenueMessage(widthOfVenueItemBtn);

                            return true;
                        }
                    });

                }
            }, new Cloud.FaultCallback() {
                @Override
                public void onFault(String code, String message) {

                    Utility.getInstance().hideLoadSpinner();
                    Utility.getInstance().showErrorDialog(CurrentVenueFragment.this.getContext(), "Loading Error", message);
                }
            });
        }
    }

    public void reloadVenue() {

        loadVenue();
    }

    public void addVenueMessage(int width) {

        if(Utility.getInstance().getLatestVenueMessage() != null) {

            //
            // First, add notice icon and text "A special message from the host:"
            //

            LinearLayout linearLayout = new LinearLayout(getContext());
            linearLayout.setOrientation(LinearLayout.HORIZONTAL);
            linearLayout.setBackgroundColor(getContext().getResources().getColor(R.color.white));
            LinearLayout.LayoutParams layoutParams =
                    new LinearLayout.LayoutParams(width, LinearLayout.LayoutParams.WRAP_CONTENT);
            linearLayout.setLayoutParams(layoutParams);
            layoutParams.setMargins(0, 16, 0, 0);
            linearLayout.setPadding(8, 8, 8, 8);

            ImageView venueNoticeImageView = new ImageView(getContext());
            venueNoticeImageView.setImageResource(R.drawable.venue_notice);
            LinearLayout.LayoutParams layoutParamsForImage =
                    new LinearLayout.LayoutParams(70, 70);
            venueNoticeImageView.setLayoutParams(layoutParamsForImage);

            TextView hostTextView = new TextView(getContext());
            hostTextView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            Typeface typeFace = TypefaceUtils.load(CandCApplication.getAppContext().getAssets(), "fonts/OpenSans-Regular.otf");
            if(typeFace != null) {
                hostTextView.setTypeface(typeFace);
            }
            hostTextView.setTextSize(17);
            hostTextView.setText("A special message from the host:");
            LinearLayout.LayoutParams layoutParamsText =
                    new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            hostTextView.setLayoutParams(layoutParamsText);

            linearLayout.addView(venueNoticeImageView);
            linearLayout.addView(hostTextView);
            mExtrasLayout.addView(linearLayout);

            //
            // Finally, add the message from the venue host...
            //

            TextView messageTextView = new TextView(getContext());
            messageTextView.setAutoLinkMask(Linkify.ALL);
            messageTextView.setBackgroundColor(getContext().getResources().getColor(R.color.white));
            messageTextView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            messageTextView.setPadding(8, 8, 8, 8);
            if(typeFace != null) {
                messageTextView.setTypeface(typeFace);
            }
            messageTextView.setTextSize(17);
            messageTextView.setText(Utility.getInstance().getLatestVenueMessage());

            LinearLayout.LayoutParams layoutParamsMessageText =
                    new LinearLayout.LayoutParams(width, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParamsMessageText.setMargins(0, 16, 0, 0);

            messageTextView.setLayoutParams(layoutParamsMessageText);

            mExtrasLayout.addView(messageTextView);

            mExtrasLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @AfterPermissionGranted(RC_LOCATION_PERM)
    private void promptForPermissions() {

        String[] perms = { Manifest.permission.ACCESS_FINE_LOCATION};

        if(EasyPermissions.hasPermissions(getContext(), perms)) {
            // We have permission, do it!
            if(Utility.getInstance().getVenueCode() != null) {

                Cloud.getInstance().startGeofenceMonitoring(getContext());
            }

        } else {
            // Ask for permission.
            EasyPermissions.requestPermissions(this, getString(R.string.rationale_location),
                    RC_LOCATION_PERM, perms);
        }
    }
}