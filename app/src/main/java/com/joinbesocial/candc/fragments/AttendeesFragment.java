package com.joinbesocial.candc.fragments;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.backendless.BackendlessUser;
import com.joinbesocial.candc.Cloud;
import com.joinbesocial.candc.EndlessRecyclerViewScrollListener;
import com.joinbesocial.candc.NavigationHostActivity;
import com.joinbesocial.candc.SimpleDividerItemDecoration;
import com.joinbesocial.candc.adapters.AttendeesAdapter;
import com.joinbesocial.candc.R;
import com.joinbesocial.candc.Utility;
import com.joinbesocial.candc.models.VenueItem;

import java.util.ArrayList;

public class AttendeesFragment extends BaseFragment {

    private final static String TAG = AttendeesFragment.class.getSimpleName();

    private VenueItem mVenueItem;

    private SwipeRefreshLayout swipeContainer;

    private RecyclerView mAttendeesRecyclerView;
    private AttendeesAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;

    private int pageOffset = 0;
    private final static int PAGE_SIZE = 6;
    private String searchText = null;

    public static AttendeesFragment newInstance(int instance) {

        Bundle args = new Bundle();
        args.putInt(ARGS_INSTANCE, instance);
        AttendeesFragment fragment = new AttendeesFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getNavigationHostActivity().setNavTitle("Visitors");
        getNavigationHostActivity().setSearchHint("Search Visitors...");

        View rootView = inflater.inflate(R.layout.fragment_attendees, container, false);

        //
        // Setup SwipeRefreshLayout for pull-to-refresh...
        //

        swipeContainer = (SwipeRefreshLayout) rootView.findViewById(R.id.srl_attendees);
        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                resetAndLoadAttendees(false);
            }
        });

        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        //
        // Setup the RecyclerView for our data...
        //

        mAttendeesRecyclerView = (RecyclerView) rootView.findViewById(R.id.rv_attendees);
        mAttendeesRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getContext()));

        // Use a linear layout manager.
        mLayoutManager = new LinearLayoutManager(getActivity());
        mAttendeesRecyclerView.setLayoutManager(mLayoutManager);

        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {

                //Log.e("XXX", "onLoadMore: page = " + String.valueOf(page) + ", totalItemsCount = " + String.valueOf(totalItemsCount));

                loadAttendees(new Cloud.AttendeesResponseCallback() {
                    @Override
                    public void onData(ArrayList<BackendlessUser> attendees) {
                        for(BackendlessUser attendee : attendees) {
                            mAdapter.add(attendee);
                        }
                    }
                }, new Cloud.FaultCallback() {
                    @Override
                    public void onFault(String code, String message) {}
                });

            }
        };

        mAttendeesRecyclerView.addOnScrollListener(endlessRecyclerViewScrollListener);

        mAdapter = new AttendeesAdapter(getContext(), getNavigationHostActivity());
        mAttendeesRecyclerView.setAdapter(mAdapter);

        resetAndLoadAttendees(true);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        getNavigationHostActivity().setOnSearchTextSubmitListener(new NavigationHostActivity.OnSearchTextSubmitListener() {
            @Override
            public void onSearchTextSubmit(String text) {

                searchText = text;
                resetAndLoadAttendees(true);
            }
        });

        getNavigationHostActivity().setOnSearchViewToggleListener(new NavigationHostActivity.OnSearchViewToggleListener() {
            @Override
            public void onSearchViewToggle(boolean toggle) {
                if(toggle) {
                    //Log.e("XXX", "Search View ON");
                } else {
                    //Log.e("XXX", "Search View OFF");
                    searchText = null;
                    resetAndLoadAttendees(true);
                }
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();

        getNavigationHostActivity().removeOnSearchTextSubmitListener();
        getNavigationHostActivity().removeOnSearchViewToggleListener();
        getNavigationHostActivity().closeSearchView();
    }

    private void loadAttendees(final Cloud.AttendeesResponseCallback attendeesResponseCallback, final Cloud.FaultCallback faultCallback) {

        //Log.e("XXX", "loadAttendees: pageOffset = " + String.valueOf(pageOffset));

        String venueItemTitle = null;
        if(mVenueItem != null) {
            venueItemTitle = mVenueItem.getTitle();
        }

        Cloud.getInstance().loadAttendeesByPage(PAGE_SIZE, pageOffset, searchText, venueItemTitle,

                new Cloud.AttendeesPageResponseCallback() {
                    @Override
                    public void onData(ArrayList<BackendlessUser> attendees, int newOffset, int totalObjects) {
                        AttendeesFragment.this.pageOffset = newOffset;
                        attendeesResponseCallback.onData(attendees);
                    }
                },

                new Cloud.FaultCallback() {
                    @Override
                    public void onFault(String code, String message) {
                        faultCallback.onFault(code, message);
                    }
                }
        );
    }

    private void resetAndLoadAttendees(final boolean showSpinner) {

        pageOffset = 0;

        if(showSpinner) {
            Utility.getInstance().showLoadSpinner(this.getContext(), "Loading Visitors");
        }

        loadAttendees(new Cloud.AttendeesResponseCallback() {
            @Override
            public void onData(ArrayList<BackendlessUser> attendees) {

                mAdapter.clear();
                endlessRecyclerViewScrollListener.resetState();
                mAdapter.addAll(attendees);

                if(attendees.size() == 0) {

                    String venueItemTitle = null;
                    if(mVenueItem != null) {
                        venueItemTitle = mVenueItem.getTitle();
                    }

                    if(venueItemTitle != null) {
                        Utility.getInstance().showOkDialog(getContext(), "Chat Representatives",
                                "I'm sorry. There doesn’t appear to be any anyone assigned to \"" + venueItemTitle + "\" just yet.");
                    }
                }

                if(showSpinner) {
                    Utility.getInstance().hideLoadSpinner();
                } else {
                    swipeContainer.setRefreshing(false);
                }
            }
        }, new Cloud.FaultCallback() {
            @Override
            public void onFault(String code, String message) {
                if(showSpinner) {
                    Utility.getInstance().hideLoadSpinner();
                } else {
                    swipeContainer.setRefreshing(false);
                }
            }
        });
    }

    public VenueItem getVenueItem() {
        return mVenueItem;
    }

    public void setVenueItem(VenueItem venueItem) {
        this.mVenueItem = venueItem;
    }
}