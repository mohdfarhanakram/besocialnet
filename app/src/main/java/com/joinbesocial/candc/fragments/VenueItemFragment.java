package com.joinbesocial.candc.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.Target;
import com.joinbesocial.candc.CandCApplication;
import com.joinbesocial.candc.CloudAnalytics;
import com.joinbesocial.candc.R;
import com.joinbesocial.candc.Utility;
import com.joinbesocial.candc.models.VenueItem;

import uk.co.chrisjenx.calligraphy.TypefaceUtils;

public class VenueItemFragment extends BaseFragment {

    private final static String TAG = VenueItemFragment.class.getSimpleName();

    private VenueItem mVenueItem = null;

    private ImageView mLogoImage;
    private TextView mTitleText;
    private TextView mSubTitleText;
    private TextView mDescText;
    private TextView mAddressText;
    private ImageView mPhoneImage;
    private ImageView mEmailImage;
    private ImageView mWebUrlImage;
    private String mPhoneText;
    private String mEmailText;
    private String mWebUrlText;
    private LinearLayout mExtrasLayout;
    private Button mChatWithVenueItemBtn;
    private ScrollView mScrollView;

    private int mWidthOfVenueItemBtn = 0;

    public static VenueItemFragment newInstance(int instance) {

        Bundle args = new Bundle();
        args.putInt(ARGS_INSTANCE, instance);
        VenueItemFragment fragment = new VenueItemFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getNavigationHostActivity().setNavTitle(mVenueItem.getTitle());

        View rootView = inflater.inflate(R.layout.fragment_venue_item, container, false);

        mLogoImage = (ImageView) rootView.findViewById(R.id.iv_venue_item_logo);
        mTitleText = (TextView) rootView.findViewById(R.id.tv_venue_item_title);
        mSubTitleText = (TextView) rootView.findViewById(R.id.tv_venue_item_subtitle);
        mDescText = (TextView) rootView.findViewById(R.id.tv_image_viewer_save);
        mAddressText = (TextView) rootView.findViewById(R.id.tv_venue_item_address);
        mPhoneImage = (ImageView) rootView.findViewById(R.id.iv_venue_item_phone);
        mEmailImage = (ImageView) rootView.findViewById(R.id.iv_venue_item_email);
        mWebUrlImage = (ImageView) rootView.findViewById(R.id.iv_venue_item_weburl);
        mExtrasLayout = (LinearLayout) rootView.findViewById(R.id.ll_venue_item_extras);
        mChatWithVenueItemBtn = (Button) rootView.findViewById(R.id.btn_venue_item_chat);
        mScrollView = (ScrollView) rootView.findViewById(R.id.sv_venue_item);

        mScrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {

                if(mScrollView != null && mVenueItem != null) {

                    // If we hit the end of the scoll view - remove the badge count for the venue message.
                    if(mScrollView.getChildAt(0).getBottom() <= (mScrollView.getHeight() + mScrollView.getScrollY())) {

                        if(!Utility.getInstance().getVenueItemMessageViewed(mVenueItem.getObjectId())) {

                            Utility.getInstance().setVenueItemMessageViewed(mVenueItem.getObjectId(), true);

                            int newVenuesBadgeCount = Utility.getInstance().getVenuesBadgeCount() - 1;

                            if(newVenuesBadgeCount > 0) {
                                getNavigationHostActivity().setBadgeCountForTabAtPosition(newVenuesBadgeCount, 0);
                            } else {
                                getNavigationHostActivity().setBadgeCountForTabAtPosition(0, 0);
                            }

                            Utility.getInstance().setVenuesBadgeCount(newVenuesBadgeCount);
                        }
                    }
                }
            }
        });

        mChatWithVenueItemBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AttendeesFragment fragment = new AttendeesFragment();
                fragment.setVenueItem(mVenueItem);
                getNavigationHostActivity().pushFragment(fragment);
            }
        });

        mAddressText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MapFragment fragment = new MapFragment();
                fragment.setVendor(mVenueItem);
                getNavigationHostActivity().pushFragment(fragment);
            }
        });

        mPhoneImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(mPhoneText == null || mPhoneText.isEmpty()) {
                    Utility.getInstance().showOkDialog(getContext(), "Phone Number", "I'm sorry, but no phone number has been provided for " + mTitleText.getText() + " yet.");
                    return;
                }

                Utility.getInstance().showOkCancelDialog(getContext(), "Place Call",
                        "Do you wish to call " + mPhoneText + " now?",
                        new Utility.OkCallback() {
                            @Override
                            public void onOk() {

                                String phoneNumbersOnly = mPhoneText.replaceAll("[^0-9]", "");

                                Uri number = Uri.parse("tel:" + phoneNumbersOnly);
                                Intent dialIntent = new Intent(Intent.ACTION_DIAL, number);
                                startActivity(dialIntent);
                            }
                        });
            }
        });

        mEmailImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(mEmailText == null || mEmailText.isEmpty()) {
                    Utility.getInstance().showOkDialog(getContext(), "Email", "I'm sorry, but no email address has been provided for " + mTitleText.getText() + " yet.");
                    return;
                }

                Intent sendEmailIntent = new Intent(Intent.ACTION_SEND);
                sendEmailIntent.setType("message/rfc822");
                sendEmailIntent.putExtra(Intent.EXTRA_EMAIL  , new String[]{mEmailText});

                try {
                    startActivity(Intent.createChooser(sendEmailIntent, "Send Email..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Utility.getInstance().showErrorDialog(getContext(), "Email Failure", "There are no email clients installed.");
                }
            }
        });

        mWebUrlImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(mWebUrlText == null || mWebUrlText.isEmpty()) {
                    Utility.getInstance().showOkDialog(getContext(), "Web Address", "I'm sorry, but no website has been provided for " + mTitleText.getText() + " yet.");
                    return;
                }

                CloudAnalytics.getInstance().sendEventLinkViewed(mWebUrlText, "venue_item", mVenueItem.getTitle(), mVenueItem.getVenueItemType());

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(mWebUrlText));
                startActivity(browserIntent);
            }
        });

        if(mVenueItem != null) {

            mTitleText.setText(mVenueItem.getTitle());
            mSubTitleText.setText(mVenueItem.getSubTitle());
            mDescText.setText(mVenueItem.getDesc().replace("\\n", System.getProperty("line.separator")));
            mAddressText.setText(mVenueItem.getAddress().replace("\\n", System.getProperty("line.separator")));
            mPhoneText = mVenueItem.getPhone();
            mEmailText = mVenueItem.getEmail();
            mWebUrlText = mVenueItem.getWebUrl();

            // If we have extras to load, observe the layout of the button above our extras layout
            // for its onPreDraw. Once we see the onPreDraw we will know exactly how wide the button
            // is and we can force the extras layout to be the same width.
            ViewTreeObserver vto = mChatWithVenueItemBtn.getViewTreeObserver();
            vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {

                public boolean onPreDraw() {

                    mChatWithVenueItemBtn.getViewTreeObserver().removeOnPreDrawListener(this);

                    mWidthOfVenueItemBtn = mChatWithVenueItemBtn.getMeasuredWidth();

                    if(mVenueItem.getLogoUrl() != null) {

                        Utility.getInstance().showLoadSpinner(getContext(), "Loading");

                        Glide.with(getContext())
                                .load(mVenueItem.getLogoUrl())
                                .asBitmap()
                                .placeholder(Utility.getInstance().getVenuePlaceHolderImage())
                                .fitCenter()
                                .listener(new RequestListener<String, Bitmap>() {
                                    @Override
                                    public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                        Utility.getInstance().hideLoadSpinner();
                                        return false;
                                    }
                                    @Override
                                    public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                        return false;
                                    }
                                }).into(new BitmapImageViewTarget(mLogoImage) {
                                    @Override
                                    protected void setResource(Bitmap resource) {

                                        // Force the image to match the width of the buttons.
                                        Bitmap bitmap = Utility.getInstance().resizeBitmapToWidth(resource, mWidthOfVenueItemBtn);
                                        Drawable drawable = new BitmapDrawable(getResources(), bitmap);
                                        mLogoImage.setImageDrawable(drawable);
                                        Utility.getInstance().hideLoadSpinner();
                                    }
                                });
                    }

                    loadExtras();

                    if(!mVenueItem.isEnableChat()) {
                        mChatWithVenueItemBtn.setVisibility(View.GONE);
                    }

                    return true;
                }
            });
        }

        return rootView;
    }

    public void loadExtras() {

        if(mExtrasLayout.getChildCount() > 0) {
            mExtrasLayout.removeAllViews();
            mExtrasLayout.setVisibility(View.GONE);
        }

        if(mVenueItem.getExtras() != null) {
            Utility.getInstance().addExtrasToLayout(getContext(), mExtrasLayout, mWidthOfVenueItemBtn, mVenueItem.getExtras(), "venue_item", mVenueItem.getTitle(), mVenueItem.getVenueItemType());
        }

        addVenueItemMessage(mWidthOfVenueItemBtn);
    }

    public void addVenueItemMessage(int width) {

        if(Utility.getInstance().getVenueItemMessage(mVenueItem.getObjectId()) != null) {

            //
            // First, add notice icon and text "A special message from the host:"
            //

            LinearLayout linearLayout = new LinearLayout(getContext());
            linearLayout.setOrientation(LinearLayout.HORIZONTAL);
            linearLayout.setBackgroundColor(getContext().getResources().getColor(R.color.white));
            LinearLayout.LayoutParams layoutParams =
                    new LinearLayout.LayoutParams(width, LinearLayout.LayoutParams.WRAP_CONTENT);
            linearLayout.setLayoutParams(layoutParams);
            layoutParams.setMargins(0, 16, 0, 0);
            linearLayout.setPadding(8, 8, 8, 8);

            ImageView venueNoticeImageView = new ImageView(getContext());
            venueNoticeImageView.setImageResource(R.drawable.venue_notice);
            LinearLayout.LayoutParams layoutParamsForImage =
                    new LinearLayout.LayoutParams(70, 70);
            venueNoticeImageView.setLayoutParams(layoutParamsForImage);

            TextView hostTextView = new TextView(getContext());
            hostTextView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            Typeface typeFace = TypefaceUtils.load(CandCApplication.getAppContext().getAssets(), "fonts/OpenSans-Regular.otf");
            if(typeFace != null) {
                hostTextView.setTypeface(typeFace);
            }
            hostTextView.setTextSize(17);
            hostTextView.setText("A special message from " + mVenueItem.getTitle() + ":");
            LinearLayout.LayoutParams layoutParamsText =
                    new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            hostTextView.setLayoutParams(layoutParamsText);

            linearLayout.addView(venueNoticeImageView);
            linearLayout.addView(hostTextView);
            mExtrasLayout.addView(linearLayout);

            //
            // Finally, add the message from the venue host...
            //

            TextView messageTextView = new TextView(getContext());
            messageTextView.setAutoLinkMask(Linkify.ALL);
            messageTextView.setBackgroundColor(getContext().getResources().getColor(R.color.white));
            messageTextView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            messageTextView.setPadding(8, 8, 8, 8);
            if(typeFace != null) {
                messageTextView.setTypeface(typeFace);
            }
            messageTextView.setTextSize(17);
            messageTextView.setText(Utility.getInstance().getVenueItemMessage(mVenueItem.getObjectId()));

            LinearLayout.LayoutParams layoutParamsMessageText =
                    new LinearLayout.LayoutParams(width, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParamsMessageText.setMargins(0, 16, 0, 0);

            messageTextView.setLayoutParams(layoutParamsMessageText);

            mExtrasLayout.addView(messageTextView);

            mExtrasLayout.setVisibility(View.VISIBLE);
        }
    }

    public VenueItem getVenueItem() {
        return mVenueItem;
    }

    public void setVenueItem(VenueItem venueItem) {
        this.mVenueItem = venueItem;
    }
}