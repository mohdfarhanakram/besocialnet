package com.joinbesocial.candc.fragments;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.joinbesocial.candc.SimpleDividerItemDecoration;
import com.joinbesocial.candc.models.Venue;
import com.joinbesocial.candc.Cloud;
import com.joinbesocial.candc.adapters.VenuesAdapter;
import com.joinbesocial.candc.R;
import com.joinbesocial.candc.Utility;

import java.util.ArrayList;

public class VenuesFragment extends BaseFragment {

    private final static String TAG = VenuesFragment.class.getSimpleName();

    private SwipeRefreshLayout swipeContainer;

    private RecyclerView mVenuesRecyclerView;
    private VenuesAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    ArrayList<Venue> mVenues = new ArrayList<>();

    public static VenuesFragment newInstance(int instance) {

        Bundle args = new Bundle();
        args.putInt(ARGS_INSTANCE, instance);
        VenuesFragment fragment = new VenuesFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getNavigationHostActivity().setNavTitle("Venues");

        View rootView = inflater.inflate(R.layout.fragment_venues, container, false);

        //
        // Setup SwipeRefreshLayout for pull-to-refresh...
        //

        swipeContainer = (SwipeRefreshLayout) rootView.findViewById(R.id.srl_venues);
        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadEvents(false);
            }
        });

        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        //
        // Setup the RecyclerView for our data...
        //

        mVenuesRecyclerView = (RecyclerView) rootView.findViewById(R.id.rv_venues);
        mVenuesRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getContext()));

        // Use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mVenuesRecyclerView.setHasFixedSize(true);

        // Use a linear layout manager.
        mLayoutManager = new LinearLayoutManager(getActivity());
        mVenuesRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new VenuesAdapter(mVenues, getNavigationHostActivity(), getContext());
        mVenuesRecyclerView.setAdapter(mAdapter);

        loadEvents(true);

        return rootView;
    }

    private void loadEvents(final boolean showSpinner) {

        if(showSpinner) {
            Utility.getInstance().showLoadSpinner(this.getContext(), "Loading Venues");
        }

        Cloud.getInstance().loadVenues(

                new Cloud.VenuesResponseCallback() {
                    @Override
                    public void onData(ArrayList<Venue> venues) {

                        if(showSpinner) {
                            Utility.getInstance().hideLoadSpinner();
                        }

                        mVenues = venues;

                        mAdapter.clear();
                        mAdapter.addAll(mVenues);

                        swipeContainer.setRefreshing(false);
                    }
                },

                new Cloud.FaultCallback() {
                    @Override
                    public void onFault(String code, String message) {

                        if(showSpinner) {
                            Utility.getInstance().hideLoadSpinner();
                        }

                        swipeContainer.setRefreshing(false);

                        Log.d(TAG, "loadEvents " + message);
                    }
                }
        );
    }
}