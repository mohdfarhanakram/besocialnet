package com.joinbesocial.candc.fragments;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.joinbesocial.candc.NavigationHostActivity;
import com.joinbesocial.candc.SimpleDividerItemDecoration;
import com.joinbesocial.candc.models.ChatSession;
import com.joinbesocial.candc.Cloud;
import com.joinbesocial.candc.adapters.MessagesAdapter;
import com.joinbesocial.candc.R;
import com.joinbesocial.candc.Utility;

import java.util.ArrayList;

public class MessagesFragment extends BaseFragment {

    private final static String TAG = MessagesFragment.class.getSimpleName();

    private SwipeRefreshLayout swipeContainer;

    private RecyclerView mMessagesRecyclerView;
    private MessagesAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private ImageView mChatBubblesImage;
    private TextView mChatIntro;

    ArrayList<ChatSession> mChats = new ArrayList<>();

    public static MessagesFragment newInstance(int instance) {

        Bundle args = new Bundle();
        args.putInt(ARGS_INSTANCE, instance);
        MessagesFragment fragment = new MessagesFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getNavigationHostActivity().setNavTitle("Messages");

        View rootView = inflater.inflate(R.layout.fragment_messages, container, false);

        mChatBubblesImage = (ImageView) rootView.findViewById(R.id.iv_messages_chat_bubbles);
        mChatIntro = (TextView) rootView.findViewById(R.id.tv_messages_intro);

        //
        // Setup SwipeRefreshLayout for pull-to-refresh...
        //

        swipeContainer = (SwipeRefreshLayout) rootView.findViewById(R.id.srl_messages);
        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadChatSessions(false);
            }
        });

        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        //
        // Setup the RecyclerView for our data...
        //

        mMessagesRecyclerView = (RecyclerView) rootView.findViewById(R.id.rv_messages);
        mMessagesRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getContext()));

        // Use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mMessagesRecyclerView.setHasFixedSize(true);

        // Use a linear layout manager.
        mLayoutManager = new LinearLayoutManager(getActivity());
        mMessagesRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new MessagesAdapter(getContext(), mChats, getNavigationHostActivity());
        mMessagesRecyclerView.setAdapter(mAdapter);

        return rootView;
    }

    @Override
    public void onResume() {

        super.onResume();

        Cloud.getInstance().registerDeviceForPush();

        loadChatSessions(true);

        getNavigationHostActivity().setOnToolbarItemSelectedListener(R.id.it_toolbar_search_users, new NavigationHostActivity.OnToolbarItemSelectedListener() {
            @Override
            public void onToolbarItemSelected() {

                if(Utility.getInstance().getVenueObjectId() == null) {

                    Utility.getInstance().showOkDialog(getContext(), "Messages",
                            "Please set your venue to begin messaging other visitors.");
                    return;
                }

                AttendeesFragment fragment = new AttendeesFragment();
                getNavigationHostActivity().pushFragment(fragment);
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();

        getNavigationHostActivity().removeOnToolbarItemSelectedListener(R.id.it_toolbar_search_users);
    }

    private void loadChatSessions(final boolean showSpinner) {

        mAdapter.clear();

        if(Utility.getInstance().getVenueObjectId() == null) {
            swipeContainer.setRefreshing(false);
            return;
        }

        if(showSpinner) {
            Utility.getInstance().showLoadSpinner(this.getContext(), "Loading Messages");
        }

        if(Utility.getInstance().getVenueObjectId() != null ) {

            Cloud.getInstance().loadChatSessions(

                    new Cloud.ChatSessionsResponseCallback() {
                        @Override
                        public void onData(ArrayList<ChatSession> chats) {

                            if(showSpinner) {
                                Utility.getInstance().hideLoadSpinner();
                            }

                            mChats = chats;

                            mAdapter.clear();
                            mAdapter.addAll(mChats);

                            swipeContainer.setRefreshing(false);

                            if(chats.size() > 0) {
                                mChatBubblesImage.setVisibility(View.INVISIBLE);
                                mChatIntro.setVisibility(View.INVISIBLE);
                            }

                            updateMessagesBadgeCount();
                        }
                    },

                    new Cloud.FaultCallback() {
                        @Override
                        public void onFault(String code, String message) {

                            if(showSpinner) {
                                Utility.getInstance().hideLoadSpinner();
                            }

                            swipeContainer.setRefreshing(false);

                            Utility.getInstance().showErrorDialog(MessagesFragment.this.getContext(), "Load Error", message);
                        }
                    }
            );
        }
    }

    private void updateMessagesBadgeCount() {

        String userObjectId = Cloud.getInstance().getUserObjectId();
        int totalUnviewedMessages = 0;

        for(ChatSession chatSession : mChats) {

            if(userObjectId.equals(chatSession.getOwnerId())) {
                // This means I'm the owner!
                if(!chatSession.isOwnerViewed()) {
                    ++totalUnviewedMessages;
                }
            } else {
                // This means I'm the target!
                if(!chatSession.isTargetViewed()) {
                    ++totalUnviewedMessages;
                }
            }
        }

        Utility.getInstance().setMessagesBadgeCount(totalUnviewedMessages);
        getNavigationHostActivity().setBadgeCountForTabAtPosition(totalUnviewedMessages, 1);
    }
}