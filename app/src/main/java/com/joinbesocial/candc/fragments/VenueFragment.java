package com.joinbesocial.candc.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.joinbesocial.candc.Cloud;
import com.joinbesocial.candc.CloudAnalytics;
import com.joinbesocial.candc.R;
import com.joinbesocial.candc.Utility;
import com.joinbesocial.candc.models.Venue;

public class VenueFragment extends BaseFragment {

    private final static String TAG = VenueFragment.class.getSimpleName();

    public static VenueFragment newInstance(int instance) {

        Bundle args = new Bundle();
        args.putInt(ARGS_INSTANCE, instance);
        VenueFragment fragment = new VenueFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getNavigationHostActivity().setNavTitle("Find Venue");

        View rootView = inflater.inflate(R.layout.fragment_venue, container, false);

        Button findEventById = (Button) rootView.findViewById(R.id.btn_venue_find_by_id);
        findEventById.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Utility.getInstance().showFindEventDialog(getContext(), new Utility.OkFindEventCallback() {
                    @Override
                    public void onOkFindEvent(String venueCode) {

                        Utility.getInstance().showLoadSpinner(VenueFragment.this.getContext(), "Searching");

                                Cloud.getInstance().findVenue(venueCode, new Cloud.VenueResponseCallback() {
                                    @Override
                                    public void onData(final Venue venue) {

                                        Cloud.getInstance().addVenueCodeToUser(venue.getVenueCode(), new Cloud.ResponseCallback() {
                                            @Override
                                            public void onResponse() {

                                                Utility.getInstance().hideLoadSpinner();

                                                Utility.getInstance().setVenueObjectId(venue.getObjectId());
                                                Utility.getInstance().setVenueCode(venue.getVenueCode());
                                                Utility.getInstance().setVenueTitle(venue.getTitle());

                                                if(venue.getGeofences() != null) {
                                                    Utility.getInstance().setGeofences(venue.getGeofences());
                                                }

                                                CloudAnalytics.getInstance().sendEventSelectedEvent("find_by_id", venue.getVenueCode());

                                                CurrentVenueFragment fragment = new CurrentVenueFragment();
                                                getNavigationHostActivity().replaceFragment(fragment);

                                            }
                                        }, new Cloud.FaultCallback() {
                                            @Override
                                            public void onFault(String code, String message) {
                                                Utility.getInstance().hideLoadSpinner();

                                                Utility.getInstance().showErrorDialog(VenueFragment.this.getContext(), "Search Error", message);
                                            }
                                        });
                                    }
                                }, new Cloud.FaultCallback() {
                                    @Override
                                    public void onFault(String code, String message) {

                                        Utility.getInstance().hideLoadSpinner();
                                        Utility.getInstance().showErrorDialog(VenueFragment.this.getContext(), "Search Error", message);
                                    }
                                });
                    }
                });
            }
        });

        Button searchEvent = (Button) rootView.findViewById(R.id.btn_venue_search);
        searchEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                VenuesFragment fragment = new VenuesFragment();
                getNavigationHostActivity().pushFragment(fragment);
            }
        });

        return rootView;
    }
}