package com.joinbesocial.candc.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.joinbesocial.candc.NavigationHostActivity;
import com.joinbesocial.candc.R;

public class BaseFragment extends Fragment {

    public static final String ARGS_INSTANCE = "com.ncapdevi.sample.argsInstance";

    private NavigationHostActivity mNavigationHostActivity;
    private int mInt = 0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if(args != null) {
            mInt = args.getInt(ARGS_INSTANCE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_main, container, false);
        return view;
    }

    @Override
    public void onAttach(Context context) {

        super.onAttach(context);

        if(context instanceof NavigationHostActivity) {
            this.mNavigationHostActivity = (NavigationHostActivity) context;
        }
    }

    public NavigationHostActivity getNavigationHostActivity() {
        return mNavigationHostActivity;
    }
}
