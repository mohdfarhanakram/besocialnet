package com.joinbesocial.candc.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.joinbesocial.candc.CloudAnalytics;
import com.joinbesocial.candc.R;
import com.joinbesocial.candc.Reports;
import com.joinbesocial.candc.Utility;

public class ReportsFragment extends BaseFragment {

    private final static String TAG = ReportsFragment.class.getSimpleName();

    private TextView mActiveUsers;
    private TextView mActiveChats;

    private TextView mTopVenueItem1;
    private TextView mTopVenueItem2;
    private TextView mTopVenueItem3;
    private TextView mTopVenueItem4;
    private TextView mTopVenueItem5;

    private TextView mTopOVenueItem1;
    private TextView mTopOVenueItem2;
    private TextView mTopOVenueItem3;
    private TextView mTopOVenueItem4;
    private TextView mTopOVenueItem5;

    private TextView mAvgUserVisits;
    private TextView mExitsPreviousHour;
    
    public static ReportsFragment newInstance(int instance) {

        Bundle args = new Bundle();
        args.putInt(ARGS_INSTANCE, instance);
        ReportsFragment fragment = new ReportsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getNavigationHostActivity().setNavTitle("Reports");

        View rootView = inflater.inflate(R.layout.fragment_reports, container, false);

        mActiveUsers = (TextView) rootView.findViewById(R.id.tv_reports_au_num);
        mActiveChats = (TextView) rootView.findViewById(R.id.tv_reports_ac_num);

        mTopVenueItem1 = (TextView) rootView.findViewById(R.id.tv_reports_topv1);
        mTopVenueItem2 = (TextView) rootView.findViewById(R.id.tv_reports_topv2);
        mTopVenueItem3 = (TextView) rootView.findViewById(R.id.tv_reports_topv3);
        mTopVenueItem4 = (TextView) rootView.findViewById(R.id.tv_reports_topv4);
        mTopVenueItem5 = (TextView) rootView.findViewById(R.id.tv_reports_topv5);

        mTopOVenueItem1 = (TextView) rootView.findViewById(R.id.tv_reports_topov1);
        mTopOVenueItem2 = (TextView) rootView.findViewById(R.id.tv_reports_topov2);
        mTopOVenueItem3 = (TextView) rootView.findViewById(R.id.tv_reports_topov3);
        mTopOVenueItem4 = (TextView) rootView.findViewById(R.id.tv_reports_topov4);
        mTopOVenueItem5 = (TextView) rootView.findViewById(R.id.tv_reports_topov5);

        mAvgUserVisits = (TextView) rootView.findViewById(R.id.tv_reports_avg_visits_num);
        mExitsPreviousHour = (TextView) rootView.findViewById(R.id.tv_reports_exits_num);

        mActiveUsers.setText("?");
        mActiveChats.setText("?");

        mTopVenueItem1.setText("?");
        mTopVenueItem2.setText("?");
        mTopVenueItem3.setText("?");
        mTopVenueItem4.setText("?");
        mTopVenueItem5.setText("?");

        mTopOVenueItem1.setText("?");
        mTopOVenueItem2.setText("?");
        mTopOVenueItem3.setText("?");
        mTopOVenueItem4.setText("?");
        mTopOVenueItem5.setText("?");

        mExitsPreviousHour.setText("?");
        mAvgUserVisits.setText("?");

        Utility.getInstance().showLoadSpinner(getContext(), "Loading Reports");

        CloudAnalytics.getInstance().collectReports(new CloudAnalytics.ReportsResponseCallback() {
            @Override
            public void onResponse(Reports reports) {

                mActiveUsers.setText(String.valueOf(reports.activeUsers));
                mActiveChats.setText(String.valueOf(reports.activeChats));

                if(reports.top5VenueItems.size() > 0) {
                    mTopVenueItem1.setText(String.valueOf(reports.top5VenueItems.get(0)));
                }

                if(reports.top5VenueItems.size() > 1) {
                    mTopVenueItem2.setText(String.valueOf(reports.top5VenueItems.get(1)));
                }

                if(reports.top5VenueItems.size() > 2) {
                    mTopVenueItem3.setText(String.valueOf(reports.top5VenueItems.get(2)));
                }

                if(reports.top5VenueItems.size() > 3) {
                    mTopVenueItem4.setText(String.valueOf(reports.top5VenueItems.get(3)));
                }

                if(reports.top5VenueItems.size() > 4) {
                    mTopVenueItem5.setText(String.valueOf(reports.top5VenueItems.get(4)));
                }

                if(reports.top5OutSideVenueItems.size() > 0) {
                    mTopOVenueItem1.setText(String.valueOf(reports.top5OutSideVenueItems.get(0)));
                }

                if(reports.top5OutSideVenueItems.size() > 1) {
                    mTopOVenueItem2.setText(String.valueOf(reports.top5OutSideVenueItems.get(1)));
                }

                if(reports.top5OutSideVenueItems.size() > 2) {
                    mTopOVenueItem3.setText(String.valueOf(reports.top5OutSideVenueItems.get(2)));
                }

                if(reports.top5OutSideVenueItems.size() > 3) {
                    mTopOVenueItem4.setText(String.valueOf(reports.top5OutSideVenueItems.get(3)));
                }

                if(reports.top5OutSideVenueItems.size() > 4) {
                    mTopOVenueItem5.setText(String.valueOf(reports.top5OutSideVenueItems.get(4)));
                }

                mExitsPreviousHour.setText(String.valueOf(reports.exitsPrevHour));
                mAvgUserVisits.setText(String.valueOf(reports.avgUserVisits));

                Utility.getInstance().hideLoadSpinner();
            }
        });

        return rootView;
    }
}