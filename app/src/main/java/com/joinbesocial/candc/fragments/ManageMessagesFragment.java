package com.joinbesocial.candc.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.joinbesocial.candc.CandCApplication;
import com.joinbesocial.candc.Cloud;
import com.joinbesocial.candc.NavigationHostActivity;
import com.joinbesocial.candc.R;
import com.joinbesocial.candc.SimpleDividerItemDecoration;
import com.joinbesocial.candc.Utility;
import com.joinbesocial.candc.adapters.ManageMessagesAdapter;
import com.joinbesocial.candc.models.ScheduledMessage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class ManageMessagesFragment extends BaseFragment {

    private final static String TAG = ManageMessagesFragment.class.getSimpleName();

    private SwipeRefreshLayout swipeContainer;

    private RecyclerView mMessagesRecyclerView;
    private ManageMessagesAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    ArrayList<ScheduledMessage> mScheduledMessages = new ArrayList<>();

    public static ManageMessagesFragment newInstance(int instance) {

        Bundle args = new Bundle();
        args.putInt(ARGS_INSTANCE, instance);
        ManageMessagesFragment fragment = new ManageMessagesFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getNavigationHostActivity().setNavTitle("Scheduled Messages");

        View rootView = inflater.inflate(R.layout.fragment_manage_messages, container, false);

        //
        // Setup SwipeRefreshLayout for pull-to-refresh...
        //

        swipeContainer = (SwipeRefreshLayout) rootView.findViewById(R.id.srl_mng_messages);
        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadScheduledMessages(false);
            }
        });

        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        //
        // Setup the RecyclerView for our data...
        //

        mMessagesRecyclerView = (RecyclerView) rootView.findViewById(R.id.rv_mng_messages);
        mMessagesRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getContext()));

        // Use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mMessagesRecyclerView.setHasFixedSize(true);

        // Use a linear layout manager.
        mLayoutManager = new LinearLayoutManager(getActivity());
        mMessagesRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new ManageMessagesAdapter(getContext(), mScheduledMessages);
        mMessagesRecyclerView.setAdapter(mAdapter);

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

                // Remove swiped item from list and notify the RecyclerView
                final int position = viewHolder.getAdapterPosition();
                ScheduledMessage scheduledMessage = mScheduledMessages.get(position);

                Utility.getInstance().showLoadSpinner(ManageMessagesFragment.this.getContext(), "Deleting Message");

                Cloud.getInstance().cancelScheduleMessage(scheduledMessage, new Cloud.CancelScheduledMessageResponseCallback() {
                    @Override
                    public void onResponse() {

                        mAdapter.notifyItemRemoved(position);
                        Utility.getInstance().hideLoadSpinner();
                        loadScheduledMessages(true);
                    }
                }, new Cloud.FaultCallback() {
                    @Override
                    public void onFault(String code, String message) {

                        Utility.getInstance().hideLoadSpinner();
                        Utility.getInstance().showErrorDialog(ManageMessagesFragment.this.getContext(), "Delete Error", message);
                    }
                });
            }

            public static final float ALPHA_FULL = 1.0f;

            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {

                if(actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {

                    // Get RecyclerView item from the ViewHolder.
                    View itemView = viewHolder.itemView;

                    Paint paint = new Paint();
                    Bitmap icon;

                    if(dX > 0) {

// We don't need any of this code for this use case since we only support swiping LEFT.
//                        // Set the icon and color for swiping RIGHT!
//                        icon = BitmapFactory.decodeResource(CandCApplication.getAppContext().getResources(), R.drawable.ic_delete_forever_white_48dp);
//                        paint.setARGB(255, 255, 0, 0);
//
//                        // Draw Rect with varying right side, equal to displacement dX
//                        c.drawRect((float) itemView.getLeft(), (float) itemView.getTop(), dX,
//                                (float) itemView.getBottom(), paint);
//
//                        // Set the image icon for Right swipe
//                        c.drawBitmap(icon,
//                                (float) itemView.getLeft() + convertDpToPx(16),
//                                (float) itemView.getTop() + ((float) itemView.getBottom() - (float) itemView.getTop() - icon.getHeight())/2,
//                                paint);
                    } else {

                        // Set the icon and color for swiping LEFT!
                        icon = BitmapFactory.decodeResource(CandCApplication.getAppContext().getResources(), R.drawable.ic_delete_forever_white_48dp);
                        paint.setColor(getResources().getColor(R.color.row_delete));

                        // Draw Rect with varying left side, equal to the item's right side
                        // plus negative displacement dX
                        c.drawRect((float) itemView.getRight() + dX, (float) itemView.getTop(),
                                (float) itemView.getRight(), (float) itemView.getBottom(), paint);

                        // Set the image icon for Left swipe
                        c.drawBitmap(icon,
                                (float) itemView.getRight() - convertDpToPx(16) - icon.getWidth(),
                                (float) itemView.getTop() + ((float) itemView.getBottom() - (float) itemView.getTop() - icon.getHeight())/2,
                                paint);
                    }

                    // Fade out the view as it is swiped out of the parent's bounds
                    final float alpha = ALPHA_FULL - Math.abs(dX) / (float) viewHolder.itemView.getWidth();
                    viewHolder.itemView.setAlpha(alpha);
                    viewHolder.itemView.setTranslationX(dX);

                } else {
                    super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                }
            }

            private int convertDpToPx(int dp){
                return Math.round(dp * (getResources().getDisplayMetrics().xdpi / DisplayMetrics.DENSITY_DEFAULT));
            }
        });
        itemTouchHelper.attachToRecyclerView(mMessagesRecyclerView);

        return rootView;
    }

    @Override
    public void onResume() {

        super.onResume();

        loadScheduledMessages(true);

        getNavigationHostActivity().setOnToolbarItemSelectedListener(R.id.it_toolbar_search_users, new NavigationHostActivity.OnToolbarItemSelectedListener() {
            @Override
            public void onToolbarItemSelected() {

                if(Utility.getInstance().getVenueObjectId() == null) {

                    Utility.getInstance().showOkDialog(getContext(), "Messages",
                            "Please set your venue to begin messaging the visitors.");
                    return;
                }

                AttendeesFragment fragment = new AttendeesFragment();
                getNavigationHostActivity().pushFragment(fragment);
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();

        getNavigationHostActivity().removeOnToolbarItemSelectedListener(R.id.it_toolbar_search_users);
    }

    private void loadScheduledMessages(final boolean showSpinner) {

        mAdapter.clear();

        if(Utility.getInstance().getVenueObjectId() == null) {
            swipeContainer.setRefreshing(false);
            return;
        }

        if(showSpinner) {
            Utility.getInstance().showLoadSpinner(this.getContext(), "Loading Messages");
        }

        if(Utility.getInstance().getVenueObjectId() != null ) {

            Cloud.getInstance().loadScheduledMessages(

                    new Cloud.ScheduledMessagesResponseCallback() {
                        @Override
                        public void onData(ArrayList<ScheduledMessage> scheduledMessages) {

                            if(showSpinner) {
                                Utility.getInstance().hideLoadSpinner();
                            }

                            mScheduledMessages = scheduledMessages;

//                            Collections.sort(mScheduledMessages, new Comparator<ScheduledMessage>() {
//                                public int compare(ScheduledMessage a, ScheduledMessage b) {
//                                    return a.getDeliveryDate().compareTo(b.getDeliveryDate());
//                                }
//                            });

                            mAdapter.clear();
                            mAdapter.addAll(mScheduledMessages);

                            swipeContainer.setRefreshing(false);
                        }
                    },

                    new Cloud.FaultCallback() {
                        @Override
                        public void onFault(String code, String message) {

                            if(showSpinner) {
                                Utility.getInstance().hideLoadSpinner();
                            }

                            swipeContainer.setRefreshing(false);

                            Utility.getInstance().showErrorDialog(ManageMessagesFragment.this.getContext(), "Load Error", message);
                        }
                    }
            );
        }
    }
}