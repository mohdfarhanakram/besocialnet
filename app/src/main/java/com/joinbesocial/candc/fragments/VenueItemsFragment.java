package com.joinbesocial.candc.fragments;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.joinbesocial.candc.EndlessRecyclerViewScrollListener;
import com.joinbesocial.candc.NavigationHostActivity;
import com.joinbesocial.candc.SimpleDividerItemDecoration;
import com.joinbesocial.candc.models.VenueItem;
import com.joinbesocial.candc.Cloud;
import com.joinbesocial.candc.adapters.VenueItemsAdapter;
import com.joinbesocial.candc.R;
import com.joinbesocial.candc.Utility;

import java.util.ArrayList;

public class VenueItemsFragment extends BaseFragment {

    private final static String TAG = VenueItemsFragment.class.getSimpleName();

    private String mVenueItemName;
    private SwipeRefreshLayout swipeContainer;

    private RecyclerView mVenueItemsRecyclerView;
    private VenueItemsAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;

    private int pageOffset = 0;
    private final static int PAGE_SIZE = 9;
    private String searchText = null;

    public static VenueItemsFragment newInstance(int instance) {

        Bundle args = new Bundle();
        args.putInt(ARGS_INSTANCE, instance);
        VenueItemsFragment fragment = new VenueItemsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if(mVenueItemName == null || mVenueItemName.isEmpty()) {
            getNavigationHostActivity().setNavTitle("Venue Offerings");
            getNavigationHostActivity().setSearchHint("Search Venue Offerings...");
        } else {
            getNavigationHostActivity().setNavTitle(mVenueItemName);
            getNavigationHostActivity().setSearchHint("Search " + mVenueItemName + "...");
        }

        View rootView = inflater.inflate(R.layout.fragment_venue_items, container, false);

        //
        // Setup SwipeRefreshLayout for pull-to-refresh...
        //

        swipeContainer = (SwipeRefreshLayout) rootView.findViewById(R.id.srl_venue_items);
        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                resetAndLoadVenueItems(false);
            }
        });

        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        //
        // Setup the RecyclerView for our data...
        //

        mVenueItemsRecyclerView = (RecyclerView) rootView.findViewById(R.id.rv_venue_items);
        mVenueItemsRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getContext()));

        // Use a linear layout manager.
        mLayoutManager = new LinearLayoutManager(getActivity());
        mVenueItemsRecyclerView.setLayoutManager(mLayoutManager);

        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {

                //Log.e("XXX", "onLoadMore: page = " + String.valueOf(page) + ", totalItemsCount = " + String.valueOf(totalItemsCount));

                loadVenueItems(new Cloud.VenueItemsResponseCallback() {
                    @Override
                    public void onData(ArrayList<VenueItem> foundVenueItems) {
                        for(VenueItem venueItem : foundVenueItems) {
                            mAdapter.add(venueItem);
                        }
                    }
                }, new Cloud.FaultCallback() {
                    @Override
                    public void onFault(String code, String message) {}
                });
            }
        };

        mVenueItemsRecyclerView.addOnScrollListener(endlessRecyclerViewScrollListener);

        mAdapter = new VenueItemsAdapter(getNavigationHostActivity());
        mVenueItemsRecyclerView.setAdapter(mAdapter);

        resetAndLoadVenueItems(true);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        getNavigationHostActivity().setOnSearchTextSubmitListener(new NavigationHostActivity.OnSearchTextSubmitListener() {
            @Override
            public void onSearchTextSubmit(String text) {

                searchText = text;
                resetAndLoadVenueItems(true);
            }
        });

        getNavigationHostActivity().setOnSearchViewToggleListener(new NavigationHostActivity.OnSearchViewToggleListener() {
            @Override
            public void onSearchViewToggle(boolean toggle) {
                if(toggle) {
                    //Log.e("XXX", "Search View ON");
                } else {
                    //Log.e("XXX", "Search View OFF");
                    searchText = null;
                    resetAndLoadVenueItems(true);
                }
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();

        getNavigationHostActivity().removeOnSearchTextSubmitListener();
        getNavigationHostActivity().removeOnSearchViewToggleListener();
        getNavigationHostActivity().closeSearchView();
    }

    private void loadVenueItems(final Cloud.VenueItemsResponseCallback venueItemsResponseCallback, final Cloud.FaultCallback faultCallback) {

        Cloud.getInstance().loadVenueItemsByPage(PAGE_SIZE, pageOffset, searchText,

                new Cloud.VenueItemPageResponseCallback() {
                    @Override
                    public void onData(ArrayList<VenueItem> foundVenueItems, int newOffset, int totalObjects) {
                        VenueItemsFragment.this.pageOffset = newOffset;
                        venueItemsResponseCallback.onData(foundVenueItems);
                    }
                },

                new Cloud.FaultCallback() {
                    @Override
                    public void onFault(String code, String message) {
                        faultCallback.onFault(code, message);
                    }
                }
        );
    }

    private void resetAndLoadVenueItems(final boolean showSpinner) {

        pageOffset = 0;

        if(showSpinner) {

            if(mVenueItemName == null || mVenueItemName.isEmpty()) {
                Utility.getInstance().showLoadSpinner(this.getContext(), "Venue Offerings");
            } else {
                Utility.getInstance().showLoadSpinner(this.getContext(), "Loading " + mVenueItemName);
            }
        }

        loadVenueItems(new Cloud.VenueItemsResponseCallback() {
            @Override
            public void onData(ArrayList<VenueItem> foundVenueItems) {

                mAdapter.clear();
                endlessRecyclerViewScrollListener.resetState();
                mAdapter.addAll(foundVenueItems);

                if(showSpinner) {
                    Utility.getInstance().hideLoadSpinner();
                } else {
                    swipeContainer.setRefreshing(false);
                }
            }
        }, new Cloud.FaultCallback() {
            @Override
            public void onFault(String code, String message) {
                if(showSpinner) {
                    Utility.getInstance().hideLoadSpinner();
                } else {
                    swipeContainer.setRefreshing(false);
                }
            }
        });
    }

    public String getVenueItemName() {
        return mVenueItemName;
    }

    public void setVenueItemName(String venueItemName) {
        this.mVenueItemName = venueItemName;
    }
}