package com.joinbesocial.candc.fragments;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.Target;
import com.joinbesocial.candc.BuildConfig;
import com.joinbesocial.candc.CandCApplication;
import com.joinbesocial.candc.Cloud;
import com.joinbesocial.candc.NavigationHostActivity;
import com.joinbesocial.candc.R;
import com.joinbesocial.candc.Utility;
import com.joinbesocial.candc.activities.MainActivity;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;
import java.util.List;

import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import uk.co.chrisjenx.calligraphy.TypefaceUtils;

import static android.app.Activity.RESULT_OK;

public class ProfileFragment extends BaseFragment implements EasyPermissions.PermissionCallbacks {

    private final static String TAG = ProfileFragment.class.getSimpleName();

    private static final int RC_LOCATION_PERM = 124;

    private ImageView mProfilePicture;
    private EditText mUserName;
    private EditText mUserWorkTitle;
    private EditText mUserWorkEmail;
    private TextView mVersionText;
    private LinearLayout mExtrasLayout;

    private boolean mProfileTextChanged = false;
    private boolean mProfileImageChanged = false;
    private Bitmap mProfileBitmap = null;

    private SwitchCompat mLocation;
    private CompoundButton.OnCheckedChangeListener mOnCheckedChangeListener;

    private final TextWatcher mTextWatcher = new TextWatcher() {

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
        public void onTextChanged(CharSequence s, int start, int before, int count) {}
        public void afterTextChanged(Editable s) {
            mProfileTextChanged = true;
        }
    };

    public static ProfileFragment newInstance(int instance) {

        Bundle args = new Bundle();
        args.putInt(ARGS_INSTANCE, instance);
        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getNavigationHostActivity().setNavTitle("Profile");

        mProfileTextChanged = false;
        mProfileImageChanged = false;
        mProfileBitmap = null;

        final View rootView = inflater.inflate(R.layout.fragment_profile, container, false);

        if(com.joinbesocial.candc.BuildConfig.DEBUG) {
            Log.d(TAG, "DEBUG");
            mVersionText = (TextView) rootView.findViewById(R.id.tv_profile_version);
            int versionCode = BuildConfig.VERSION_CODE;
            String versionName = BuildConfig.VERSION_NAME;
            mVersionText.setText("v" +versionName + " (" + String.valueOf(versionCode) + ")");
        } else {
            Log.d(TAG, "RELEASE");
            mVersionText = (TextView) rootView.findViewById(R.id.tv_profile_version);
            String versionName = BuildConfig.VERSION_NAME;
            mVersionText.setText("v" +versionName);
        }

        mUserName = (EditText) rootView.findViewById(R.id.et_profile_name);
        mUserName.setText(Cloud.getInstance().getUserName());
        mUserName.addTextChangedListener(mTextWatcher);

        mUserWorkTitle = (EditText) rootView.findViewById(R.id.et_profile_work_title);
        mUserWorkTitle.setText(Cloud.getInstance().getUserWorkTitle());
        mUserWorkTitle.addTextChangedListener(mTextWatcher);

        mUserWorkEmail = (EditText) rootView.findViewById(R.id.et_profile_work_email);
        mUserWorkEmail.setText(Cloud.getInstance().getUserWorkEmail());
        mUserWorkEmail.addTextChangedListener(mTextWatcher);

        Utility.getInstance().showLoadSpinner(ProfileFragment.this.getContext(), "Loading Profile");

        mProfilePicture = (ImageView) rootView.findViewById(R.id.iv_profile_picture);
        mProfilePicture.setImageDrawable(Utility.getInstance().getPlaceHolderImage());

        mProfilePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // https://github.com/jkwiecien/EasyImage
                EasyImage.openChooserWithGallery(ProfileFragment.this, "Where would you like to select your new profile picture from?", 0);
            }
        });

        Cloud.getInstance().refreshUserData(new Cloud.ResponseCallback() {
            @Override
            public void onResponse() {
                mUserName.setText(Cloud.getInstance().getUserName());
                mUserWorkTitle.setText(Cloud.getInstance().getUserWorkTitle());
                mUserWorkEmail.setText(Cloud.getInstance().getUserWorkEmail());
                mProfileTextChanged = false;

                // If the logged in user is the host of the current venue - show the extra buttons.
                if(!Cloud.getInstance().getHostForVenueCode().isEmpty() &&
                        Cloud.getInstance().getHostForVenueCode().equals(Utility.getInstance().getVenueCode())) {

                    mExtrasLayout.setVisibility(View.VISIBLE);

                    //
                    // Reports Button...
                    //

                    Button reportsBtn = new Button(getContext());

                    reportsBtn.setBackgroundResource(R.drawable.btn_red);
                    reportsBtn.setText("Reports");
                    reportsBtn.setTextSize(17.0f);
                    reportsBtn.setTextColor(getResources().getColor(R.color.white));
                    reportsBtn.setAllCaps(false);
                    Typeface typeFace = TypefaceUtils.load(CandCApplication.getAppContext().getAssets(), "fonts/Montserrat-Bold.otf");
                    if(typeFace != null) {
                        reportsBtn.setTypeface(typeFace);
                    }

                    LinearLayout.LayoutParams layoutParamsReportsBtn =
                            new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    layoutParamsReportsBtn.setMargins(0, 16, 0, 0);
                    layoutParamsReportsBtn.setMarginEnd(16);
                    layoutParamsReportsBtn.setMarginStart(16);

                    reportsBtn.setLayoutParams(layoutParamsReportsBtn);

                    reportsBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            ReportsFragment fragment = new ReportsFragment();
                            getNavigationHostActivity().pushFragment(fragment);
                        }
                    });

                    mExtrasLayout.addView(reportsBtn);

                    //
                    // Send Messages Button...
                    //

                    Button sednMessagesBtn = new Button(getContext());

                    sednMessagesBtn.setBackgroundResource(R.drawable.btn_red);
                    sednMessagesBtn.setText("Send Messages");
                    sednMessagesBtn.setTextSize(17.0f);
                    sednMessagesBtn.setTextColor(getResources().getColor(R.color.white));
                    sednMessagesBtn.setAllCaps(false);
                    if(typeFace != null) {
                        sednMessagesBtn.setTypeface(typeFace);
                    }

                    LinearLayout.LayoutParams layoutParamsSendMessagesBtn =
                            new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    layoutParamsSendMessagesBtn.setMargins(0, 16, 0, 0);
                    layoutParamsSendMessagesBtn.setMarginEnd(16);
                    layoutParamsSendMessagesBtn.setMarginStart(16);

                    sednMessagesBtn.setLayoutParams(layoutParamsSendMessagesBtn);

                    sednMessagesBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            SendMessagesFragment fragment = new SendMessagesFragment();
                            getNavigationHostActivity().pushFragment(fragment);
                        }
                    });

                    mExtrasLayout.addView(sednMessagesBtn);

                }

                String profileImageUrl = Cloud.getInstance().getUserProfileImageUrl();

                if(profileImageUrl != null && !profileImageUrl.isEmpty()) {

                    // Attempt to load the image via Glide.
                    Glide.with(getContext())
                            .load(profileImageUrl)
                            .asBitmap()
                            .placeholder(Utility.getInstance().getPlaceHolderImage())
                            .centerCrop()
                            .listener(new RequestListener<String, Bitmap>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {

                            // If Glide can't get the image, it probabluy doens't exist so we'll need
                            // to see if we can find an image via social media.
                            Cloud.getInstance().loadUserProfileImage(new Cloud.ResponseImageCallback() {
                                @Override
                                public void onResponse(Drawable drawable) {
                                    mProfilePicture.setImageDrawable(drawable);
                                    Utility.getInstance().hideLoadSpinner();
                                }
                            }, new Cloud.FaultCallback() {
                                @Override
                                public void onFault(String code, String message) {

                                    Utility.getInstance().hideLoadSpinner();
                                    Utility.getInstance().showErrorDialog(ProfileFragment.this.getContext(), "Loading Error", message);
                                }
                            });

                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            return false;
                        }

                    }).into(new BitmapImageViewTarget(mProfilePicture) {
                        @Override
                        protected void setResource(Bitmap resource) {

                            // If Glide can get it - make it round and show it.
                            Drawable drawable = Utility.getInstance().createRoundDrawableFromBitmap(resource);
                            mProfilePicture.setImageDrawable(drawable);
                            Utility.getInstance().hideLoadSpinner();
                        }
                    });

                } else {
                    Utility.getInstance().hideLoadSpinner();
                }
            }
        }, new Cloud.FaultCallback() {
            @Override
            public void onFault(String code, String message) {
                Utility.getInstance().hideLoadSpinner();
                Utility.getInstance().showErrorDialog(ProfileFragment.this.getContext(), "Loading Error", message);
            }
        });

        mExtrasLayout = (LinearLayout) rootView.findViewById(R.id.ll_profile_extras);

        Button logoutBtn = (Button) rootView.findViewById(R.id.btn_profile_logout);
        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Utility.getInstance().showOkCancelDialog(ProfileFragment.this.getContext(), "Signing Out",
                        "Are you sure you wish to sign out of your account?",
                        new Utility.OkCallback() {
                    @Override
                    public void onOk() {

                        Utility.getInstance().showLoadSpinner(ProfileFragment.this.getContext(), "Signing Out");

                        Cloud.getInstance().signoutUser(

                                new Cloud.ResponseCallback() {
                                    @Override
                                    public void onResponse() {

                                        Utility.getInstance().hideLoadSpinner();

                                        Intent nextScreen = new Intent(getContext(), MainActivity.class);
                                        startActivity(nextScreen);
                                    }
                                },

                                new Cloud.FaultCallback() {
                                    @Override
                                    public void onFault(String code, String message) {

                                        Utility.getInstance().hideLoadSpinner();
                                        Utility.getInstance().showErrorDialog(ProfileFragment.this.getContext(), "Sign Out Error", message);
                                    }
                                });
                    }
                });
            }
        });

        Button leaveEvent = (Button) rootView.findViewById(R.id.btn_profile_leave);

        if(Utility.getInstance().getVenueObjectId() == null) {
            // TODO: The button doens't look disabled!
            leaveEvent.setEnabled(false);
        }

        leaveEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Utility.getInstance().showOkCancelDialog(ProfileFragment.this.getContext(), "Leave Event",
                        "Are you sure you wish to leave this event?",
                        new Utility.OkCallback() {
                            @Override
                            public void onOk() {

                                Utility.getInstance().showLoadSpinner(ProfileFragment.this.getContext(), "Leaving");

                                Cloud.getInstance().removeVenueCodeFromUser(new Cloud.ResponseCallback() {
                                    @Override
                                    public void onResponse() {

                                        Utility.getInstance().leaveCurrentEvent();

                                        Utility.getInstance().hideLoadSpinner();

                                        //getNavigationHostActivity().selectTabAtPosition(0);

                                        //MainNavActivity.this.selectTabAtPosition(0);

                                    }
                                }, new Cloud.FaultCallback() {
                                    @Override
                                    public void onFault(String code, String message) {
                                        Utility.getInstance().hideLoadSpinner();
                                    }
                                });
                            }
                        });
            }
        });

        mLocation = (SwitchCompat) rootView.findViewById(R.id.sw_sendmsg_target_vendor);

        mOnCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if(b == false) {

                    // Turn it off!

                    // If the user has granted the permission in the past but now wants to disable it,
                    // prompt them to switch it off manually in the App's info panel.
                    if(EasyPermissions.hasPermissions(getContext(), new String[] {Manifest.permission.ACCESS_FINE_LOCATION})) {

                        Utility.getInstance().showOkCancelDialog(getContext(), "Disable Location",
                                "Android requires that Location be disabled manually from the App's info panel.", new Utility.OkCallback() {
                                    @Override
                                    public void onOk() {
                                        Utility.getInstance().startInstalledAppDetailsActivity();
                                    }
                                }, new Utility.CancelCallback() {
                                    @Override
                                    public void onCancel() {
                                        setLocationChecked(true);
                                    }
                                });
                    }

                } else {

                    // Turn it on!
                    promptForPermissions();
                }
            }
        };

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //Log.e("XXX", "requestCode = " + String.valueOf(requestCode));

        if(requestCode == 21356) {

            EasyImage.handleActivityResult(requestCode, resultCode, data, getActivity(), new DefaultCallback() {
                @Override
                public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {

                    Log.e(TAG, "Failed to pick new profile image: " + e.getLocalizedMessage());
                    Log.e(TAG, e.getLocalizedMessage());
                }

                @Override
                public void onImagesPicked(List<File> imagesFiles, EasyImage.ImageSource source, int type) {

                    Uri uri = Uri.fromFile(imagesFiles.get(0)); // We only care about the first image!

                    // https://github.com/ArthurHub/Android-Image-Cropper
                    CropImage.activity(uri)
                            .setGuidelines(CropImageView.Guidelines.ON)
                            .setAspectRatio(1,1)
                            .setFixAspectRatio(true)
                            .start(getActivity(), ProfileFragment.this);
                }
            });

        } else if(requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if(resultCode == RESULT_OK) {

                Uri resultUri = result.getUri();

                try {

                    mProfileImageChanged = true;

                    Bitmap fullSizeImage = MediaStore.Images.Media.getBitmap(CandCApplication.getAppContext().getContentResolver(), resultUri);

                    if(fullSizeImage.getWidth() > 1024) {
                        // Don't let users upload any images over 1024x1024 square.
                        // The UI never needs more than this.
                        mProfileBitmap = ThumbnailUtils.extractThumbnail(fullSizeImage, 1024, 1024);
                    } else {
                        mProfileBitmap = fullSizeImage;
                    }

                    Drawable drawable = Utility.getInstance().createRoundDrawableFromBitmap(mProfileBitmap);
                    mProfilePicture.setImageDrawable(drawable);

                } catch(IOException e) {
                    Log.e(TAG, "Failed to create Bitmap for new profile image: " + e.getLocalizedMessage());
                }

            } else if(resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {

                Exception e = result.getError();
                Log.e(TAG, "Failed to crop to new profile image: " + e.getLocalizedMessage());
            }
        }
    }

    private void setLocationChecked(boolean checked) {

        // Manually change the checked state of the Switch but prevent the listener from firing.
        mLocation.setOnCheckedChangeListener(null);
        mLocation.setChecked(checked);
        mLocation.setOnCheckedChangeListener(mOnCheckedChangeListener);
    }

    @Override
    public void onResume() {

        super.onResume();

        if(EasyPermissions.hasPermissions(getContext(), new String[] {Manifest.permission.ACCESS_FINE_LOCATION})) {
            setLocationChecked(true);
        } else {
            setLocationChecked(false);
        }

        getNavigationHostActivity().setOnToolbarItemSelectedListener(R.id.it_toolbar_save, new NavigationHostActivity.OnToolbarItemSelectedListener() {
            @Override
            public void onToolbarItemSelected() {

                if(!mProfileTextChanged && !mProfileImageChanged) {
                    return;
                }

                if(mUserName.getText().toString().matches("")) {
                    Utility.getInstance().showOkDialog(getContext(), "Save Error", "Please enter a name.");
                    return;
                }

                if(!mUserWorkEmail.getText().toString().matches("") && !Utility.getInstance().isValidEmail(mUserWorkEmail.getText())) {
                    Utility.getInstance().showOkDialog(getContext(), "Save Error", "Please enter a valid email address.");
                    return;
                }

                Utility.getInstance().showLoadSpinner(getContext(),  "Saving Profile");

                Cloud.getInstance().setUserName(mUserName.getText().toString());
                Cloud.getInstance().setUserWorkEmail(mUserWorkEmail.getText().toString());
                Cloud.getInstance().setUserWorkTitle(mUserWorkTitle.getText().toString());

                Cloud.getInstance().saveUser(new Cloud.ResponseCallback() {
                    @Override
                    public void onResponse() {
                        Utility.getInstance().hideLoadSpinner();
                        mProfileTextChanged = false;

                        if(mProfileImageChanged && mProfileBitmap != null) {

                            Utility.getInstance().hideLoadSpinner();
                            Utility.getInstance().showLoadSpinner(ProfileFragment.this.getContext(), "Saving Image");

                            Cloud.getInstance().uploadProfileImage(mProfileBitmap, new Cloud.ResponseImageCallback() {
                                @Override
                                public void onResponse(Drawable drawable) {

                                    mProfilePicture.setImageDrawable(drawable);

                                    mProfileImageChanged = false;
                                    mProfileBitmap = null;

                                    Utility.getInstance().hideLoadSpinner();

                                    Cloud.getInstance().updateUserDataOnChatSessions();
                                }
                            }, new Cloud.FaultCallback() {
                                @Override
                                public void onFault(String code, String message) {
                                    Utility.getInstance().hideLoadSpinner();
                                }
                            });
                        } else {
                            Utility.getInstance().hideLoadSpinner();

                            Cloud.getInstance().updateUserDataOnChatSessions();
                        }

                    }
                }, new Cloud.FaultCallback() {
                    @Override
                    public void onFault(String code, String message) {
                        Utility.getInstance().hideLoadSpinner();
                        Utility.getInstance().showErrorDialog(getContext(), "Save Error", message);
                    }
                });
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @AfterPermissionGranted(RC_LOCATION_PERM)
    private void promptForPermissions() {

        String[] perms = { Manifest.permission.ACCESS_FINE_LOCATION};

        if(EasyPermissions.hasPermissions(getContext(), perms)) {

            // We already have the permission, so just do what we want!
            if(Utility.getInstance().getVenueCode() != null) {

                //Cloud.getInstance().startGeofenceMonitoring(getContext() ,Utility.getInstance().getVenueCode());
                Cloud.getInstance().startGeofenceMonitoring(getContext());
            }

        } else {

            // We don't have the permission, so ask for it.
            EasyPermissions.requestPermissions(this, getString(R.string.rationale_location),
                    RC_LOCATION_PERM, perms);
        }
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

        Log.d(TAG, "onPermissionsGranted:" + requestCode + ":" + perms.size());
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

        Log.d(TAG, "onPermissionsDenied:" + requestCode + ":" + perms.size());

        // If permission was denied because the user manually disabled it in the past,
        // prompt them to switch it back on manually in the App's info panel.
        if(EasyPermissions.hasPermissions(getContext(), new String[] {Manifest.permission.ACCESS_FINE_LOCATION})) {

            Utility.getInstance().showOkCancelDialog(getContext(), "Enable Location",
                    "Android requires that Location be enabled manually from the App's info panel.", new Utility.OkCallback() {
                        @Override
                        public void onOk() {
                            Utility.getInstance().startInstalledAppDetailsActivity();
                        }
                    }, new Utility.CancelCallback() {
                        @Override
                        public void onCancel() {

                            setLocationChecked(false);
                        }
                    });
        } else {
            setLocationChecked(false);
        }
    }
}