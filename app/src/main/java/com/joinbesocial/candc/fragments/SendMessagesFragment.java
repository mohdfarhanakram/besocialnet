package com.joinbesocial.candc.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker;
import com.joinbesocial.candc.Cloud;
import com.joinbesocial.candc.R;
import com.joinbesocial.candc.Utility;
import com.joinbesocial.candc.adapters.VenuItemsBottomSheetAdapter;
import com.joinbesocial.candc.models.ScheduledMessage;
import com.joinbesocial.candc.models.VenueItem;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

public class SendMessagesFragment extends BaseFragment  implements VenuItemsBottomSheetAdapter.ItemListener {

    private final static String TAG = ReportsFragment.class.getSimpleName();

    private Button mSendPushBtn;

    private SwitchCompat mScheduleTimeSwitch;
    private SwitchCompat mTargetVendorSwitch;

    private Button mSetTimeBtn;
    private Button mSetVendorBtn;
    private Button mManageMessagesBtn;

    private TextView mTimeTextView;
    private TextView mVendorTextView;

    private ArrayList<VenueItem> mVenueItems = null;
    private Date mDate = null;
    private VenueItem mVenueItem = null;
    private EditText mMessageEdit;
    private TextView mRemainingText;

    BottomSheetBehavior mDatePickerBottomSheet;
    // https://github.com/florent37/SingleDateAndTimePicker
    private com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker mDatePicker;

    BottomSheetBehavior mVenutItemsBottomSheet;
    RecyclerView mVenueItemRecyclerView;
    private VenuItemsBottomSheetAdapter mAdapter;
    CoordinatorLayout mCoordinatorLayout;

    private final TextWatcher mTextWatcher = new TextWatcher() {

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

        public void onTextChanged(CharSequence s, int start, int before, int count) {

            mRemainingText.setText("Remaining: " + String.valueOf(500 - s.length()));
        }

        public void afterTextChanged(Editable s) {}
    };
    public static SendMessagesFragment newInstance(int instance) {

        Bundle args = new Bundle();
        args.putInt(ARGS_INSTANCE, instance);
        SendMessagesFragment fragment = new SendMessagesFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getNavigationHostActivity().setNavTitle("Reports");

        View rootView = inflater.inflate(R.layout.fragment_send_messages, container, false);

        mTimeTextView = (TextView) rootView.findViewById(R.id.tv_sendmsg_time);
        mVendorTextView = (TextView) rootView.findViewById(R.id.tx_bsheet_venue_item_title);

        mDatePicker = (com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker) rootView.findViewById(R.id.sdtp_bsheet_date_picker);
        mDatePicker.setMustBeOnFuture(true);
        mDatePicker.setStepMinutes(1);
        mDatePicker.setListener(new SingleDateAndTimePicker.Listener() {
            @Override
            public void onDateChanged(String displayed, Date date) {

                mDate = date;

                SimpleDateFormat sdf = new SimpleDateFormat("h:mm aa z\nMMM d, yyyy");
                sdf.setTimeZone(TimeZone.getDefault());
                String formatedDate = sdf.format(mDate);

                mTimeTextView.setText(formatedDate);
            }
        });

        mScheduleTimeSwitch = (SwitchCompat) rootView.findViewById(R.id.sw_sendmsg_schedule_time);
        mScheduleTimeSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if(b == false) {
                    mDate = null;
                    mSetTimeBtn.setEnabled(false);
                    mTimeTextView.setText(getResources().getString(R.string.send_immediately));
                    mDatePickerBottomSheet.setState(BottomSheetBehavior.STATE_COLLAPSED);
                } else {
                    mSetTimeBtn.setEnabled(true);
                }
            }
        });

        mTargetVendorSwitch = (SwitchCompat) rootView.findViewById(R.id.sw_sendmsg_target_vendor);
        mTargetVendorSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if(b == false) {
                    mVenueItem = null;
                    mSetVendorBtn.setEnabled(false);
                    mVendorTextView.setText(getResources().getString(R.string.none_specified));
                    mVenutItemsBottomSheet.setState(BottomSheetBehavior.STATE_COLLAPSED);
                } else {
                    mSetVendorBtn.setEnabled(true);
                }
            }
        });

        mSetTimeBtn = (Button) rootView.findViewById(R.id.btn_sendmsg_set_time);
        mSetTimeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mDatePicker.setMinDate(new Date());
                mDatePickerBottomSheet.setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });

        mVenueItemRecyclerView = (RecyclerView) rootView.findViewById(R.id.rv_bsheet_venue_item);
        mVenueItemRecyclerView.setHasFixedSize(true);
        mVenueItemRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        mSetVendorBtn = (Button) rootView.findViewById(R.id.btn_sendmsg_set_vendor);
        mSetVendorBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(mVenueItems == null) {

                    Utility.getInstance().showLoadSpinner(getContext(), "Loading");

                    Cloud.getInstance().loadVenueItems(new Cloud.VenueItemsResponseCallback() {
                        @Override
                        public void onData(ArrayList<VenueItem> foundVenueItems) {

                            mVenueItems = foundVenueItems;
                            mAdapter = new VenuItemsBottomSheetAdapter(mVenueItems, SendMessagesFragment.this);
                            mVenueItemRecyclerView.setAdapter(mAdapter);
                            Utility.getInstance().hideLoadSpinner();

                            mVenutItemsBottomSheet.setState(BottomSheetBehavior.STATE_EXPANDED);
                        }
                    }, new Cloud.FaultCallback() {
                        @Override
                        public void onFault(String code, String message) {

                            Utility.getInstance().hideLoadSpinner();
                            Utility.getInstance().showErrorDialog(getContext(), "Load Error", message);
                        }
                    });
                } else {
                    mVenutItemsBottomSheet.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
            }
        });

        mSendPushBtn = (Button) rootView.findViewById(R.id.btn_sendmsg_send_message);
        mSendPushBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(mMessageEdit.getText().toString().matches("")) {
                    Utility.getInstance().showOkDialog(getContext(), "Message Error", "Please enter a message to broadcast.");
                    return;
                }

                String dialogMessage = "Are you sure you wish to send this message to all visitors of the venue?";

                if(mDate != null) {
                    if(mDate.before(new Date())) {
                        Utility.getInstance().showOkDialog(getContext(), "Message Error", "Scheduled date to send message has already past. Please set a new date for delivery.");
                        return;
                    }
                    dialogMessage = "Are you sure you wish to schedule this message for all visitors of the venue?";
                }

                Utility.getInstance().showOkCancelDialog(getContext(), "Verify Message", dialogMessage,
                        new Utility.OkCallback() {
                            @Override
                            public void onOk() {

                                String spinnerMessage = "Sending Message";

                                if(mDate != null) {
                                    spinnerMessage = "Scheduling Message";
                                }

                                Utility.getInstance().showLoadSpinner(getContext(), spinnerMessage);

                                String objectId = null;
                                String title = null;
                                if(mVenueItem != null) {
                                    objectId = mVenueItem.getObjectId();
                                    title = mVenueItem.getTitle();
                                }

                                final String venueTitle = title;

                                Cloud.getInstance().sendBroadcastPush(mMessageEdit.getText().toString(), mDate, objectId, title,
                                    new Cloud.MessageResponseCallback() {
                                        @Override
                                        public void onResponse(String messageId) {

                                            if(mDate != null) {

                                                ScheduledMessage scheduledMessage =
                                                        new ScheduledMessage(Utility.getInstance().getVenueCode(),
                                                                messageId,
                                                                mMessageEdit.getText().toString(),
                                                                mDate,
                                                                venueTitle);

                                                Cloud.getInstance().saveScheduledMessage(scheduledMessage, new Cloud.SaveScheduledMessageResponseCallback() {
                                                    @Override
                                                    public void onData(ScheduledMessage scheduledMessage) {

                                                        Utility.getInstance().setHostsLatestVenueMessage(mMessageEdit.getText().toString());
                                                        Utility.getInstance().hideLoadSpinner();
                                                    }
                                                }, new Cloud.FaultCallback() {
                                                    @Override
                                                    public void onFault(String code, String message) {
                                                        Utility.getInstance().setHostsLatestVenueMessage(mMessageEdit.getText().toString());
                                                        Utility.getInstance().hideLoadSpinner();
                                                    }
                                                });
                                            } else {

                                                Utility.getInstance().setHostsLatestVenueMessage(mMessageEdit.getText().toString());
                                                Utility.getInstance().hideLoadSpinner();
                                            }



                                        }
                                    }, new Cloud.FaultCallback() {
                                        @Override
                                        public void onFault(String code, String message) {
                                            Utility.getInstance().hideLoadSpinner();
                                            Utility.getInstance().showErrorDialog(getContext(), "Message Error", message);
                                        }
                                    });

                            }
                        }, new Utility.CancelCallback() {
                            @Override
                            public void onCancel() {
                            }
                        });
                }
            });

        mManageMessagesBtn = (Button) rootView.findViewById(R.id.btn_sendmsg_manage_messages);
        mManageMessagesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ManageMessagesFragment fragment = new ManageMessagesFragment();
                getNavigationHostActivity().pushFragment(fragment);
            }
        });

        mMessageEdit = (EditText)  rootView.findViewById(R.id.et_sendmsg_mesage);
        mRemainingText = (TextView) rootView.findViewById(R.id.tv_sendmsg_remaining);

        String lastMessage = Utility.getInstance().getHostsLatestVenueMessage();

        if(lastMessage != null) {
            mMessageEdit.setText(lastMessage);
            mRemainingText.setText("Remaining: " + String.valueOf(500 - lastMessage.length()));
        }

        mMessageEdit.addTextChangedListener(mTextWatcher);

// TODO: Research this issue and see if this can be done in a ConstraintLayout.
        // Force the edit text for the message to match the same width as the send button.
        // There's no good way to do this in a ConstraintLayout.
        final View finalRootView = rootView;
        ViewTreeObserver vto = mSendPushBtn.getViewTreeObserver();
        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {

            public boolean onPreDraw() {

                mSendPushBtn.getViewTreeObserver().removeOnPreDrawListener(this);
                //Log.d("XXX", "Height: " + mSendPushBtn.getMeasuredHeight() + " Width: " + mSendPushBtn.getMeasuredWidth());

                int width = mSendPushBtn.getMeasuredWidth();

                ConstraintLayout layout = (ConstraintLayout) finalRootView.findViewById(R.id.cl_reports);
                ConstraintSet set = new ConstraintSet();
                set.clone(layout);
                set.constrainWidth(R.id.et_sendmsg_mesage, width);
                set.applyTo(layout);

                return true;
            }
        });

        mCoordinatorLayout = (CoordinatorLayout) rootView.findViewById(R.id.crdl_sendmsg);

        //
        // Bottom Sheet for Date/Time Picker...
        //

        Button doneBtn = (Button) rootView.findViewById(R.id.btn_bsheet_date_picker_done);
        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(mDate != null) {

                    SimpleDateFormat sdf = new SimpleDateFormat("h:mm aa z 'on' MMM d, yyyy");
                    sdf.setTimeZone(TimeZone.getDefault());
                    String formatedDate = sdf.format(mDate);

                    Snackbar.make(mCoordinatorLayout, formatedDate.toString(), Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                } else {
                    Snackbar.make(mCoordinatorLayout, "Not date set...", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }

                mDatePickerBottomSheet.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });

        //
        // Bottom Sheet for Vendor List...
        //

        View bottomSheetVenueItems = rootView.findViewById(R.id.cl_bsheet_venue_items);
        mVenutItemsBottomSheet = BottomSheetBehavior.from(bottomSheetVenueItems);
        mVenutItemsBottomSheet.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                // React to state change
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });

        View  datePickerBottomSheet = rootView.findViewById(R.id.cl_bsheet_date_picker);
        mDatePickerBottomSheet = BottomSheetBehavior.from(datePickerBottomSheet);
        mDatePickerBottomSheet.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                // React to state change
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        mVenueItems = null;
    }

    @Override
    public void onItemClick(VenueItem venueItem) {

        mVenueItem = venueItem;

        if(mVenueItem != null) {
            Snackbar.make(mCoordinatorLayout, "Selected: " + mVenueItem.getTitle(), Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();

            mVendorTextView.setText(mVenueItem.getTitle());
        } else {
            mVendorTextView.setText(getResources().getString(R.string.none_specified));
        }

        mVenutItemsBottomSheet.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }
}