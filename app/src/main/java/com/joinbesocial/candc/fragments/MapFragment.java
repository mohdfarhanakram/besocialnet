package com.joinbesocial.candc.fragments;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.backendless.geo.GeoPoint;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.joinbesocial.candc.Cloud;
import com.joinbesocial.candc.R;
import com.joinbesocial.candc.Utility;
import com.joinbesocial.candc.models.Venue;
import com.joinbesocial.candc.models.VenueItem;

import java.util.List;

public class MapFragment extends BaseFragment {

    private final static String TAG = MapFragment.class.getSimpleName();

    private Venue mVenue;
    private VenueItem mVenueItem;

    private MapView mMapView;
    private GoogleMap mGoogleMap;

    //private final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if(mVenue != null) {
            getNavigationHostActivity().setNavTitle(mVenue.getTitle());
        } else if(mVenueItem != null) {
            getNavigationHostActivity().setNavTitle(mVenueItem.getTitle());
        }

        View rootView = inflater.inflate(R.layout.fragment_map, container, false);

        mMapView = (MapView) rootView.findViewById(R.id.mv_map);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume(); // Needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap map) {

                mGoogleMap = map;

//TODO: https://github.com/00ec454/Ask

//                if(ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
//                        != PackageManager.PERMISSION_GRANTED) {
//
//                    // Should we show an explanation?
//                    if(shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
//
//                        // Show an explanation to the user *asynchronously* -- don't block
//                        // this thread waiting for the user's response! After the user
//                        // sees the explanation, try again to request the permission.
//                    } else {
//
//                        requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
//                                MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
//                    }
//                } else {
//
//                    // For showing a move to my location button
//                    mGoogleMap.setMyLocationEnabled(true);
//                }


                GeoPoint location = null;
                String title = "";
                String subTitle = "";
                String address = "";

                if(mVenue != null) {
                    location = mVenue.getLocation();
                    title = mVenue.getTitle();
                    subTitle = mVenue.getSubTitle();
                    address = mVenue.getAddress();
                } else if(mVenueItem != null) {
                    location = mVenueItem.getLocation();
                    title = mVenueItem.getTitle();
                    subTitle = mVenueItem.getSubTitle();
                    address = mVenueItem.getAddress();
                }

                if(location != null) {
                    // If we have a lat/long - use it.
                    LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                    addMarkerUsingLatLng(latLng, title, subTitle);

                } else {

                    // If not - use address instead!
                    if(address != null) {
                        String addressCleaned = address.replace("\\n", ", ");
                        addMarkerUsingAddress(addressCleaned, title, subTitle);
                    }
                }
            }
        });

        return rootView;
    }

    private void addMarkerUsingLatLng(LatLng latLng, String title, String subTitle) {

        final Marker marker = mGoogleMap.addMarker(new MarkerOptions().position(latLng).title(title).snippet(subTitle));

        // Zoom automatically to the location of the marker.
        CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(12).build();
        mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        mGoogleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                marker.showInfoWindow();
            }
        });
    }

    private void addMarkerUsingAddress(final String address, final String title, final String subTitle) {

        LatLng latLng = Utility.getInstance().getLatLngFromAddressUsingGeocoder(getContext(), address);

        if(latLng == null) {

            Cloud.getInstance().getLatLngFromAddressUsingGoogle(address, new Cloud.LatLngCallback() {
                @Override
                public void onData(LatLng latLng) {
                    addMarkerUsingLatLng(latLng, title, subTitle);
                }
            }, new Cloud.FaultCallback() {
                @Override
                public void onFault(String code, String message) {
// TODO:
                }
            });

            return;
        }

        addMarkerUsingLatLng(latLng, title, subTitle);
    }

//    @Override
//    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
//
//        switch(requestCode) {
//
//            case MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
//
//                // If request is cancelled, the result arrays are empty.
//                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//
//                    if(ContextCompat.checkSelfPermission(MapFragment.this.getActivity(),
//                            Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
//                        mGoogleMap.setMyLocationEnabled(true);
//                    }
//
//                } else {
//
//                    // Permission denied! Disable the functionality that depends on this permission.
//                }
//
//                return;
//            }
//        }
//    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    public Venue getEvent() {
        return mVenue;
    }

    public void setVenue(Venue mVenue) {
        this.mVenue = mVenue;
    }

    public VenueItem getVendor() {
        return mVenueItem;
    }

    public void setVendor(VenueItem mVenueItem) {
        this.mVenueItem = mVenueItem;
    }
}