package com.joinbesocial.candc.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.backendless.BackendlessUser;
import com.joinbesocial.candc.CloudAnalytics;
import com.joinbesocial.candc.Utility;
import com.joinbesocial.candc.activities.ImageViewerActivity;
import com.joinbesocial.candc.models.ChatMessage;
import com.joinbesocial.candc.adapters.ChatMessageAdapter;
import com.joinbesocial.candc.models.ChatSession;
import com.joinbesocial.candc.Cloud;
import com.joinbesocial.candc.models.FBChat;
import com.joinbesocial.candc.R;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

public class ChatFragment extends BaseFragment {

    private final static String TAG = ChatFragment.class.getSimpleName();

    private SwipeRefreshLayout swipeContainer;

    public BackendlessUser mBackendlessUser;
    private ChatSession mChatSession;

    private Bitmap mPhotoBitmap;
    private Bitmap mPreviewBitmap;
    private int mEditWidth = 500; // Assume 500 until we know the real width.
    private boolean mReturningFromKnownActivity = false;

    HashMap<String, FBChat> chats = new HashMap<>();

    private RecyclerView mRecyclerView;
    private Button mButtonSend;
    private EditText mEditTextMessage;
    private ImageView mImageView;
    private ChatMessage mTypingIndicatorMessage = null;
    private boolean typingStarted = false;

    private ChatMessageAdapter mAdapter;

    private static final int RESULT_FROM_IMAGE_PICKER = 1;
    private static final int RESULT_FROM_IMAGE_VIEWER = 2;

    public static ChatFragment newInstance(int instance) {

        Bundle args = new Bundle();
        args.putInt(ARGS_INSTANCE, instance);
        ChatFragment fragment = new ChatFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_chat, container, false);

        //
        // Setup SwipeRefreshLayout for pull-to-refresh...
        //

        swipeContainer = (SwipeRefreshLayout) rootView.findViewById(R.id.srl_chat);
        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadChats(false);
            }
        });

        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        //
        // Setup the RecyclerView for our data...
        //

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.rv_chat);
        mButtonSend = (Button) rootView.findViewById(R.id.btn_chat_send);
        mEditTextMessage = (EditText) rootView.findViewById(R.id.et_chat_message);
        mImageView = (ImageView) rootView.findViewById(R.id.iv_chat_image);

        mButtonSend.setEnabled(false);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        mAdapter = new ChatMessageAdapter(getContext(), this, new ArrayList<ChatMessage>());
        mRecyclerView.setAdapter(mAdapter);

        // We nned to find out what the final width of the edit text will be so we can scale the
        // preview images to it.
        ViewTreeObserver vto = mEditTextMessage.getViewTreeObserver();
        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {

            public boolean onPreDraw() {
                mEditTextMessage.getViewTreeObserver().removeOnPreDrawListener(this);
                mEditWidth = mEditTextMessage.getMeasuredWidth();
                return true;
            }
        });

        mButtonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mPhotoBitmap != null) {
                    sendImageOnlyMessage();
                } else {
                    sendTextOnlyMessage();
                }
            }
        });

        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchPhotoPicker();
            }
        });

        mEditTextMessage.addTextChangedListener(new TextWatcher() {

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void afterTextChanged(Editable s) {

                boolean hasImage = hasImageSpan(mEditTextMessage);

                if(!TextUtils.isEmpty(s.toString()) || hasImage) {

                    typingStarted = true;
                    mButtonSend.setEnabled(true);

                    if(mChatSession != null && mChatSession.getObjectId() != null) {
                        Cloud.getInstance().setTypingIndicator(true);
                    }

                } else if(TextUtils.isEmpty(s.toString()) && hasImage == false) {

                    if(mPreviewBitmap != null) {
                        mPreviewBitmap.recycle();
                        mPreviewBitmap = null;
                    }

                    if(mPhotoBitmap != null) {
                        mPhotoBitmap.recycle();
                        mPhotoBitmap = null;
                    }

                    mButtonSend.setEnabled(false);

                    if(typingStarted) {
                        typingStarted = false;
                        if(mChatSession != null && mChatSession.getObjectId() != null) {
                            Cloud.getInstance().setTypingIndicator(false);
                        }
                    }
                }
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        // If we're resuming from being covered by a known intent that doesn't require a reload of
        // the messages then just return and do nothing!
        if(mReturningFromKnownActivity) {
            mReturningFromKnownActivity = false;
            return;
        }

        if(mChatSession != null) {

            Utility.getInstance().showLoadSpinner(this.getContext(), "Loading Messages");

            Cloud.getInstance().loadChatSession(mChatSession.getObjectId(), new Cloud.ChatSessionResponseCallback() {
                @Override
                public void onData(ChatSession chatSession) {

                    mChatSession = chatSession;

                    if(mChatSession != null) {

                        if(mChatSession.getOwnerId().equals(Cloud.getInstance().getUserObjectId())) {
                            getNavigationHostActivity().setNavTitle(mChatSession.getTargetName());
                        } else {
                            getNavigationHostActivity().setNavTitle(mChatSession.getOwnerName());
                        }

                        markChatSessionViewedIfRequired();

                        Utility.getInstance().hideLoadSpinner();

                        addTypingListener();
                        loadChats(true);
                    } else {
                        Utility.getInstance().hideLoadSpinner();
                    }
                }
            }, new Cloud.FaultCallback() {
                @Override
                public void onFault(String code, String message) {
                    Utility.getInstance().hideLoadSpinner();
                }
            });

        } else if(mBackendlessUser != null) {

            getNavigationHostActivity().setNavTitle(mBackendlessUser.getProperty("name").toString());

            Utility.getInstance().showLoadSpinner(this.getContext(), "Loading Messages");

            Cloud.getInstance().loadChatSessionWithUser(mBackendlessUser.getObjectId(), new Cloud.ChatSessionResponseCallback() {
                @Override
                public void onData(ChatSession chatSession) {

                    mChatSession = chatSession;

                    if(mChatSession != null) {

                        if(mChatSession.getOwnerId().equals(Cloud.getInstance().getUserObjectId())) {
                            getNavigationHostActivity().setNavTitle(mChatSession.getTargetName());
                        } else {
                            getNavigationHostActivity().setNavTitle(mChatSession.getOwnerName());
                        }

                        markChatSessionViewedIfRequired();

                        Utility.getInstance().hideLoadSpinner();

                        addTypingListener();
                        loadChats(true);
                    } else {
                        Utility.getInstance().hideLoadSpinner();
                    }
                }
            }, new Cloud.FaultCallback() {
                @Override
                public void onFault(String code, String message) {
                    Utility.getInstance().hideLoadSpinner();
                }
            });
        }
    }

    public ChatSession getChatSession() {
        return mChatSession;
    }

    public void setChatSession(ChatSession chatSession) {
        this.mChatSession = chatSession;
    }

    private void addTypingListener() {

        if(mChatSession.getObjectId() != null) {

            Cloud.getInstance().listenToTypingIndicator(mChatSession, new Cloud.OnTypingCallback() {
                @Override
                public void onTyping(boolean isTyping) {
                    if(isTyping) {
                        if(!typingStarted) {
                            addTypingIndicatorMessage();
                        }
                    } else {
                        removeTypingIndicatorMessage();
                    }
                }
            });
        }
    }

    public boolean hasImageSpan(EditText editText) {

        Editable text  = editText.getEditableText();
        ImageSpan[] spans = text.getSpans(0, text.length(), ImageSpan.class);
        return !(spans.length == 0);
    }

    private void addTypingIndicatorMessage() {

        mTypingIndicatorMessage = new ChatMessage("...", false, false);
        mAdapter.add(mTypingIndicatorMessage);

        mRecyclerView.scrollToPosition(mAdapter.getItemCount() - 1);
    }

    private void removeTypingIndicatorMessage() {

        if(mTypingIndicatorMessage != null) {

            mAdapter.remove(mTypingIndicatorMessage);
            mRecyclerView.scrollToPosition(mAdapter.getItemCount() - 1);
            mTypingIndicatorMessage = null;
        }
    }

    private void loadChats(final boolean showSpinner) {

        if(mChatSession.getObjectId() == null) {
            return;
        }

        if(showSpinner) {
            Utility.getInstance().showLoadSpinner(this.getContext(), "Loading Messages");
        }

        Cloud.getInstance().loadFirebaseChatSession(mChatSession.getObjectId(),

                new Cloud.ChatSessionsDataCallback() {
                    @Override
                    public void onData(ArrayList<FBChat> chatList) {

                        mAdapter.clear();

                        for(FBChat chat : chatList) {
                            chats.put(chat.key, chat);
                            addMessage(chat);
                        }

                        swipeContainer.setRefreshing(false);

                        if(showSpinner) {
                            Utility.getInstance().hideLoadSpinner();
                        }

                        Cloud.getInstance().listenToFirebaseChatSession(mChatSession.getObjectId(),

                                new Cloud.ChatSessionDataCallback() {
                                    @Override
                                    public void onData(FBChat chat) {

                                        if(!chats.containsKey(chat.key)) {

                                            chats.put(chat.key, chat);
                                            addMessage(chat);
                                        }
                                    }
                                },

                                new Cloud.DatabaseErrorCallback() {
                                    @Override
                                    public void onDatabaseError(int code, String message) {

                                        Log.d(TAG, "code = " + String.valueOf(code) + ", message = " + message);
                                    }
                                }
                        );
                    }
                },

                new Cloud.DatabaseErrorCallback() {
                    @Override
                    public void onDatabaseError(int code, String message) {

                        if(showSpinner) {
                            Utility.getInstance().hideLoadSpinner();
                        }

                        Utility.getInstance().showErrorDialog(ChatFragment.this.getContext(), "Load Error", message);
                    }
                }
        );
    }

    private void addMessage(FBChat chat) {

        if(Cloud.getInstance().getUserObjectId().equals(chat.senderId)) {

            if(chat.imageUrl != null) {
                ChatMessage chatMessage = new ChatMessage(chat.imageUrl, true, true);
                mAdapter.add(chatMessage);
            } else {
                ChatMessage chatMessage = new ChatMessage(chat.text, true, false);
                mAdapter.add(chatMessage);
            }

        } else {

            if(mTypingIndicatorMessage != null) {

                if(chat.imageUrl != null) {
                    mTypingIndicatorMessage.setContent(chat.imageUrl);
                    mTypingIndicatorMessage.setIsImage(true);
                } else {
                    mTypingIndicatorMessage.setContent(chat.text);
                }

                mTypingIndicatorMessage = null;

                mAdapter.notifyDataSetChanged();

            } else {

                if(chat.imageUrl != null) {
                    ChatMessage chatMessage = new ChatMessage(chat.imageUrl, false, true);
                    mAdapter.add(chatMessage);
                } else {
                    ChatMessage chatMessage = new ChatMessage(chat.text, false, false);
                    mAdapter.add(chatMessage);
                }
            }
        }

        mRecyclerView.scrollToPosition(mAdapter.getItemCount() - 1);
    }

//    public void forceScrollToBottom() {
//
//        mRecyclerView.scrollToPosition(mAdapter.getItemCount() - 1);
//    }

    private void sendTextOnlyMessage() {

        // Hide Keyboard.
        InputMethodManager inputManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

        final String message = mEditTextMessage.getText().toString();
        if(TextUtils.isEmpty(message)) {
            return;
        }

        if(mChatSession == null && mBackendlessUser != null) {

            Utility.getInstance().showLoadSpinner(getContext(), "Sending Message");

            String name = "";
            String workTitle = "";
            String profileImageThumbnailUrl = "";

            Object nameObj = mBackendlessUser.getProperty("name");
            if(nameObj != null) {
                name = nameObj.toString();
            }

            Object workTitleObj = mBackendlessUser.getProperty("workTitle");
            if(workTitleObj != null) {
                workTitle = workTitleObj.toString();
            }

            Object profileImageThumbnailUrlObj = mBackendlessUser.getProperty("profileImageThumbnailUrl");
            if(profileImageThumbnailUrlObj != null) {
                profileImageThumbnailUrl = profileImageThumbnailUrlObj.toString();
            }

            ChatSession chatSession = new ChatSession(
                    Utility.getInstance().getVenueCode(),
                    Cloud.getInstance().getUserName(),
                    Cloud.getInstance().getUserWorkTitle(),
                    Cloud.getInstance().getUserProfileImageThumbnailUrl(),
                    mBackendlessUser.getObjectId(),
                    name,
                    workTitle,
                    profileImageThumbnailUrl,
                    Cloud.getInstance().getUserMemberOfVenueItemTitle());

            Cloud.getInstance().saveChatSession(chatSession, new Cloud.SaveChatSessionResponseCallback() {
                @Override
                public void onData(ChatSession chatSession) {
                    Utility.getInstance().hideLoadSpinner();

                    mChatSession = chatSession;

                    CloudAnalytics.getInstance().sendEventChatStarted();

                    addTypingListener();
                    loadChats(true);

                    FBChat fbChat = new FBChat(Cloud.getInstance().getUserName(),
                            Cloud.getInstance().getUserObjectId(),
                            message);

                    Cloud.getInstance().sendFirebaseChatMessage(mChatSession.getObjectId(), fbChat, new Cloud.ResponseCallback() {
                        @Override
                        public void onResponse() {
                            Utility.getInstance().hideLoadSpinner();
                            markChatSessionUnViewed();
                            mEditTextMessage.setText("");
                        }
                    }, new Cloud.FaultCallback() {
                        @Override
                        public void onFault(String code, String message) {
                            Utility.getInstance().hideLoadSpinner();
                            Utility.getInstance().showErrorDialog(getContext(), "Sending Message Error", message);
                        }
                    });

                }
            }, new Cloud.FaultCallback() {
                @Override
                public void onFault(String code, String message) {
                    Utility.getInstance().hideLoadSpinner();
                    Utility.getInstance().showErrorDialog(getContext(), "Sending Message Error", message);
                }
            });

        } else {

            Utility.getInstance().showLoadSpinner(getContext(), "Sending Message");

            FBChat fbChat = new FBChat(Cloud.getInstance().getUserName(),
                    Cloud.getInstance().getUserObjectId(),
                    message);

            Cloud.getInstance().sendFirebaseChatMessage(mChatSession.getObjectId(), fbChat, new Cloud.ResponseCallback() {
                @Override
                public void onResponse() {
                    Utility.getInstance().hideLoadSpinner();
                    markChatSessionUnViewed();
                    mEditTextMessage.setText("");
                }
            }, new Cloud.FaultCallback() {
                @Override
                public void onFault(String code, String message) {
                    Utility.getInstance().hideLoadSpinner();
                    Utility.getInstance().showErrorDialog(getContext(), "Sending Message Error", message);
                }
            });
        }
    }

    private void sendImageOnlyMessage() {

        // Hide Keyboard.
        InputMethodManager inputManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

        if(mPhotoBitmap == null) {
            return;
        }

        Utility.getInstance().showLoadSpinner(getContext(), "Sending Image");

        Cloud.getInstance().createFirbaseImage(getContext(), mPhotoBitmap, new Cloud.StorageUploadCallback() {
            @Override
            public void onUpload(final Uri uri) {

                if(mChatSession == null && mBackendlessUser != null) {

                    String name = "";
                    String workTitle = "";
                    String profileImageThumbnailUrl = "";

                    Object nameObj = mBackendlessUser.getProperty("name");
                    if(nameObj != null) {
                        name = nameObj.toString();
                    }

                    Object workTitleObj = mBackendlessUser.getProperty("workTitle");
                    if(workTitleObj != null) {
                        workTitle = workTitleObj.toString();
                    }

                    Object profileImageThumbnailUrlObj = mBackendlessUser.getProperty("profileImageThumbnailUrl");
                    if(profileImageThumbnailUrlObj != null) {
                        profileImageThumbnailUrl = profileImageThumbnailUrlObj.toString();
                    }

                    ChatSession chatSession = new ChatSession(
                            Utility.getInstance().getVenueCode(),
                            Cloud.getInstance().getUserName(),
                            Cloud.getInstance().getUserWorkTitle(),
                            Cloud.getInstance().getUserProfileImageThumbnailUrl(),
                            mBackendlessUser.getObjectId(),
                            name,
                            workTitle,
                            profileImageThumbnailUrl,
                            Cloud.getInstance().getUserMemberOfVenueItemTitle());

                    Cloud.getInstance().saveChatSession(chatSession, new Cloud.SaveChatSessionResponseCallback() {
                        @Override
                        public void onData(ChatSession chatSession) {
                            Utility.getInstance().hideLoadSpinner();

                            mChatSession = chatSession;

                            addTypingListener();
                            loadChats(true);

                            FBChat fbChat = new FBChat(Cloud.getInstance().getUserName(),
                                    Cloud.getInstance().getUserObjectId(),
                                    "", uri.toString());

                            Cloud.getInstance().sendFirebaseChatMessage(mChatSession.getObjectId(), fbChat, new Cloud.ResponseCallback() {
                                @Override
                                public void onResponse() {

                                    if(mPreviewBitmap != null) {
                                        mPreviewBitmap.recycle();
                                        mPreviewBitmap = null;
                                    }

                                    if(mPhotoBitmap != null) {
                                        mPhotoBitmap.recycle();
                                        mPhotoBitmap = null;
                                    }

                                    Utility.getInstance().hideLoadSpinner();
                                    markChatSessionUnViewed();
                                    mEditTextMessage.setText("");
                                }
                            }, new Cloud.FaultCallback() {
                                @Override
                                public void onFault(String code, String message) {
                                    Utility.getInstance().hideLoadSpinner();
                                    Utility.getInstance().showErrorDialog(getContext(), "Sending Image Error", message);
                                }
                            });

                        }
                    }, new Cloud.FaultCallback() {
                        @Override
                        public void onFault(String code, String message) {
                            Utility.getInstance().hideLoadSpinner();
                            Utility.getInstance().showErrorDialog(getContext(), "Sending Image Error", message);
                        }
                    });

                } else {

                    FBChat fbChat = new FBChat(Cloud.getInstance().getUserName(),
                            Cloud.getInstance().getUserObjectId(),
                            "", uri.toString());

                    Cloud.getInstance().sendFirebaseChatMessage(mChatSession.getObjectId(), fbChat, new Cloud.ResponseCallback() {
                        @Override
                        public void onResponse() {

                            if(mPreviewBitmap != null) {
                                mPreviewBitmap.recycle();
                                mPreviewBitmap = null;
                            }

                            if(mPhotoBitmap != null) {
                                mPhotoBitmap.recycle();
                                mPhotoBitmap = null;
                            }

                            Utility.getInstance().hideLoadSpinner();
                            markChatSessionUnViewed();
                            mEditTextMessage.setText("");
                        }
                    }, new Cloud.FaultCallback() {
                        @Override
                        public void onFault(String code, String message) {
                            Utility.getInstance().hideLoadSpinner();
                            Utility.getInstance().showErrorDialog(getContext(), "Sending Image Error", message);
                        }
                    });
                }
            }
        }, new Cloud.StorageErrorCallback() {
            @Override
            public void onStorageErrorCallback(String message) {
                Utility.getInstance().hideLoadSpinner();
                Utility.getInstance().showErrorDialog(getContext(), "Sending Image Error", message);
            }
        });
    }

    private void sendPush() {

        if(mBackendlessUser != null) {

            String deviceId = mBackendlessUser.getProperty("deviceId").toString();
            Cloud.getInstance().sendChatPushNotification(deviceId, mChatSession.getObjectId());

        } else {

            String userObjectId;

            if(mChatSession.getOwnerId().equals(Cloud.getInstance().getUserObjectId())) {
                userObjectId = mChatSession.getTargetId();
            } else {
                userObjectId = mChatSession.getOwnerId();
            }

            if(userObjectId != null) {

                Cloud.getInstance().loadUser(userObjectId, new Cloud.UserResponseCallback() {
                    @Override
                    public void onData(BackendlessUser user) {

                        mBackendlessUser = user; // Cache this just in case we send a lot of messages in one session.

                        String deviceId = mBackendlessUser.getProperty("deviceId").toString();
                        Cloud.getInstance().sendChatPushNotification(deviceId, mChatSession.getObjectId());

                    }
                }, new Cloud.FaultCallback() {
                    @Override
                    public void onFault(String code, String message) {
                        // If everything else went well and only the push failed - just ignore it.
                    }
                });
            }
        }
    }

    private void markChatSessionUnViewed() {

        if(mChatSession != null) {

            if(mChatSession.getOwnerId().equals(Cloud.getInstance().getUserObjectId())) {

                // This means I'm the owner so mark the target as unviewed!
                mChatSession.setTargetViewed(false);

                Cloud.getInstance().saveChatSession(mChatSession, new Cloud.SaveChatSessionResponseCallback() {
                    @Override
                    public void onData(ChatSession chatSession) {
                        sendPush();
                    }
                }, new Cloud.FaultCallback() {
                    @Override
                    public void onFault(String code, String message) {
                        // It failed - so back to true!
                        mChatSession.setTargetViewed(true);
                    }
                });

            }  else {

                // This means I'm the target so mark the owner as unviewed!
                mChatSession.setOwnerViewed(false);

                Cloud.getInstance().saveChatSession(mChatSession, new Cloud.SaveChatSessionResponseCallback() {
                    @Override
                    public void onData(ChatSession chatSession) {
                        sendPush();
                    }
                }, new Cloud.FaultCallback() {
                    @Override
                    public void onFault(String code, String message) {
                        // It failed - so back to true!
                        mChatSession.setOwnerViewed(true);
                    }
                });
            }
        }
    }

    private void markChatSessionViewedIfRequired() {

        if(mChatSession != null) {

            if(mChatSession.getOwnerId().equals(Cloud.getInstance().getUserObjectId())) {

                // This means I'm the owner!
                if(!mChatSession.isOwnerViewed()) {

                    mChatSession.setOwnerViewed(true);

                    Cloud.getInstance().saveChatSession(mChatSession, new Cloud.SaveChatSessionResponseCallback() {
                        @Override
                        public void onData(ChatSession chatSession) {
                            // It was set - update our badge count if needed.
                            updateMessagesTabBadgeCount();
                        }
                    }, new Cloud.FaultCallback() {
                        @Override
                        public void onFault(String code, String message) {
                            // It failed - so back to false!
                            mChatSession.setOwnerViewed(false);
                        }
                    });
                }

            } else {

                // This means I'm the target!
                if(!mChatSession.isTargetViewed()) {

                    mChatSession.setTargetViewed(true);

                    Cloud.getInstance().saveChatSession(mChatSession, new Cloud.SaveChatSessionResponseCallback() {
                        @Override
                        public void onData(ChatSession chatSession) {
                            // It was set - update our badge count if needed.
                            updateMessagesTabBadgeCount();
                        }
                    }, new Cloud.FaultCallback() {
                        @Override
                        public void onFault(String code, String message) {
                            // It failed - so back to false!
                            mChatSession.setTargetViewed(false);
                        }
                    });
                }
            }
        }
    }

    private void updateMessagesTabBadgeCount() {

        int messagesBadgeCount = Utility.getInstance().getMessagesBadgeCount();
        if(messagesBadgeCount > 0) {

            --messagesBadgeCount;

            if(messagesBadgeCount > 0) {
                getNavigationHostActivity().setBadgeCountForTabAtPosition(messagesBadgeCount, 1);
            } else {
                getNavigationHostActivity().setBadgeCountForTabAtPosition(0, 1);
            }

        } else {
            getNavigationHostActivity().setBadgeCountForTabAtPosition(0, 1);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == RESULT_FROM_IMAGE_PICKER || requestCode == RESULT_FROM_IMAGE_VIEWER) {
            mReturningFromKnownActivity = true;
        }

        if(requestCode == RESULT_FROM_IMAGE_PICKER && resultCode == Activity.RESULT_OK && data != null) {

            Utility.getInstance().showLoadSpinner(getContext(), "Loading Preview");

            Uri selectedImageUri = data.getData();

            try {

                InputStream is = getActivity().getContentResolver().openInputStream(selectedImageUri);

                if(is != null) {

                    mPhotoBitmap = BitmapFactory.decodeStream(is);

                    if(mPhotoBitmap != null) {

                        // Insert a preview image into the edit text so the user can see it before sending it.
                        mPreviewBitmap = Utility.getInstance().resizeBitmapToWidth(mPhotoBitmap, mEditWidth);

                        String stringWithImage = " "; // We'll insert an ImageSpan into this empty string.

                        SpannableString spannableString = new SpannableString(stringWithImage);

                        Drawable previewDrawable = new BitmapDrawable(getResources(), mPreviewBitmap);
                        previewDrawable.setBounds(0, 0, previewDrawable.getIntrinsicWidth(), previewDrawable.getIntrinsicHeight());

                        ImageSpan span = new ImageSpan(previewDrawable, stringWithImage, ImageSpan.ALIGN_BASELINE);
                        spannableString.setSpan(span, 0, stringWithImage.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

// TODO: We don't currently support sending text AND an images together. So, don't allow it.
                        mEditTextMessage.setText(spannableString);
                        mButtonSend.setEnabled(true);
                    }

                    Utility.getInstance().hideLoadSpinner();
                }

            } catch(FileNotFoundException e) {

                Utility.getInstance().hideLoadSpinner();
                Utility.getInstance().showErrorDialog(getContext(), "Image Error", "File not found!");
                e.printStackTrace();
            }
        }
    }

    public void launchImageViewer(String imageUrl) {

        Intent intent = new Intent(getContext(), ImageViewerActivity.class);

        Bundle extras = new Bundle();
        extras.putString("imageUrl", imageUrl);
        intent.putExtras(extras);
        startActivityForResult(intent, RESULT_FROM_IMAGE_VIEWER);
    }

    private void launchPhotoPicker() {

// TODO: We don't currently support sending text AND an images together. So, don't allow it.
        mEditTextMessage.setText("");
        mButtonSend.setEnabled(false);

        if(mPreviewBitmap != null) {
            mPreviewBitmap.recycle();
            mPreviewBitmap = null;
        }

        if(mPhotoBitmap != null) {
            mPhotoBitmap.recycle();
            mPhotoBitmap = null;
        }

        Intent intent =  new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, RESULT_FROM_IMAGE_PICKER);
    }
}