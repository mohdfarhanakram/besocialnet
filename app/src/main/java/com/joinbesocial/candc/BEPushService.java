package com.joinbesocial.candc;

import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.backendless.messaging.PublishOptions;
import com.backendless.push.BackendlessPushService;
import com.joinbesocial.candc.activities.MainNavActivity;
import com.joinbesocial.candc.fragments.ChatFragment;
import com.joinbesocial.candc.fragments.CurrentVenueFragment;
import com.joinbesocial.candc.fragments.VenueItemFragment;

public class BEPushService extends BackendlessPushService {

    private final static String TAG = BEPushService.class.getSimpleName();

    public static final String FROM_NOTIFICATION_CENTER = "FROM_NOTIFICATION_CENTER";
    public static final String VENUE_CODE = "VENUE_CODE";
    public static final String CHAT_SESSION_ID = "CHAT_SESSION_ID";
    public static final String VENUE_MESSAGE = "VENUE_MESSAGE";
    public static final String VENUE_ITEM_ID = "VENUE_ITEM_ID";

    @Override
    public boolean onMessage(Context context, Intent intent) {

        Log.d(TAG, "onMessage called");

        // Don't bother with incoming messages if the user is logged out or has no Venue set!
        if(!Cloud.getInstance().isUserLoggedIn() || Utility.getInstance().getVenueCode() == null) {
            Log.d(TAG, "Ignoring message - User is not logged in or has set a Venue yet!");
            return false;
        }

        final CharSequence message = intent.getStringExtra(PublishOptions.MESSAGE_TAG);
        final CharSequence actionTag = intent.getStringExtra(PublishOptions.ANDROID_ACTION_TAG);
        final CharSequence contentTitle = intent.getStringExtra(PublishOptions.ANDROID_CONTENT_TITLE_TAG);
        final CharSequence contentText = intent.getStringExtra(PublishOptions.ANDROID_CONTENT_TEXT_TAG);
        final CharSequence tickerText = intent.getStringExtra(PublishOptions.ANDROID_TICKER_TEXT_TAG);
        final CharSequence venueCode = intent.getStringExtra("venue-code");
        final CharSequence chatSessionId = intent.getStringExtra("chat-session-id");
        final CharSequence venueMessage = intent.getStringExtra("venue-message");
        final CharSequence venueItemId = intent.getStringExtra("venue-item-id");

        Log.d(TAG, "message = " + String.valueOf(message));
        Log.d(TAG, "ANDROID_ACTION_TAG = " + String.valueOf(actionTag));
        Log.d(TAG, "android-content-text = " + String.valueOf(contentText));
        Log.d(TAG, "android-content-title = " + String.valueOf(contentTitle));
        Log.d(TAG, "android-ticker-text = " + String.valueOf(tickerText));
        Log.d(TAG, "venue-code = " + String.valueOf(venueCode));
        Log.d(TAG, "chat-session-id = " + String.valueOf(chatSessionId));
        Log.d(TAG, "venue-message = " + String.valueOf(venueMessage));
        Log.d(TAG, "venue-item-id = " + String.valueOf(venueItemId));

        // Don't bother if the venue codes do not match!
        if(venueCode != null) {
            String localVenueCode = Utility.getInstance().getVenueCode();
            if(!localVenueCode.equals(venueCode)) {
                return false;
            }
        } else {
            // Don't bother with messages that don't have a venueCode at all!
            return false;
        }

        final boolean isActivityVisible = CandCApplication.isActivityVisible();

        if(!isActivityVisible) {

            //
            // The app is not active, so create a Notification and send it to the Notification
            // Center. If the user taps on it, it will be handled later by MainNavActivity.
            //

            if(chatSessionId != null && venueCode != null) {

                Bundle bundle = new Bundle();
                bundle.putBoolean(FROM_NOTIFICATION_CENTER, true);
                bundle.putString(CHAT_SESSION_ID, chatSessionId.toString());
                bundle.putString(VENUE_CODE, venueCode.toString());

                Utility.getInstance().postNotification(contentTitle.toString(), contentText.toString(), tickerText.toString(), bundle);

            } else if(venueItemId != null && venueCode != null && venueMessage != null) {

                Bundle bundle = new Bundle();
                bundle.putBoolean(FROM_NOTIFICATION_CENTER, true);
                bundle.putString(VENUE_ITEM_ID, venueItemId.toString());
                bundle.putString(VENUE_CODE, venueCode.toString());
                bundle.putString(VENUE_MESSAGE, venueMessage.toString());

                Utility.getInstance().postNotification(contentTitle.toString(), contentText.toString(), tickerText.toString(), bundle);

            } else if(venueMessage != null && venueCode != null) {

                Bundle bundle = new Bundle();
                bundle.putBoolean(FROM_NOTIFICATION_CENTER, true);
                bundle.putString(VENUE_MESSAGE, venueMessage.toString());
                bundle.putString(VENUE_CODE, venueCode.toString());

                Utility.getInstance().postNotification(contentTitle.toString(), contentText.toString(), tickerText.toString(), bundle);
            }
        }

        // Post some work back to the main UI thread to update the badge counts and play sound FX.
        final Handler mainHandler = new Handler(context.getMainLooper());
        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {

                CandCApplication app = (CandCApplication) getApplication();

                if(app != null) {

                    MainNavActivity mainNavActivity = app.getMainNavActivity();

                    if(mainNavActivity != null) {

                        if(chatSessionId != null) {

                            boolean addToBadgeCount = true;

                            // If the user left the app at the ChatViewController we don't want to up the
                            // badge count if they're already looking this chat message.
                            if(mainNavActivity.getCurrentTabIndex() == 1) {
                                if(mainNavActivity.getCurrentFragment() instanceof ChatFragment) {
                                    ChatFragment chatFrgament = (ChatFragment) mainNavActivity.getCurrentFragment();
                                    if(chatFrgament != null) {
                                        if(chatFrgament.getChatSession() != null) {
                                            if(chatFrgament.getChatSession().getObjectId().equals(chatSessionId)) {
                                                addToBadgeCount = false;
                                            }
                                        }
                                    }
                                }
                            }

                            if(addToBadgeCount) {
                                int badgeCount = Utility.getInstance().getMessagesBadgeCount() + 1;
                                mainNavActivity.setBadgeCountForTabAtPosition(badgeCount, 1);
                                Utility.getInstance().setMessagesBadgeCount(badgeCount);
                            }

                            if(isActivityVisible) {
                                try {
                                    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                                    Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                                    r.play();
                                } catch(Exception e) {
                                    e.printStackTrace();
                                }
                            }

                        } else if(venueItemId != null) {

                            boolean addToBadgeCount = true;

                            // If viewed is already false, we shouldn't up the badge count.
                            if(!Utility.getInstance().getVenueItemMessageViewed(venueItemId.toString())){
                                addToBadgeCount = false;
                            }

                            Utility.getInstance().setVenueItemMessageData(venueItemId.toString(), venueMessage.toString(), false);

                            if(addToBadgeCount) {
                                int newVenuesBadgeCount = Utility.getInstance().getVenuesBadgeCount() + 1;
                                mainNavActivity.setBadgeCountForTabAtPosition(newVenuesBadgeCount, 0);
                                Utility.getInstance().setVenuesBadgeCount(newVenuesBadgeCount);
                            }

                            if(isActivityVisible) {
                                try {
                                    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                                    Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                                    r.play();
                                } catch(Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            if(mainNavActivity.getCurrentTabIndex() == 0) {
                                if(mainNavActivity.getCurrentFragment() instanceof VenueItemFragment) {
                                    // The user is on the VenueItemFragment so force reload to pick up the message!
                                    VenueItemFragment venueItemFragment = (VenueItemFragment) mainNavActivity.getCurrentFragment();
                                    if(venueItemFragment != null) {
                                        venueItemFragment.loadExtras();
                                    }
                                }
                            }

                        } else if(venueMessage != null) {

                            boolean addToBadgeCount = true;

                            // If viewed is already false, we shouldn't up the badge count.
                            if(!Utility.getInstance().isLatestVenueMessageViewed()) {
                                addToBadgeCount = false;
                            }

                            Utility.getInstance().setLatestVenueMessage(venueMessage.toString());
                            Utility.getInstance().setLatestVenueMessageViewed(false);

                            if(addToBadgeCount) {
                                int newVenuesBadgeCount = Utility.getInstance().getVenuesBadgeCount() + 1;
                                mainNavActivity.setBadgeCountForTabAtPosition(newVenuesBadgeCount, 0);
                                Utility.getInstance().setVenuesBadgeCount(newVenuesBadgeCount);
                            }

                            if(isActivityVisible) {
                                try {
                                    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                                    Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                                    r.play();
                                } catch(Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            if(mainNavActivity.getCurrentTabIndex() == 0) {
                                if(mainNavActivity.getCurrentFragment() instanceof CurrentVenueFragment) {
                                    // The user is on the CurrentVenueFragment so force reload to pick up the message!
                                    CurrentVenueFragment currentVenueFragment = (CurrentVenueFragment) mainNavActivity.getCurrentFragment();
                                    if(currentVenueFragment != null) {
                                        currentVenueFragment.reloadVenue();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        };
        mainHandler.post(myRunnable);

        // When returning 'true', default Backendless onMessage implementation will be executed.
        // The default implementation displays the notification in the Android Notification Center.
        // Returning false, cancels the execution of the default implementation.
        return false;
    }

    @Override
    public void onError(Context context, String messageError) {

        Log.d(TAG, "onError: " + messageError);

//        if(MainNavActivity.handler != null) {
//            Message message = new Message();
//            message.obj = new Error( messageError );
//            MainNavActivity.handler.sendMessage( message );
//        }
    }
}