package com.joinbesocial.candc.models;

// Make sure to read up on the restrictions that apply to your data objects!
// https://backendless.com/documentation/data/android/data_data_object.htm

public class ChatSession {

    private String objectId;

    private String memberOfVenueItemTitle;
    private String venueCode;

    private String ownerId;
    private String ownerName;
    private String ownerThumbnailUrl;
    private String ownerTitle;
    private boolean ownerViewed;

    private String targetId;
    private String targetName;
    private String targetThumbnailUrl;
    private String targetTitle;
    private boolean targetViewed;


    public ChatSession() {
        // Default constructor required for calls to DataSnapshot.getValue(ChatSession.class)
    }

    public ChatSession(String venueCode, String ownerName, String ownerTitle, String ownerThumbnailUrl,
                       String targetId, String targetName, String targetTitle, String targetThumbnailUrl,
                       String memberOfVenueItemTitle) {

        this.setVenueCode(venueCode);
        this.setMemberOfVenueItemTitle(memberOfVenueItemTitle);

        this.setOwnerName(ownerName);
        this.setOwnerTitle(ownerTitle);
        this.setOwnerThumbnailUrl(ownerThumbnailUrl);
        this.setOwnerViewed(true);

        this.setTargetId(targetId);
        this.setTargetName(targetName);
        this.setTargetTitle(targetTitle);
        this.setTargetThumbnailUrl(targetThumbnailUrl);
        this.setTargetViewed(false);
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId( String objectId ) {
        this.objectId = objectId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getTargetId() {
        return targetId;
    }

    public void setTargetId(String targetId) {
        this.targetId = targetId;
    }

    public String getMemberOfVenueItemTitle() {
        return memberOfVenueItemTitle;
    }

    public void setMemberOfVenueItemTitle(String memberOfVenueItemTitle) {
        this.memberOfVenueItemTitle = memberOfVenueItemTitle;
    }

    public String getVenueCode() {
        return venueCode;
    }

    public void setVenueCode(String venueCode) {
        this.venueCode = venueCode;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getTargetName() {
        return targetName;
    }

    public void setTargetName(String targetName) {
        this.targetName = targetName;
    }

    public String getOwnerTitle() {
        return ownerTitle;
    }

    public void setOwnerTitle(String ownerTitle) {
        this.ownerTitle = ownerTitle;
    }

    public String getTargetTitle() {
        return targetTitle;
    }

    public void setTargetTitle(String targetTitle) {
        this.targetTitle = targetTitle;
    }

    public String getOwnerThumbnailUrl() {
        return ownerThumbnailUrl;
    }

    public void setOwnerThumbnailUrl(String ownerThumbnailUrl) {
        this.ownerThumbnailUrl = ownerThumbnailUrl;
    }

    public String getTargetThumbnailUrl() {
        return targetThumbnailUrl;
    }

    public void setTargetThumbnailUrl(String targetThumbnailUrl) {
        this.targetThumbnailUrl = targetThumbnailUrl;
    }

    public boolean isOwnerViewed() {
        return ownerViewed;
    }

    public void setOwnerViewed(boolean ownerViewed) {
        this.ownerViewed = ownerViewed;
    }

    public boolean isTargetViewed() {
        return targetViewed;
    }

    public void setTargetViewed(boolean targetViewed) {
        this.targetViewed = targetViewed;
    }
}
