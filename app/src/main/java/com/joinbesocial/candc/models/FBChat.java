package com.joinbesocial.candc.models;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.ServerValue;

import java.util.HashMap;
import java.util.Map;

@IgnoreExtraProperties
public class FBChat {

    public String key;
    public String name;
    public String senderId;
    public Long timeStamp;

    public String text;
    public String imageUrl;
    public double lat;
    public double lng;

    public FBChat() {
        // Default constructor required for calls to DataSnapshot.getValue(Comment.class)
    }

    public static FBChat createFromSnapshot(DataSnapshot dataSnapshot) {

        FBChat fbChat = dataSnapshot.getValue(FBChat.class);
        fbChat.key = dataSnapshot.getKey();

        return fbChat;
    }

    public FBChat(String name, String senderId, String text) {
        this.name = name;
        this.senderId = senderId;
        this.text = text;
    }

    public FBChat(String name, String senderId, String text, String imageUrl) {
        this.name = name;
        this.senderId = senderId;
        this.text = text;
        this.imageUrl = imageUrl;
    }

    public FBChat(String name, String senderId, double lat, double lng) {
        this.name = name;
        this.senderId = senderId;
        this.lat = lat;
        this.lng = lng;
    }

    @Exclude
    public Map<String, Object> toMap() {

        HashMap<String, Object> result = new HashMap<>();

        result.put("name", name);
        result.put("senderId", senderId);
        result.put("timeStamp", ServerValue.TIMESTAMP);

        result.put("text", text);
        result.put("imageUrl", imageUrl);

        if(lat != 0) {
            result.put("lat", lat);
        }

        if(lng != 0) {
            result.put("long", lng);
        }

        return result;
    }
}
