package com.joinbesocial.candc.models;

// Make sure to read up on the restrictions that apply to your data objects!
// https://backendless.com/documentation/data/android/data_data_object.htm

import com.backendless.geo.GeoPoint;

public class Venue {

    private String objectId;
    private String venueCode;
    private String venueItemName;
    private String logoUrl;
    private String title;
    private String subTitle;
    private String desc;
    private String address;
    private GeoPoint location;
    private String phone;
    private String email;
    private String webUrl;
    private String geofences;
    private String extras;

    public Venue() {
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId( String objectId ) {
        this.objectId = objectId;
    }

    public String getVenueCode() {
        return venueCode;
    }

    public void setVenueCode( String venueCode ) {
        this.venueCode = venueCode;
    }

    public String getVenueItemName() {
        return venueItemName;
    }

    public void setVenueItemName(String venueItemName) {
        this.venueItemName = venueItemName;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public GeoPoint getLocation() {
        return location;
    }

    public void setLocation(GeoPoint location) {
        this.location = location;
    }

    public String getGeofences() {
        return geofences;
    }

    public void setGeofences(String geofences) {
        this.geofences = geofences;
    }

    public String getExtras() {
        return extras;
    }

    public void setExtras(String extras) {
        this.extras = extras;
    }
}
