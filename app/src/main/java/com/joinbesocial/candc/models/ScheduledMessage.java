package com.joinbesocial.candc.models;

import java.util.Date;

public class ScheduledMessage {

    private String objectId;

    private String venueCode;
    private String messageId;
    private String message;
    private Date deliveryDate;
    private String venueItem;

    public ScheduledMessage() {
        // Default constructor required for calls to DataSnapshot.getValue(ChatSession.class)
    }

    public ScheduledMessage(String venueCode, String messageId, String message, Date deliveryDate, String venueItem) {
        this.setVenueCode(venueCode);
        this.setMessageId(messageId);
        this.setMessage(message);
        this.setDeliveryDate(deliveryDate);
        this.setVenueItem(venueItem);
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId( String objectId ) {
        this.objectId = objectId;
    }


    public String getVenueCode() {
        return venueCode;
    }

    public void setVenueCode(String venueCode) {
        this.venueCode = venueCode;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getVenueItem() {
        return venueItem;
    }

    public void setVenueItem(String venueItem) {
        this.venueItem = venueItem;
    }
}
