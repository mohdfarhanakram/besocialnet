package com.joinbesocial.candc;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.NotificationCompat;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RemoteViews;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.backendless.push.NotificationLookAndFeel;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.List;

import uk.co.chrisjenx.calligraphy.TypefaceUtils;

// https://github.com/afollestad/material-dialogs
// https://github.com/kcochibili/TinyDB--Android-Shared-Preferences-Turbo

public class Utility {

    private final static String TAG = Utility.class.getSimpleName();

    private static class Holder {
        static final Utility INSTANCE = new Utility();
    }

    public void init() {

        mTinydb = new TinyDB(CandCApplication.getAppContext());
    }

    public static Utility getInstance() {
        return Utility.Holder.INSTANCE;
    }

    private KProgressHUD mProgressHUD = null;

    private TinyDB mTinydb = null;

    private Drawable mVenuePlaceHolderDrawable;
    private Drawable mPlaceHolderDrawable;
    private Drawable mPlaceHolderThumbnailDrawable;

    public static boolean isOnline() {

        ConnectivityManager cm =
                (ConnectivityManager) CandCApplication.getAppContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();

        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public void setIsUserLoggedIn(boolean isUserLoggedIn) {
        mTinydb.putBoolean("isUserLoggedIn", isUserLoggedIn);
    }

    public boolean isUserLoggedIn() {
        return mTinydb.getBoolean("isUserLoggedIn");
    }

    public void setHostsLatestVenueMessage(String hostsLatestVenueMessage) {

        if(hostsLatestVenueMessage == null) {
            mTinydb.remove("hostsLatestVenueMessage");
        } else {
            mTinydb.putString("hostsLatestVenueMessage", hostsLatestVenueMessage);
        }
    }

    public String getHostsLatestVenueMessage() {

        String value = mTinydb.getString("latestVenueMessage");

        if(value.isEmpty()) {
            return null;
        } else {
            return value;
        }
    }

    public void setLatestVenueMessage(String latestVenueMessage) {

        if(latestVenueMessage == null) {
            mTinydb.remove("latestVenueMessage");
        } else {
            mTinydb.putString("latestVenueMessage", latestVenueMessage);
        }
    }

    public String getLatestVenueMessage() {

        String value = mTinydb.getString("latestVenueMessage");

        if(value.isEmpty()) {
            return null;
        } else {
            return value;
        }
    }

    public void setLatestVenueMessageViewed(boolean latestVenueMessageViewed) {

        mTinydb.putBoolean("latestVenueMessageViewed", latestVenueMessageViewed);
    }

    public boolean isLatestVenueMessageViewed() {

        return mTinydb.getBoolean("latestVenueMessageViewed");
    }

    public void setMessagesBadgeCount(int messagesBadgeCount) {

        if(messagesBadgeCount < 0) {
            mTinydb.putInt("messagesBadgeCount", 0); // Don't allow negative values.
        } else {
            mTinydb.putInt("messagesBadgeCount", messagesBadgeCount);
        }
    }

    public int getMessagesBadgeCount() {
        return mTinydb.getInt("messagesBadgeCount");
    }

    public void setVenuesBadgeCount(int venuesBadgeCount) {

        if(venuesBadgeCount < 0) {
            mTinydb.putInt("venuesBadgeCount", 0); // Don't allow negative values.
        } else {
            mTinydb.putInt("venuesBadgeCount", venuesBadgeCount);
        }
    }

    public void setVenueItemMessageData(String venueItemId, String message, boolean viewed) {

        VenueItemMessageData venueItemMessageData;

        try {
            venueItemMessageData = (VenueItemMessageData) mTinydb.getObject(venueItemId, VenueItemMessageData.class);
        } catch(Exception e) {
            venueItemMessageData = new VenueItemMessageData(message);
        }

        venueItemMessageData.message = message;
        venueItemMessageData.viewed = viewed;

        mTinydb.putObject(venueItemId, venueItemMessageData);
    }

    public String getVenueItemMessage(String venueItemId) {

        try {
            VenueItemMessageData venueItemMessageData = (VenueItemMessageData) mTinydb.getObject(venueItemId, VenueItemMessageData.class);
            if(venueItemMessageData != null) {
                return venueItemMessageData.message;
            }
        } catch(Exception e) {
            return null;
        }

        return null;
    }

    public void setVenueItemMessage(String venueItemId, String message) {

        VenueItemMessageData venueItemMessageData;

        try {
            venueItemMessageData = (VenueItemMessageData) mTinydb.getObject(venueItemId, VenueItemMessageData.class);
        } catch(Exception e) {
            return;
        }

        venueItemMessageData.message = message;

        mTinydb.putObject(venueItemId, venueItemMessageData);
    }

    public boolean getVenueItemMessageViewed(String venueItemId) {

        try {
            VenueItemMessageData venueItemMessageData = (VenueItemMessageData) mTinydb.getObject(venueItemId, VenueItemMessageData.class);
            if(venueItemMessageData != null) {
                return venueItemMessageData.viewed;
            }
        } catch(Exception e) {
            return true; // Assume viewed if it fails!
        }

        return true; // Assume viewed if it fails!
    }

    public void setVenueItemMessageViewed(String venueItemId, boolean viewed) {

        VenueItemMessageData venueItemMessageData;

        try {
            venueItemMessageData = (VenueItemMessageData) mTinydb.getObject(venueItemId, VenueItemMessageData.class);
        } catch(Exception e) {
            return;
        }

        venueItemMessageData.viewed = viewed;

        mTinydb.putObject(venueItemId, venueItemMessageData);
    }

    public int getVenuesBadgeCount() {
        return mTinydb.getInt("venuesBadgeCount");
    }

    public void setVenueObjectId(String venueObjectId) {

        if(venueObjectId == null) {
            mTinydb.remove("venueObjectId");
        } else {
            mTinydb.putString("venueObjectId", venueObjectId);
        }
    }

    public String getVenueObjectId() {

        String value = mTinydb.getString("venueObjectId");

        if(value.isEmpty()) {
            return null;
        } else {
            return value;
        }
    }

    public void setVenueCode(String venueCode) {

        if(venueCode == null) {

            if(getVenueCode() != null) {
                //Cloud.getInstance().stopGeofenceMonitoring(getVenueCode());
                Cloud.getInstance().stopGeofenceMonitoring();
            }

            mTinydb.remove("venueCode");

        } else {
            mTinydb.putString("venueCode", venueCode);

            //Cloud.getInstance().startGeofenceMonitoring(venueCode);
        }
    }

    public String getVenueCode() {

        String value = mTinydb.getString("venueCode");

        if(value.isEmpty()) {
            return null;
        } else {
            return value;
        }
    }

    public void setVenueTitle(String venueTitle) {

        if(venueTitle == null) {
            mTinydb.remove("venueTitle");
        } else {
            mTinydb.putString("venueTitle", venueTitle);
        }
    }

    public String getVenueTitle() {

        String value = mTinydb.getString("venueTitle");

        if(value.isEmpty()) {
            return null;
        } else {
            return value;
        }
    }

    public void setDeviceId(String deviceId) {

        if(deviceId == null) {
            mTinydb.remove("deviceId");
        } else {
            mTinydb.putString("deviceId", deviceId);
        }
    }

    public String getDeviceId() {

        String value = mTinydb.getString("deviceId");

        if(value.isEmpty()) {
            return null;
        } else {
            return value;
        }
    }

    public void setGeofences(String geofences) {

        Log.d(TAG, "setGeofences: " + String.valueOf(geofences));

        if(geofences == null) {
            Cloud.getInstance().stopGeofenceMonitoring();
            mTinydb.remove("geofences");
        } else {

            //boolean restartGeofenceMonitoring = false;

            if(getGeofences() != null && !getGeofences().equals(geofences)) {
                Cloud.getInstance().stopGeofenceMonitoring();
                //restartGeofenceMonitoring = true;
            }

            mTinydb.putString("geofences", geofences);

            //if(restartGeofenceMonitoring) {
            //}
        }
    }

    public String getGeofences() {

        String value = mTinydb.getString("geofences");

        if(value.isEmpty()) {
            return null;
        } else {
            return value;
        }
    }

    public void setInsideGeofence(String geofenceName, boolean insideGeofence) {

        GeoFenceData geoFenceData;

        try {
            geoFenceData = (GeoFenceData) mTinydb.getObject(geofenceName, GeoFenceData.class);
        } catch(Exception e) {
            geoFenceData = new GeoFenceData(geofenceName);
        }

        geoFenceData.inside = insideGeofence;

        if(insideGeofence) {
            geoFenceData.dateEntered = new Date();
        } else {
            geoFenceData.dateEntered = null;
        }

        mTinydb.putObject(geofenceName, geoFenceData);
    }

    public int getGeofenceLengthOfStay(String geofenceName) {

        GeoFenceData geoFenceData;

        try {
            geoFenceData = (GeoFenceData) mTinydb.getObject(geofenceName, GeoFenceData.class);

            if(geoFenceData != null) {
                Date now = new Date();
                return (int) (now.getTime() - geoFenceData.dateEntered.getTime()) / 1000;
            }

        } catch(Exception e) {
            return 0;
        }

        return 0;
    }

    public boolean isInsideGeofence(String geofenceName) {

        try {
            GeoFenceData geoFenceData = (GeoFenceData) mTinydb.getObject(geofenceName, GeoFenceData.class);
            if(geoFenceData != null) {
                return geoFenceData.inside;
            }
        } catch(Exception e) {
            return false;
        }

        return false;
    }

    public void leaveCurrentEvent() {

        setVenueObjectId(null);
        setVenueCode(null);
        setVenueTitle(null);
        setHostsLatestVenueMessage(null);
        setLatestVenueMessage(null);
        setLatestVenueMessageViewed(true);
        setGeofences(null);

        CloudAnalytics.getInstance().unregisterVenueNameSuperProperty();
        Cloud.getInstance().unregisterDeviceForPush();

        Glide.get(CandCApplication.getAppContext()).clearMemory();
    }

    public interface OkCallback {
        void onOk();
    }

    public interface CancelCallback {
        void onCancel();
    }

    public interface OkFindEventCallback {
        void onOkFindEvent(String venueCode);
    }

    public final static boolean isValidEmail(CharSequence emailAddress) {

        if(emailAddress == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(emailAddress).matches();
        }
    }

    public void startInstalledAppDetailsActivity() {

        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.setData(Uri.parse("package:" + CandCApplication.getAppContext().getPackageName()));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        CandCApplication.getAppContext().startActivity(intent);
    }

    public int getSquareCropDimensionForBitmap(Bitmap bitmap) {

        // Use the smallest dimension of the image to crop to
        return Math.min(bitmap.getWidth(), bitmap.getHeight());
    }

    public Bitmap resizeBitmapToWidth(Bitmap bitmap, int toWidth) {

        float aspectRatio = bitmap.getWidth() / (float) bitmap.getHeight();
        int height = Math.round(toWidth / aspectRatio);

        Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, toWidth, height, false);

        return resizedBitmap;
    }

    public Drawable createRoundDrawableFromBitmap(Bitmap bitmap) {

        Resources res = CandCApplication.getAppContext().getResources();
        RoundedBitmapDrawable drawable = RoundedBitmapDrawableFactory.create(res, bitmap);
        drawable.setCircular(true);

        return drawable;
    }

    public Drawable getVenuePlaceHolderImage() {

        if(mVenuePlaceHolderDrawable == null) {
            Bitmap bitmap = BitmapFactory.decodeResource(CandCApplication.getAppContext().getResources(), R.drawable.image_placeholder);
            mVenuePlaceHolderDrawable = Utility.getInstance().createRoundDrawableFromBitmap(bitmap);
        }

        return mVenuePlaceHolderDrawable;
    }

    public Drawable getPlaceHolderImage() {

        if(mPlaceHolderDrawable == null) {
            Bitmap bitmap = BitmapFactory.decodeResource(CandCApplication.getAppContext().getResources(), R.drawable.profile_placeholder);
            mPlaceHolderDrawable = Utility.getInstance().createRoundDrawableFromBitmap(bitmap);
        }

        return mPlaceHolderDrawable;
    }

    public Drawable getPlaceHolderThumbnailImage() {

        if(mPlaceHolderThumbnailDrawable == null) {
            Bitmap bitmap = BitmapFactory.decodeResource(CandCApplication.getAppContext().getResources(), R.drawable.profile_placeholder_thumbnail);
            mPlaceHolderThumbnailDrawable = Utility.getInstance().createRoundDrawableFromBitmap(bitmap);
        }

        return mPlaceHolderThumbnailDrawable;
    }

    public String readFileFromAssets(String fileName) {

        String fileString = "";

        BufferedReader reader = null;
        try {
            reader = new BufferedReader(
                    new InputStreamReader(CandCApplication.getAppContext().getAssets().open(fileName)));

            // do reading, usually loop until end of file reading
            String mLine;
            while ((mLine = reader.readLine()) != null) {
                //process line
                fileString += mLine;
            }

            return fileString;

        } catch (IOException e) {
            //log the exception
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                }
            }
        }

        return fileString;
    }

    public void showLoadSpinner(Context context, String title) {

        mProgressHUD = KProgressHUD.create(context)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(title)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
    }

    public void hideLoadSpinner() {
        hideLoadSpinner(0);
    }

    public void hideLoadSpinner(long delayMillis) {

        if(delayMillis > 0) {

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(mProgressHUD != null) {
                        mProgressHUD.dismiss();
                        mProgressHUD = null;
                    }
                }
            }, delayMillis);

        } else {

            if(mProgressHUD != null) {
                mProgressHUD.dismiss();
                mProgressHUD = null;
            }
        }
    }

    public void showErrorDialog(Context context, String title, String message) {

        new MaterialDialog.Builder(context)
                .titleColor(ContextCompat.getColor(context, R.color.dialog_error))
                .positiveColor(ContextCompat.getColor(context, R.color.ui_red))
                .title(title)
                .content(message)
                .positiveText(R.string.agree)
                .show();
    }

    public void showOkDialog(Context context, String title, String message) {

        new MaterialDialog.Builder(context)
                .positiveColor(ContextCompat.getColor(context, R.color.ui_red))
                .title(title)
                .content(message)
                .positiveText(R.string.agree)
                .show();
    }

    public void showOkCancelDialog(Context context, String title, String message, final OkCallback okCallback) {

        new MaterialDialog.Builder(context)
                .positiveColor(ContextCompat.getColor(context, R.color.ui_red))
                .negativeColor(ContextCompat.getColor(context, R.color.light_gray))
                .title(title)
                .content(message)
                .positiveText(R.string.agree)
                .negativeText(R.string.disagree)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        okCallback.onOk();
                    }
                })
                .show();
    }

    public void showOkCancelDialog(Context context, String title, String message, final OkCallback okCallback, final CancelCallback cancelCallback) {

        new MaterialDialog.Builder(context)
                .positiveColor(ContextCompat.getColor(context, R.color.ui_red))
                .negativeColor(ContextCompat.getColor(context, R.color.light_gray))
                .title(title)
                .content(message)
                .positiveText(R.string.agree)
                .negativeText(R.string.disagree)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        okCallback.onOk();
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        cancelCallback.onCancel();
                    }
                })
                .show();
    }

    public void showFindEventDialog(final Context context, final OkFindEventCallback okFindEventCallback) {

        new MaterialDialog.Builder(context)
                .title("Find Venue")
                .content("If you know your venue's unique ID, you can enter it now!")
                .negativeText(R.string.disagree)
                .inputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS)
                .inputRange(4, 10, ContextCompat.getColor(context, R.color.dialog_error))
                .input("Venue ID", "", new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(MaterialDialog dialog, CharSequence input) {
                        okFindEventCallback.onOkFindEvent(input.toString());
                    }
                }).show();
    }

    public LatLng getLatLngFromAddressUsingGeocoder(Context context, String strAddress) {

        Geocoder geocoder = new Geocoder(context);

        if(!geocoder.isPresent()) {
            Log.e(TAG, "getLatLngFromAddressUsingGeocoder: Geocoder is not present!");
            return null;
        }

        List<Address> address;
        LatLng latLng = null;

        try {
            address = geocoder.getFromLocationName(strAddress, 1);

            if(address == null ||address.size() == 0) {
                Log.e(TAG, "getLatLngFromAddressUsingGeocoder: failed to find any matches for address: " + strAddress);
                return null;
            }

            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();

            latLng = new LatLng(location.getLatitude(), location.getLongitude());

        } catch (Exception e) {
            e.printStackTrace();
        }

        return latLng;
    }

    public void postNotification(String contentTitle, String contentText, String tickerText) {

        postNotification(contentTitle, contentText, tickerText, null);
    }

    public void postNotification(String contentTitle, String contentText, String tickerText, Bundle bundle) {

        // NOTE: Most of the code below was copied from BackendlessPushService for
        // customization!

        if(tickerText != null && tickerText.length() > 0) {

            Context context = CandCApplication.getAppContext();

//            int appIcon = context.getApplicationInfo().icon;
//            if(appIcon == 0) {
//                appIcon = android.R.drawable.sym_def_app_icon;
//            }

            Intent notificationIntent = context.getPackageManager().getLaunchIntentForPackage(context.getApplicationInfo().packageName);

            if(bundle != null) {
                notificationIntent.putExtras(bundle);
            }

            int requestID = (int) System.currentTimeMillis();
            PendingIntent contentIntent = PendingIntent.getActivity(context, requestID, notificationIntent, PendingIntent.FLAG_ONE_SHOT);

            Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            Notification notification = new NotificationCompat.Builder(context)
                    .setSmallIcon(R.drawable.ic_stat_name)
                    .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
                    .setTicker(tickerText)
                    .setContentTitle(contentTitle)
                    .setContentText(contentText)
                    .setContentIntent(contentIntent)
                    .setWhen(System.currentTimeMillis())
                    .setSound(soundUri)
                    .build();

            notification.flags |= Notification.FLAG_AUTO_CANCEL;

            int customLayout = context.getResources().getIdentifier("notification", "layout", context.getPackageName());
            int customLayoutTitle = context.getResources().getIdentifier("title", "id", context.getPackageName());
            int customLayoutDescription = context.getResources().getIdentifier("text", "id", context.getPackageName());
            int customLayoutImageContainer = context.getResources().getIdentifier("image", "id", context.getPackageName());
            int customLayoutImage = context.getResources().getIdentifier("push_icon", "drawable", context.getPackageName());

            if(customLayout > 0 && customLayoutTitle > 0 && customLayoutDescription > 0 && customLayoutImageContainer > 0) {
                NotificationLookAndFeel lookAndFeel = new NotificationLookAndFeel();
//lookAndFeel.extractColors( context ); // ??????
                RemoteViews contentView = new RemoteViews(context.getPackageName(), customLayout);
                contentView.setTextViewText(customLayoutTitle, contentTitle);
                contentView.setTextViewText(customLayoutDescription, contentText);
                contentView.setTextColor(customLayoutTitle, lookAndFeel.getTextColor());
                contentView.setFloat(customLayoutTitle, "setTextSize", lookAndFeel.getTextSize());
                contentView.setTextColor(customLayoutDescription, lookAndFeel.getTextColor());
                contentView.setFloat(customLayoutDescription, "setTextSize", lookAndFeel.getTextSize());
                contentView.setImageViewResource(customLayoutImageContainer, customLayoutImage);
                notification.contentView = contentView;
            }

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(requestID, notification);
        }
    }

    public void addExtrasToLayout(final Context context, LinearLayout extrasLayout, int width, final String exrasJson,
                                  final String pageForAnalytics, final String vendorNameForAnalytics, final String vendorTypeForAnalytics) {

        // Examples of JSON Formatted Extras:
        //
        //  [
        //      {
        //          "type": "facebook",
        //          "url": "https://www.facebook.com/MicrosoftCloud/"
        //      },
        //
        //      {
        //          "type": "twitter",
        //          "url": "https://twitter.com/MSCloud?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor"
        //      },
        //
        //      {
        //          "type": "image",
        //          "imageUrl": "https://api.backendless.com/F2FA2DCD-CE0B-B5DD-FF9E-D7AF5C85FD00/v1/files/extra/digital_transformation.jpg",
        //          "infoUrl": "https://twitter.com/hashtag/DigitalTransformation?src=hash",
        //          "text": "DigitalTransformation is causing organizations across nearly all industries to imagine what's possible."
        //      },
        //
        //      {
        //          "type": "text",
        //          "text": "Make sure to visit our booth for our IOT device giveaway special!"
        //      }
        //  ]

        JsonParser parser = new JsonParser();
        JsonArray rootArray = parser.parse(exrasJson).getAsJsonArray();

        if(rootArray.size() > 0) {
            extrasLayout.setVisibility(View.VISIBLE);
        }

        for(JsonElement extraElement : rootArray) {

            JsonObject extraObj = extraElement.getAsJsonObject();

            if(extraObj.get("type") != null) {

                final String type = extraObj.get("type").getAsString();

                //Log.d(TAG, "type = " + type);

                if((type.equals("facebook") || type.equals("twitter") || type.equals("instagram") || type.equals("linkedin"))
                        && extraObj.get("url") != null) {

                    final String url = extraObj.get("url").getAsString();

                    //Log.d(TAG, "url = " + url);

                    Button btn = new Button(context);

                    if(type.equals("facebook")) {
                        btn.setBackgroundResource(R.drawable.facebook_login);
                    } else if(type.equals("twitter")) {
                        btn.setBackgroundResource(R.drawable.twitter_login);
                    } else if(type.equals("instagram")) {
                        btn.setBackgroundResource(R.drawable.instagram);
                    } else if(type.equals("linkedin")) {
                        btn.setBackgroundResource(R.drawable.linkedin);
                    }

                    LinearLayout.LayoutParams layoutParams =
                            new LinearLayout.LayoutParams(width, LinearLayout.LayoutParams.WRAP_CONTENT);
                    layoutParams.setMargins(0, 16, 0, 0);

                    btn.setLayoutParams(layoutParams);

                    btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Log.d(TAG, "url = " + url);

                            CloudAnalytics.getInstance().sendEventLinkViewed(url, pageForAnalytics, vendorNameForAnalytics, vendorTypeForAnalytics);

                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                            context.startActivity(browserIntent);
                        }
                    });

                    extrasLayout.addView(btn);

                } else if(type.equals("image") && extraObj.get("imageUrl") != null) {

                    final String imageUrl = extraObj.get("imageUrl").getAsString();

                    //Log.d(TAG, "imageUrl = " + imageUrl);

                    ImageView imageView = new ImageView(context);

                    LinearLayout.LayoutParams layoutParams =
                            new LinearLayout.LayoutParams(width, LinearLayout.LayoutParams.WRAP_CONTENT);
                    layoutParams.setMargins(0, 16, 0, 0);

                    imageView.setLayoutParams(layoutParams);

                    Glide.with(context)
                            .load(imageUrl)
                            .fitCenter()
                            .listener(new RequestListener<String, GlideDrawable>() {
                                @Override
                                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                    Log.e(TAG, e.getLocalizedMessage());
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                    return false;
                                }
                            })
                            .into(imageView);


                    if(extraObj.get("infoUrl") != null) {

                        final String infoUrl = extraObj.get("infoUrl").getAsString();

                        imageView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                //Log.d(TAG, "infoUrl = " + infoUrl);

                                CloudAnalytics.getInstance().sendEventLinkViewed(infoUrl, pageForAnalytics, vendorNameForAnalytics, vendorTypeForAnalytics);

                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(infoUrl));
                                context.startActivity(browserIntent);
                            }
                        });
                    }

                    extrasLayout.addView(imageView);

                    if(extraObj.get("text") != null) {

                        final String text = extraObj.get("text").getAsString();

                        TextView textView = new TextView(context);
                        textView.setBackgroundColor(context.getResources().getColor(R.color.white));
                        textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                        textView.setPadding(8, 8, 8, 8);
                        Typeface typeFace = TypefaceUtils.load(CandCApplication.getAppContext().getAssets(), "fonts/OpenSans-Regular.otf");
                        if(typeFace != null) {
                            textView.setTypeface(typeFace);
                        }
                        textView.setTextSize(17);
                        textView.setText(text);

                        LinearLayout.LayoutParams layoutParamsText =
                                new LinearLayout.LayoutParams(width, LinearLayout.LayoutParams.WRAP_CONTENT);
                        layoutParamsText.setMargins(0, 16, 0, 0);
//                        layoutParamsText.setMarginEnd(16);
//                        layoutParamsText.setMarginStart(16);

                        textView.setLayoutParams(layoutParamsText);

                        extrasLayout.addView(textView);
                    }

                } else if(type.equals("text") && extraObj.get("text") != null) {

                    final String text = extraObj.get("text").getAsString();

                    TextView textView = new TextView(context);
                    textView.setBackgroundColor(context.getResources().getColor(R.color.white));
                    textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    textView.setPadding(8, 8, 8, 8);
                    Typeface typeFace = TypefaceUtils.load(CandCApplication.getAppContext().getAssets(), "fonts/OpenSans-Regular.otf");
                    if(typeFace != null) {
                        textView.setTypeface(typeFace);
                    }
                    textView.setTextSize(17);
                    textView.setText(text);

                    LinearLayout.LayoutParams layoutParamsText =
                            new LinearLayout.LayoutParams(width, LinearLayout.LayoutParams.WRAP_CONTENT);
                    layoutParamsText.setMargins(0, 16, 0, 0);
//                    layoutParamsText.setMarginEnd(16);
//                    layoutParamsText.setMarginStart(16);

                    textView.setLayoutParams(layoutParamsText);

                    extrasLayout.addView(textView);
                }
            }
        }
    }
}
