package com.joinbesocial.candc;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.BackendlessUser;
import com.backendless.DeviceRegistration;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.async.callback.BackendlessCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.files.BackendlessFile;
import com.backendless.geo.geofence.GeoFenceMonitoring;
import com.backendless.geo.geofence.IGeofenceCallback;
import com.backendless.messaging.DeliveryOptions;
import com.backendless.messaging.PublishOptions;
import com.backendless.messaging.PushBroadcastMask;
import com.backendless.messaging.PushPolicyEnum;
import com.backendless.persistence.BackendlessDataQuery;
import com.backendless.persistence.QueryOptions;
import com.backendless.persistence.local.UserTokenStorageFactory;
import com.backendless.services.messaging.MessageStatus;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.joinbesocial.candc.models.ChatSession;
import com.joinbesocial.candc.models.ScheduledMessage;
import com.joinbesocial.candc.models.Venue;
import com.joinbesocial.candc.models.FBChat;
import com.joinbesocial.candc.models.VenueItem;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class Cloud {

    private final static String TAG = Cloud.class.getSimpleName();

    private final static String BACKENDLESS_APP_VERSION = "v1";
    private String BACKENDLESS_APP_ID = "";
    private String BACKENDLESS_SECRET_KEY = "";
    private String CHAT_SESSIONS_KEY = "";
    private String TYPING_INDICATOR_KEY = "";

    private final static String GOOGLE_PROJECT_ID = "355581384048";
    private final static String GOOGLE_MAPS_GEOCODING_URL = "https://maps.googleapis.com/maps/api/geocode/json";
    private final static String GOOGLE_MAPS_GEOCODING_KEY = "AIzaSyCFyd0ME7UOV1_ApRgcbwFa6Uwb5xfq-Cs";

    private DatabaseReference mFBDatabase;
    private FirebaseStorage mFBStorage;
    private StorageReference mFBStorageRef;
    private DatabaseReference mFBUserIsTypingRef;
    private IGeofenceCallback mGeofenceCallback;
    private AsyncCallback<Void> mGeofenceCompletionCallback;

    private static class Holder {
        static final Cloud INSTANCE = new Cloud();
    }

    public static Cloud getInstance() {
        return Holder.INSTANCE;
    }

    public interface ResponseCallback {
        void onResponse();
    }

    public interface MessageResponseCallback {
        void onResponse(String messageID);
    }

    public interface LatLngCallback {
        void onData(LatLng latLng);
    }

    public interface ResponseImageCallback {
        void onResponse(Drawable drawable);
    }

    public interface FaultCallback {
        void onFault(String code, String message);
    }

    public interface ChatSessionResponseCallback {
        void onData(ChatSession chatSession);
    }

    public interface ChatSessionsResponseCallback {
        void onData(ArrayList<ChatSession> chatSessions);
    }

    public interface SaveChatSessionResponseCallback {
        void onData(ChatSession chatSession);
    }

    public interface SaveScheduledMessageResponseCallback {
        void onData(ScheduledMessage scheduledMessages);
    }

    public interface CancelScheduledMessageResponseCallback {
        void onResponse();
    }

    public interface RemoveScheduledMessageResponseCallback {
        void onResponse();
    }

    public interface ScheduledMessagesResponseCallback {
        void onData(ArrayList<ScheduledMessage> scheduledMessage);
    }

    public interface VenueItemPageResponseCallback {
        void onData(ArrayList<VenueItem> foundVenueItems, int newOffset, int totalObjects);
    }

    public interface VenueItemsResponseCallback {
        void onData(ArrayList<VenueItem> foundVenueItems);
    }

    public interface VenueItemResponseCallback {
        void onData(VenueItem foundVenueItem);
    }

    public interface VenueResponseCallback {
        void onData(Venue venue);
    }

    public interface AttendeesPageResponseCallback {
        void onData(ArrayList<BackendlessUser> users, int newOffset, int totalObjects);
    }

    public interface AttendeesResponseCallback {
        void onData(ArrayList<BackendlessUser> users);
    }

    public interface UserResponseCallback {
        void onData(BackendlessUser user);
    }

    public interface VenuesResponseCallback {
        void onData(ArrayList<Venue> venues);
    }

    public interface ChatSessionsDataCallback {
        void onData(ArrayList<FBChat> chatSessions);
    }

    public interface ChatSessionDataCallback {
        void onData(FBChat chatSession);
    }

    public interface DatabaseErrorCallback {
        void onDatabaseError(int code, String message);
    }

    public interface StorageUploadCallback {
        void onUpload(Uri uri);
    }

    public interface StorageErrorCallback {
        void onStorageErrorCallback(String message);
    }

    public interface OnTypingCallback {
        void onTyping(boolean isTyping);
    }

    public void init() {

        if(!com.joinbesocial.candc.BuildConfig.DEBUG) {
            Log.d(TAG, "DEBUG");
            // CandC_develop
            BACKENDLESS_APP_ID = "4BE04D0E-06FB-A8F0-FFB5-63FD97238000";
            BACKENDLESS_SECRET_KEY = "3B4347BA-EE72-CD23-FF18-3F53A45FFE00";

            CHAT_SESSIONS_KEY = "chatSessions_develop";
            TYPING_INDICATOR_KEY = "typingIndicator_develop";
        } else {
            Log.d(TAG, "RELEASE");
            // CandC
            BACKENDLESS_APP_ID = "F2FA2DCD-CE0B-B5DD-FF9E-D7AF5C85FD00";
            BACKENDLESS_SECRET_KEY = "DC8E93BF-2E8A-3055-FF6C-0BBA2C42AE00";

            CHAT_SESSIONS_KEY = "chatSessions";
            TYPING_INDICATOR_KEY = "typingIndicator";
        }


        Backendless.initApp(CandCApplication.getAppContext(), BACKENDLESS_APP_ID, BACKENDLESS_SECRET_KEY, BACKENDLESS_APP_VERSION);

        refreshUserData(new ResponseCallback() {
            @Override
            public void onResponse() {
            }
        }, new FaultCallback() {
            @Override
            public void onFault(String code, String message) {
            }
        });

        mFBDatabase = FirebaseDatabase.getInstance().getReference();
        mFBStorage = FirebaseStorage.getInstance();
        mFBStorageRef = mFBStorage.getReferenceFromUrl("gs://candc-93e28.appspot.com");
    }

    private Boolean isUserLoggedIn = null;

    public boolean isUserLoggedIn() {

        if(isUserLoggedIn != null) {
            return isUserLoggedIn.booleanValue();
        }

        cacheLoginStatus();

        return isUserLoggedIn.booleanValue();
    }

    private void cacheLoginStatus() {

        // This call to UserTokenStorageFactory is not very reliable right after a new login!
        // We'll leave it for now but we will now set isUserLoggedIn manually from signinUser,
        // signinUserViaTwitter, and signinUserViaFacebook to force the correct value!
        String userToken = UserTokenStorageFactory.instance().getStorage().get();

        if(userToken != null && !userToken.equals("")) {

            // We found a cached user token so we know for sure the user is already logged!
            isUserLoggedIn = new Boolean(true);
            Utility.getInstance().setIsUserLoggedIn(true);

        } else {

            // If we can't get the userToken - check to see if the user was ever logged in, in the past.
            // If so, just say we're logged in any way. This is typically caused by loss of network.
            isUserLoggedIn = Utility.getInstance().isUserLoggedIn();
        }
    }

    public void refreshUserData(final ResponseCallback responseCallback, final FaultCallback faultCallback) {

        // If logged in - load data about the user!
        Backendless.UserService.isValidLogin(new AsyncCallback<Boolean>() {

            @Override
            public void handleResponse(Boolean isValidLogin) {

                if(isValidLogin) {

                    String currentUserId = Backendless.UserService.loggedInUser();

                    if(!currentUserId.equals("")) {

                        Backendless.UserService.findById(currentUserId, new AsyncCallback<BackendlessUser>() {
                            @Override
                            public void handleResponse(BackendlessUser currentUser) {

                                // We found the user's data - set it as current for fuutre use.
                                Backendless.UserService.setCurrentUser(currentUser);

                                if(currentUser.getProperty("geofences") != null) {
                                    Utility.getInstance().setGeofences(currentUser.getProperty("geofences").toString());
                                }

                                responseCallback.onResponse();
                            }

                            @Override
                            public void handleFault(BackendlessFault fault) {
                                faultCallback.onFault(fault.getCode() , fault.getMessage());
                            }
                        });
                    }
                }
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                faultCallback.onFault(fault.getCode() , fault.getMessage());
            }
        });
    }

    public String getUserObjectId() {

        if(Backendless.UserService.CurrentUser() != null) {
            return Backendless.UserService.CurrentUser().getObjectId();
        } else {
            return "";
        }
    }

    public void setUserName(String name) {

        Backendless.UserService.CurrentUser().setProperty("name", name);
    }

    public String getUserName() {

        if(Backendless.UserService.CurrentUser() != null && Backendless.UserService.CurrentUser().getProperty("name") != null) {
            return Backendless.UserService.CurrentUser().getProperty("name").toString();
        } else {
            return "";
        }
    }

    public void setUserWorkEmail(String workEmail) {

        Backendless.UserService.CurrentUser().setProperty("workEmail", workEmail);
    }

    public String getUserWorkEmail() {

        if(Backendless.UserService.CurrentUser() != null && Backendless.UserService.CurrentUser().getProperty("workEmail") != null) {
            return Backendless.UserService.CurrentUser().getProperty("workEmail").toString();
        } else {
            return "";
        }
    }

    public void setUserWorkTitle(String workTitle) {

        Backendless.UserService.CurrentUser().setProperty("workTitle", workTitle);
    }

    public String getUserWorkTitle() {

        if(Backendless.UserService.CurrentUser() != null && Backendless.UserService.CurrentUser().getProperty("workTitle") != null) {
            return Backendless.UserService.CurrentUser().getProperty("workTitle").toString();
        } else {
            return "";
        }
    }

    public String getUserProfileImageThumbnailUrl() {

        if(Backendless.UserService.CurrentUser() != null && Backendless.UserService.CurrentUser().getProperty("profileImageThumbnailUrl") != null) {
            return Backendless.UserService.CurrentUser().getProperty("profileImageThumbnailUrl").toString();
        } else {
            return "";
        }
    }

    public String getUserProfileImageUrl() {

        if(Backendless.UserService.CurrentUser() != null && Backendless.UserService.CurrentUser().getProperty("profileImageUrl") != null) {
            return Backendless.UserService.CurrentUser().getProperty("profileImageUrl").toString();
        } else {
            return "";
        }
    }

    public String getUserMemberOfVenueItemTitle() {

        if(Backendless.UserService.CurrentUser() != null && Backendless.UserService.CurrentUser().getProperty("memberOfenueItemTitle") != null) {
            return Backendless.UserService.CurrentUser().getProperty("memberOfenueItemTitle").toString();
        } else {
            return "";
        }
    }

    public String getHostForVenueCode() {

        if(Backendless.UserService.CurrentUser() != null && Backendless.UserService.CurrentUser().getProperty("hostForVenueCode") != null) {
            return Backendless.UserService.CurrentUser().getProperty("hostForVenueCode").toString();
        } else {
            return "";
        }
    }

    public void registerUser(String name, String email, String password, final ResponseCallback responseCallback, final FaultCallback faultCallback) {

        Log.d(TAG, "registerUser called!");

        if(!Utility.isOnline()) {
            faultCallback.onFault("", CandCApplication.getAppContext().getResources().getString(R.string.unable_to_access));
            return;
        }

        // In a real app, this where we would send the user to a register screen to collect their
        // user name and password for registering a new user. For testing purposes, we will simply
        // register a test user using a hard coded user name and password.
        BackendlessUser user = new BackendlessUser();
        user.setProperty( "name", name );
        user.setEmail(email);
        user.setPassword(password);

        // Set the user's work emial to their current email.
        // They can chnage it later on the profile view.
        user.setProperty("workEmail", email);

        Backendless.UserService.register(user, new BackendlessCallback<BackendlessUser>() {

            @Override
            public void handleResponse(BackendlessUser backendlessUser) {

                Log.d(TAG, "User was registered: " + backendlessUser.getObjectId());

                CloudAnalytics.getInstance().sendEventSignUp("email");

                responseCallback.onResponse();
            }

            @Override
            public void handleFault(BackendlessFault fault) {

                Log.d(TAG, "User failed to register: " + fault.toString());
                faultCallback.onFault(fault.getCode() , fault.getMessage());
            }
        });
    }

    public void signinUser(String email, String password, final ResponseCallback responseCallback, final FaultCallback faultCallback) {

        Log.d(TAG, "signinUser called!");

        if(!Utility.isOnline()) {
            faultCallback.onFault("", CandCApplication.getAppContext().getResources().getString(R.string.unable_to_access));
            return;
        }

        Backendless.UserService.login(email, password, new AsyncCallback<BackendlessUser>() {

            public void handleResponse(BackendlessUser user) {

                Log.d(TAG, "User logged in: " + user.getUserId());
                isUserLoggedIn = new Boolean(true);
                Utility.getInstance().setIsUserLoggedIn(true);
                responseCallback.onResponse();
            }

            public void handleFault(BackendlessFault fault) {

                Log.d(TAG, "User failed to login: " + fault.toString());
                faultCallback.onFault(fault.getCode() , fault.getMessage());
            }
        }, true); // true = Stay Loggedn In!
    }

    public void signinUserViaFacebook(Activity activity, final ResponseCallback responseCallback, final FaultCallback faultCallback) {

        Log.d(TAG, "signinUserViaFacebook called!");

        if(!Utility.isOnline()) {
            faultCallback.onFault("", CandCApplication.getAppContext().getResources().getString(R.string.unable_to_access));
            return;
        }

        Map<String, String> facebookFieldMappings = new HashMap<>();
        facebookFieldMappings.put( "email", "fb_email" );

        List<String> permissions = new ArrayList<>();
        permissions.add( "email" );
        Backendless.UserService.loginWithFacebook(activity, null, new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser user) {
                Log.d(TAG, "User logged in via Facebook: " + user.getUserId());

                CloudAnalytics.getInstance().sendEventSignUp("facebook");

                isUserLoggedIn = new Boolean(true);
                Utility.getInstance().setIsUserLoggedIn(true);
                responseCallback.onResponse();
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.d(TAG, "User failed to login via Facebook: " + fault.toString());
                faultCallback.onFault(fault.getCode(), fault.getMessage());
            }
        }, true); // true = Stay Loggedn In!
    }

    public void signinUserViaTwitter(Activity activity, final ResponseCallback responseCallback, final FaultCallback faultCallback) {

        Log.d(TAG, "signinUserViaTwitter called!");

        if(!Utility.isOnline()) {
            faultCallback.onFault("", CandCApplication.getAppContext().getResources().getString(R.string.unable_to_access));
            return;
        }

        Map<String, String> twitterFieldsMappings = new HashMap<>();
        twitterFieldsMappings.put( "name", "twitter_name" );

        Backendless.UserService.loginWithTwitter(activity, twitterFieldsMappings, new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse( BackendlessUser user ) {
                Log.d(TAG, "User logged in via Twitter: " + user.getUserId());

                CloudAnalytics.getInstance().sendEventSignUp("twitter");

                isUserLoggedIn = new Boolean(true);
                Utility.getInstance().setIsUserLoggedIn(true);
                responseCallback.onResponse();
            }

            @Override
            public void handleFault( BackendlessFault fault ) {
                Log.d(TAG, "User failed to login via Twitter: " + fault.toString());
                faultCallback.onFault(fault.getCode() , fault.getMessage());
            }
        }, true ); // true = Stay Loggedn In!
    }

    public Drawable getProfileImagePlaceholder() {

        Resources res = CandCApplication.getAppContext().getResources();
        Bitmap bitmap = BitmapFactory.decodeResource(res, R.drawable.profile_placeholder);
        Drawable drawable = Utility.getInstance().createRoundDrawableFromBitmap(bitmap);

        return drawable;
    }

    public void loadUserProfileImage(final ResponseImageCallback responseImageCallback, final FaultCallback faultCallback) {

        Log.d(TAG, "loadUserProfileImage called!");

        if(!Utility.isOnline()) {
            faultCallback.onFault("", CandCApplication.getAppContext().getResources().getString(R.string.unable_to_access));
            return;
        }

        BackendlessUser currentUser = Backendless.UserService.CurrentUser();

        if(currentUser != null) {

            String profileImageUrl = null;

            if(currentUser.getProperty("profileImageUrl") != null) {
                profileImageUrl = currentUser.getProperty("profileImageUrl").toString();
            }

            if(profileImageUrl != null && !profileImageUrl.isEmpty()) {

                ImageRequest imageRequest = new ImageRequest(profileImageUrl,
                        new Response.Listener<Bitmap>() {
                            @Override
                            public void onResponse(Bitmap response) {

                                Drawable drawable = Utility.getInstance().createRoundDrawableFromBitmap(response);
                                responseImageCallback.onResponse(drawable);
                            }
                        },
                        0, // Image width
                        0, // Image height
                        ImageView.ScaleType.CENTER_CROP, // Image scale type
                        Bitmap.Config.RGB_565, //Image decode configuration

                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                //error.printStackTrace();
                                responseImageCallback.onResponse(getProfileImagePlaceholder());
                            }
                        }
                );

                RequestQueue requestQueue = Volley.newRequestQueue(CandCApplication.getAppContext());
                requestQueue.add(imageRequest);

            } else {

                if(currentUser.getProperty("socialAccount") != null) {

                    String socialAccount = currentUser.getProperty("socialAccount").toString();

                    if(socialAccount != null && socialAccount.equals("Facebook")) {

                        String userID = (String) currentUser.getProperty("id");

                        String facebookProfileUrl = "https://graph.facebook.com/" + userID +"/picture?type=large";

                        ImageRequest imageRequest = new ImageRequest(facebookProfileUrl,
                                new Response.Listener<Bitmap>() {
                                    @Override
                                    public void onResponse(final Bitmap response) {

                                        uploadProfileImage(response, new ResponseImageCallback() {
                                            @Override
                                            public void onResponse(Drawable drawable) {
                                                responseImageCallback.onResponse(drawable);
                                            }
                                        }, new FaultCallback() {
                                            @Override
                                            public void onFault(String code, String message) {
                                                responseImageCallback.onResponse(getProfileImagePlaceholder());
                                            }
                                        });
                                    }
                                },
                                0, // Image width
                                0, // Image height
                                ImageView.ScaleType.CENTER_CROP, // Image scale type
                                Bitmap.Config.RGB_565, //Image decode configuration

                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        //error.printStackTrace();
                                        responseImageCallback.onResponse(getProfileImagePlaceholder());
                                    }
                                }
                        );

                        RequestQueue requestQueue = Volley.newRequestQueue(CandCApplication.getAppContext());
                        requestQueue.add(imageRequest);

                    } else if(socialAccount != null && socialAccount.equals("Twitter")) {

                        String userTwitterName = (String) currentUser.getProperty("name");

                        String twitterProfileUrl = "https://twitter.com/" + userTwitterName + "/profile_image?size=original";

                        ImageRequest imageRequest = new ImageRequest(twitterProfileUrl,
                                new Response.Listener<Bitmap>() {
                                    @Override
                                    public void onResponse(Bitmap response) {

                                        uploadProfileImage(response, new ResponseImageCallback() {
                                            @Override
                                            public void onResponse(Drawable drawable) {
                                                responseImageCallback.onResponse(drawable);
                                            }
                                        }, new FaultCallback() {
                                            @Override
                                            public void onFault(String code, String message) {
                                                responseImageCallback.onResponse(getProfileImagePlaceholder());
                                            }
                                        });
                                    }
                                },
                                0, // Image width
                                0, // Image height
                                ImageView.ScaleType.CENTER_CROP, // Image scale type
                                Bitmap.Config.RGB_565, //Image decode configuration

                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        //error.printStackTrace();
                                        responseImageCallback.onResponse(getProfileImagePlaceholder());
                                    }
                                }
                        );

                        RequestQueue requestQueue = Volley.newRequestQueue(CandCApplication.getAppContext());
                        requestQueue.add(imageRequest);

                    } else {
                        responseImageCallback.onResponse(getProfileImagePlaceholder());
                    }

                } else {
                    responseImageCallback.onResponse(getProfileImagePlaceholder());
                }
            }

        } else {
            faultCallback.onFault("" , "Invalid User.");
        }
    }

    public void removePhotoAndThumbnail(String photoUrl, String thumbnailUrl) {

        // Get just the file name which is everything after "/files/".
        String photoFile = photoUrl.substring(photoUrl.lastIndexOf("/files/") + 6);

        Backendless.Files.remove(photoFile, new AsyncCallback<Void>() {
            @Override
            public void handleResponse(Void response) {
            }

            @Override
            public void handleFault(BackendlessFault fault) {
            }
        });

        String thumbnailFile = thumbnailUrl.substring(thumbnailUrl.lastIndexOf("/files/") + 6);

        Backendless.Files.remove(thumbnailFile, new AsyncCallback<Void>() {
            @Override
            public void handleResponse(Void response) {
            }

            @Override
            public void handleFault(BackendlessFault fault) {
            }
        });
    }

    public void uploadProfileImage(Bitmap fullSizeImage, final ResponseImageCallback responseImageCallback, final FaultCallback faultCallback) {

        Log.d(TAG, "uploadProfileImage called!");

        if(!Utility.isOnline()) {
            faultCallback.onFault("", CandCApplication.getAppContext().getResources().getString(R.string.unable_to_access));
            return;
        }

        final BackendlessUser currentUser = Backendless.UserService.CurrentUser();

        if(currentUser == null) {
            faultCallback.onFault("", "Invalid user.");
            return;
        }

        // Cache old URLs for removal!
        final String oldProfileImageUrl;
        final String oldProfileImageThumbnailUrl;

        if(currentUser.getProperty("profileImageUrl") != null) {
            oldProfileImageUrl = currentUser.getProperty("profileImageUrl").toString();
        } else {
            oldProfileImageUrl = null;
        }

        if(currentUser.getProperty("profileImageThumbnailUrl") != null) {
            oldProfileImageThumbnailUrl = currentUser.getProperty("profileImageThumbnailUrl").toString();
        } else {
            oldProfileImageThumbnailUrl = null;
        }

        final String uuid = UUID.randomUUID().toString().toUpperCase();

        int dimension = Utility.getInstance().getSquareCropDimensionForBitmap(fullSizeImage);
        final Bitmap squareFullsizeBitmap = ThumbnailUtils.extractThumbnail(fullSizeImage, dimension, dimension);
        Bitmap thumbnailBitmap = ThumbnailUtils.extractThumbnail(fullSizeImage, 75, 75);

        //
        // Upload thumbnail image...
        //

        Backendless.Files.Android.upload(thumbnailBitmap, Bitmap.CompressFormat.JPEG, 100, currentUser.getObjectId() + "/thumb_" + uuid + ".jpg", "photos", true,
                new AsyncCallback<BackendlessFile>() {
                    @Override
                    public void handleResponse(final BackendlessFile backendlessFile) {

                        final String profileImageThumbnailUrl = backendlessFile.getFileURL();

                        //
                        // Upload full size image...
                        //

                        Backendless.Files.Android.upload(squareFullsizeBitmap, Bitmap.CompressFormat.JPEG, 80, currentUser.getObjectId() + "/full_" + uuid + ".jpg", "photos", true,
                                new AsyncCallback<BackendlessFile>() {
                                    @Override
                                    public void handleResponse(final BackendlessFile backendlessFile) {

                                        String profileImageUrl = backendlessFile.getFileURL();

                                        addProfileImageUrlToUser(profileImageUrl, profileImageThumbnailUrl, new ResponseCallback() {
                                            @Override
                                            public void onResponse() {

                                                Drawable drawable = Utility.getInstance().createRoundDrawableFromBitmap(squareFullsizeBitmap);
                                                responseImageCallback.onResponse(drawable);

                                                if(oldProfileImageUrl != null && oldProfileImageThumbnailUrl != null) {
                                                    removePhotoAndThumbnail(oldProfileImageUrl, oldProfileImageThumbnailUrl);
                                                }
                                            }
                                        }, new FaultCallback() {
                                            @Override
                                            public void onFault(String code, String message) {
                                                faultCallback.onFault(code, message);
                                            }
                                        });
                                    }

                                    @Override
                                    public void handleFault(BackendlessFault fault) {
                                        Log.d(TAG, "Failed to uload image: " + fault.toString());
                                        faultCallback.onFault(fault.getCode() , fault.getMessage());
                                    }
                                });
                    }

                    @Override
                    public void handleFault(BackendlessFault fault) {
                        Log.d(TAG, "Failed to uload image: " + fault.toString());
                        faultCallback.onFault(fault.getCode() , fault.getMessage());
                    }
                });
    }

    public void signoutUser(final ResponseCallback responseCallback, final FaultCallback faultCallback) {

        Log.d(TAG, "signoutUser called!");

        if(!Utility.isOnline()) {
            faultCallback.onFault("", CandCApplication.getAppContext().getResources().getString(R.string.unable_to_access));
            return;
        }

        Backendless.UserService.logout(new AsyncCallback<Void>() {
            @Override
            public void handleResponse(Void response) {

                Log.d(TAG, "User logged out: ");

                isUserLoggedIn = null;
                Utility.getInstance().setIsUserLoggedIn(false);

                Utility.getInstance().leaveCurrentEvent();
                responseCallback.onResponse();
            }

            @Override
            public void handleFault(BackendlessFault fault) {

                Log.d(TAG, "User failed to log out: " + fault.toString());
                faultCallback.onFault(fault.getCode() , fault.getMessage());
            }
        });
    }

    public void registerDeviceForPush() {

        if(Utility.getInstance().getVenueCode() == null) {
            return;
        }

        if(Utility.getInstance().getDeviceId() != null) {
            return;
        }

        String channelName = Utility.getInstance().getVenueCode();

        Backendless.Messaging.registerDevice(GOOGLE_PROJECT_ID, channelName, new AsyncCallback<Void>() {
            @Override
            public void handleResponse(Void response) {
                Log.d(TAG, "registerDeviceForPush response");

                Backendless.Messaging.getDeviceRegistration(new AsyncCallback<DeviceRegistration>() {
                    @Override
                    public void handleResponse(DeviceRegistration response) {

                        if(response != null) {

                            Log.d(TAG, "deviceId = " + String.valueOf(response.getDeviceId()));
                            addDeviceIdToUser(response.getDeviceId());

                        } else {
                            Log.d(TAG, "getDeviceRegistration failed");
                        }
                    }

                    @Override
                    public void handleFault(BackendlessFault fault) {
                        Log.d(TAG, "getDeviceRegistration failed");
                    }
                });
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.d(TAG, "registerDeviceForPush failed: " + fault.toString());
            }
        });
    }

    public void unregisterDeviceForPush() {

        if(Utility.getInstance().getDeviceId() == null) {
            return;
        }

        Backendless.Messaging.unregisterDevice(new AsyncCallback<Void>() {
            @Override
            public void handleResponse(Void response) {
                Log.d(TAG, "Unregister device!");
                removeDeviceIdFromUser();
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.d(TAG, "Failed to unregister device: " + fault.toString());
            }
        });
    }

    public void addProfileImageUrlToUser(String profileImageUrl, String profileImageThumbnailUrl,
                                         final ResponseCallback responseCallback, final FaultCallback faultCallback) {

        BackendlessUser user = Backendless.UserService.CurrentUser();

        if(user != null) {

            user.setProperty("profileImageUrl", profileImageUrl);
            user.setProperty("profileImageThumbnailUrl", profileImageThumbnailUrl);

            Backendless.UserService.update(user, new AsyncCallback<BackendlessUser>() {

                public void handleResponse(BackendlessUser user) {
                    responseCallback.onResponse();
                }

                public void handleFault(BackendlessFault fault) {
                    faultCallback.onFault(fault.getCode() , fault.getMessage());
                }
            });
        }
    }

    public void addDeviceIdToUser(final String deviceId) {

        BackendlessUser user = Backendless.UserService.CurrentUser();

        if(user != null) {

            user.setProperty("deviceId", deviceId);

            Backendless.UserService.update(user, new AsyncCallback<BackendlessUser>() {

                public void handleResponse(BackendlessUser user) {
                    Utility.getInstance().setDeviceId(deviceId);
                }

                public void handleFault(BackendlessFault fault) {
                    Log.d(TAG, "addDeviceIdToUser failed: " + fault.toString());
                }
            });
        }
    }

    public void removeDeviceIdFromUser() {

        BackendlessUser user = Backendless.UserService.CurrentUser();

        if(user != null) {

            user.setProperty("deviceId", null);

            Backendless.UserService.update(user, new AsyncCallback<BackendlessUser>() {

                public void handleResponse(BackendlessUser user) {
                    Log.d(TAG, "removeDeviceIdFromUser: removed deviceId from User " + user.getObjectId());
                    Utility.getInstance().setDeviceId(null);
                }

                public void handleFault(BackendlessFault fault) {
                    Log.d(TAG, "removeDeviceIdFromUser failed to remove deviceId from User:" + fault.toString());
                }
            });
        }
    }

    public void saveUser(final ResponseCallback responseCallback, final FaultCallback faultCallback) {

        if(!Utility.isOnline()) {
            faultCallback.onFault("", CandCApplication.getAppContext().getResources().getString(R.string.unable_to_access));
            return;
        }

        BackendlessUser user = Backendless.UserService.CurrentUser();

        if(user != null) {

            Backendless.UserService.update(user, new AsyncCallback<BackendlessUser>() {

                public void handleResponse(BackendlessUser user) {
                    responseCallback.onResponse();
                }

                public void handleFault(BackendlessFault fault) {
                    Log.d(TAG, "saveUser failed: " + fault.toString());
                    faultCallback.onFault(fault.getCode() , fault.getMessage());
                }
            });
        }
    }

    public void loadUser(String userObjectId, final UserResponseCallback userResponseCallback, final FaultCallback faultCallback) {

        if(!Utility.isOnline()) {
            faultCallback.onFault("", CandCApplication.getAppContext().getResources().getString(R.string.unable_to_access));
            return;
        }

        Backendless.Persistence.of(BackendlessUser.class).findById(userObjectId, new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser response) {

                if(response != null) {
                    userResponseCallback.onData(response);
                } else {
                    faultCallback.onFault("", "Failed to find the User.");
                }
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                faultCallback.onFault(fault.getCode(), fault.getMessage());
            }
        });
    }

    public void sendChatPushNotification(String deviceId, String chatSessionId) {

        if(!Utility.isOnline()) {
            return;
        }

        if(deviceId == null || deviceId.isEmpty()) {
            return;
        }

        String channelName = Utility.getInstance().getVenueCode();

        if(channelName == null) {
            return;
        }

        String chatName = Cloud.getInstance().getUserName();

        PublishOptions publishOptions = new PublishOptions();

        publishOptions.putHeader("publisher_name", "BeSocial Net");

        publishOptions.putHeader("android-content-title", chatName + " sent you a message.");
        publishOptions.putHeader("android-content-text", "Tap to view message.");
        publishOptions.putHeader("android-ticker-text", chatName + " sent you a message.");

        publishOptions.putHeader("ios-alert", chatName + " sent you a message.");
        publishOptions.putHeader("ios-sound", "default");
// TODO: Add support for badge count! Can't do it now unless we track the badge count for every user!
        //publishOptions.putHeader( "ios-badge", "1" );

        publishOptions.putHeader("venue-code", Utility.getInstance().getVenueCode());
        publishOptions.putHeader("chat-session-id", chatSessionId);

        DeliveryOptions deliveryOptions = new DeliveryOptions();
        deliveryOptions.setPushPolicy(PushPolicyEnum.ONLY);
        deliveryOptions.addPushSinglecast(deviceId);

        Backendless.Messaging.publish(channelName, "default", publishOptions, deliveryOptions, new AsyncCallback<MessageStatus>() {
            @Override
            public void handleResponse(MessageStatus response) {
                //Log.e(TAG, "Backendless.Messaging.publish: response = " + response.toString());
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.d(TAG, "sendPush failed: " + fault.toString());
            }
        });

    }

    public void sendBroadcastPush(String message, Date date, String venueItemId, String venueItemTitle,
                                  final MessageResponseCallback messageResponseCallback, final FaultCallback faultCallback) {

        if(!Utility.isOnline()) {
            return;
        }

        String channelName = Utility.getInstance().getVenueCode();

        if(channelName == null) {
            return;
        }

        String title = "A special message from your host!"; // Default title!

        if(venueItemTitle != null) {
            title = "A special message from " + venueItemTitle + "!";
        } else {
            if(Utility.getInstance().getVenueTitle() != null) {
                title = "A special message from " + Utility.getInstance().getVenueTitle() + "!";
            }
        }

        String truncatedMesaage = message;
        if(truncatedMesaage.length() > 200) {
            truncatedMesaage = truncatedMesaage.substring(0, 200);
        }

        PublishOptions publishOptions = new PublishOptions();

        publishOptions.putHeader("publisher_name", "BeSocial Net");

        publishOptions.putHeader("android-content-title", truncatedMesaage);
        publishOptions.putHeader("android-content-text", "Tap to view message.");
        publishOptions.putHeader("android-ticker-text", truncatedMesaage);

        publishOptions.putHeader("ios-alert", truncatedMesaage);
        publishOptions.putHeader("ios-sound", "default");
// TODO: Add support for badge count! Can't do it now unless we track the badge count for every user!
        //publishOptions.putHeader( "ios-badge", "1" );

        publishOptions.putHeader("venue-code", Utility.getInstance().getVenueCode());
        publishOptions.putHeader("venue-message", message);

        // Check for this optional item!
        if(venueItemId != null) {
            publishOptions.putHeader("venue-item-id", venueItemId);
        }

        DeliveryOptions deliveryOptions = new DeliveryOptions();
        deliveryOptions.setPushPolicy(PushPolicyEnum.ONLY);
        deliveryOptions.setPushBroadcast(PushBroadcastMask.ALL);

// TEST ONLY:
//deliveryOptions.addPushSinglecast("FA71M0300038"); // Pixel
//deliveryOptions.addPushSinglecast("080066790068d8a1"); // Nexus
//deliveryOptions.addPushSinglecast("1FC25D5B-9A76-4669-8D26-3B3F90FAD1CF"); // Facebook Login on my iPhone.

        if(date != null) {
            deliveryOptions.setPublishAt(date);
        }

        Backendless.Messaging.publish(channelName, "default", publishOptions, deliveryOptions, new AsyncCallback<MessageStatus>() {
            @Override
            public void handleResponse(MessageStatus response) {
                messageResponseCallback.onResponse(response.getMessageId());
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.d(TAG, "sendPush failed: " + fault.toString());
                faultCallback.onFault(fault.getCode(), fault.getMessage());
            }
        });
    }

    public void addVenueCodeToUser(final String venueCode, final ResponseCallback responseCallback, final FaultCallback faultCallback) {

        if(!Utility.isOnline()) {
            faultCallback.onFault("", CandCApplication.getAppContext().getResources().getString(R.string.unable_to_access));
            return;
        }

        BackendlessUser user = Backendless.UserService.CurrentUser();

        if(user != null) {

            user.setProperty("venueCode", venueCode);

            Backendless.UserService.update(user, new AsyncCallback<BackendlessUser>() {

                public void handleResponse(BackendlessUser user) {
                    responseCallback.onResponse();
                }

                public void handleFault(BackendlessFault fault) {
                    Log.d(TAG, "addVenueCodeToUser failed: " + fault.toString());
                    faultCallback.onFault(fault.getCode() , fault.getMessage());
                }
            });
        }
    }

    public void removeVenueCodeFromUser(final ResponseCallback responseCallback, final FaultCallback faultCallback) {

        if(!Utility.isOnline()) {
            faultCallback.onFault("", CandCApplication.getAppContext().getResources().getString(R.string.unable_to_access));
            return;
        }

        BackendlessUser user = Backendless.UserService.CurrentUser();

        if(user != null) {

            user.setProperty("venueCode", "");

            Backendless.UserService.update(user, new AsyncCallback<BackendlessUser>() {

                public void handleResponse(BackendlessUser user) {
                    responseCallback.onResponse();
                }

                public void handleFault(BackendlessFault fault) {
                    Log.d(TAG, "removeVenueCodeFromUser failed: " + fault.toString());
                    faultCallback.onFault(fault.getCode() , fault.getMessage());
                }
            });
        }
    }

    public String getDeviceId() {

        DeviceRegistration devReg = Backendless.Messaging.getDeviceRegistration();

        if(devReg != null) {
            return devReg.getDeviceId();
        } else {
            return "";
        }
    }

    public void updateUserDataOnChatSessions() {

        if(!Utility.isOnline()) {
            return;
        }

        final BackendlessDataQuery dataQuery = new BackendlessDataQuery("ownerId = '" + getUserObjectId() + "' OR targetId = '" + getUserObjectId() + "'");

        Backendless.Persistence.of(ChatSession.class).find(dataQuery, new AsyncCallback<BackendlessCollection<ChatSession>>(){

            @Override
            public void handleResponse( BackendlessCollection<ChatSession> foundChatSessions ) {

                Log.i(TAG, "ChatSession have been fetched:");

                for(ChatSession chatSession : foundChatSessions.getData()) {

                    if(chatSession.getObjectId().equals(getUserObjectId())) {

                        chatSession.setOwnerName(getUserName());
                        chatSession.setOwnerTitle(getUserWorkTitle());
                        chatSession.setOwnerThumbnailUrl(getUserProfileImageThumbnailUrl());

                    } else if(chatSession.getTargetId().equals(getUserObjectId())) {

                        chatSession.setTargetName(getUserName());
                        chatSession.setTargetTitle(getUserWorkTitle());
                        chatSession.setTargetThumbnailUrl(getUserProfileImageThumbnailUrl());
                    }

                    saveChatSession(chatSession, new SaveChatSessionResponseCallback() {
                        @Override
                        public void onData(ChatSession chatSession) {

                        }
                    }, new FaultCallback() {
                        @Override
                        public void onFault(String code, String message) {

                        }
                    });
                }
            }

            @Override
            public void handleFault( BackendlessFault fault ) {

                Log.i(TAG, "No ChatSession were fetched using the whereClause: " + dataQuery.getWhereClause() + ": " + fault.toString());
            }
        });
    }

    public void loadChatSessions(final ChatSessionsResponseCallback chatSessionsResponseCallback, final FaultCallback faultCallback) {

        if(!Utility.isOnline()) {
            faultCallback.onFault("", CandCApplication.getAppContext().getResources().getString(R.string.unable_to_access));
            return;
        }

        final BackendlessDataQuery dataQuery = new BackendlessDataQuery("venueCode = '" + Utility.getInstance().getVenueCode() + "' AND (ownerId = '" + getUserObjectId() + "' OR targetId = '" + getUserObjectId() + "')");

        Backendless.Persistence.of(ChatSession.class).find(dataQuery, new AsyncCallback<BackendlessCollection<ChatSession>>(){

            @Override
            public void handleResponse( BackendlessCollection<ChatSession> foundChatSessions ) {

                Log.i(TAG, "ChatSession have been fetched:");

                ArrayList<ChatSession> chatSessions = new ArrayList<>();

                for(ChatSession chatSession : foundChatSessions.getData()) {
                    chatSessions.add(chatSession);
                }

                chatSessionsResponseCallback.onData(chatSessions);
            }

            @Override
            public void handleFault( BackendlessFault fault ) {

                Log.i(TAG, "No ChatSession were fetched using the whereClause: " + dataQuery.getWhereClause() + ": " + fault.toString());
                faultCallback.onFault(fault.getCode(), fault.getMessage());
            }
        });
    }

    public void loadChatSessionWithUser(String objectId, final ChatSessionResponseCallback chatSessionResponseCallback, final FaultCallback faultCallback) {

        if(!Utility.isOnline()) {
            faultCallback.onFault("", CandCApplication.getAppContext().getResources().getString(R.string.unable_to_access));
            return;
        }

        final BackendlessDataQuery dataQuery = new BackendlessDataQuery("venueCode = '" + Utility.getInstance().getVenueCode() +
                "' AND ((ownerId = '" + objectId + "' AND targetId = '" + getUserObjectId() + "') OR (ownerId = '" + getUserObjectId() + "' AND targetId = '" + objectId + "'))");

        Backendless.Persistence.of(ChatSession.class).find(dataQuery, new AsyncCallback<BackendlessCollection<ChatSession>>(){

            @Override
            public void handleResponse( BackendlessCollection<ChatSession> foundChatSessions ) {

                Log.i(TAG, "ChatSession has been fetched:");

                // There should only ever be one!
                if(foundChatSessions.getData().size() > 0) {
                    chatSessionResponseCallback.onData(foundChatSessions.getData().get(0));
                } else {
                    faultCallback.onFault("", "Failed to find the ChatSession");
                }
            }

            @Override
            public void handleFault( BackendlessFault fault ) {

                Log.i(TAG, "No ChatSession were fetched using the whereClause: " + dataQuery.getWhereClause() + ": " + fault.toString());
                faultCallback.onFault(fault.getCode(), fault.getMessage());
            }
        });
    }

    public void loadChatSession(String chatSessionId, final ChatSessionResponseCallback chatSessionResponseCallback, final FaultCallback faultCallback) {

        if(!Utility.isOnline()) {
            faultCallback.onFault("", CandCApplication.getAppContext().getResources().getString(R.string.unable_to_access));
            return;
        }

        Backendless.Persistence.of(ChatSession.class).findById(chatSessionId, new AsyncCallback<ChatSession>() {
            @Override
            public void handleResponse(ChatSession response) {

                if(response != null) {
                    chatSessionResponseCallback.onData(response);
                } else {
                    faultCallback.onFault("", "Failed to find the ChatSession");
                }
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                faultCallback.onFault(fault.getCode(), fault.getMessage());
            }
        });
    }

    public void saveChatSession(ChatSession chatSession, final SaveChatSessionResponseCallback saveChatSessionResponseCallback, final FaultCallback faultCallback) {

        if(!Utility.isOnline()) {
            faultCallback.onFault("", CandCApplication.getAppContext().getResources().getString(R.string.unable_to_access));
            return;
        }

        Backendless.Persistence.save(chatSession, new AsyncCallback<ChatSession>() {

            @Override
            public void handleResponse(ChatSession chatSession) {
                saveChatSessionResponseCallback.onData(chatSession);
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                faultCallback.onFault(fault.getCode(), fault.getMessage());
            }
        });
    }

    public void saveScheduledMessage(ScheduledMessage scheduledMessage, final SaveScheduledMessageResponseCallback saveScheduledMessageResponseCallback, final FaultCallback faultCallback) {

        if(!Utility.isOnline()) {
            faultCallback.onFault("", CandCApplication.getAppContext().getResources().getString(R.string.unable_to_access));
            return;
        }

        Backendless.Persistence.save(scheduledMessage, new AsyncCallback<ScheduledMessage>() {

            @Override
            public void handleResponse(ScheduledMessage scheduledMessage) {
                saveScheduledMessageResponseCallback.onData(scheduledMessage);
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                faultCallback.onFault(fault.getCode(), fault.getMessage());
            }
        });
    }

    public void removeScheduledMessage(final ScheduledMessage scheduledMessage, final RemoveScheduledMessageResponseCallback removeScheduledMessageResponseCallback, final FaultCallback faultCallback) {

        if(!Utility.isOnline()) {
            faultCallback.onFault("", CandCApplication.getAppContext().getResources().getString(R.string.unable_to_access));
            return;
        }

        Backendless.Persistence.of(ScheduledMessage.class).findById(scheduledMessage.getObjectId(), new AsyncCallback<ScheduledMessage>() {
            @Override
            public void handleResponse(ScheduledMessage response) {

                if(response != null) {

                    Backendless.Persistence.of(ScheduledMessage.class).remove(response, new AsyncCallback<Long>() {
                        @Override
                        public void handleResponse(Long response) {
                            removeScheduledMessageResponseCallback.onResponse();
                        }

                        @Override
                        public void handleFault(BackendlessFault fault) {
                            faultCallback.onFault(fault.getCode(), fault.getMessage());
                        }
                    });

                } else {
                    faultCallback.onFault("", "Message was canceled, but failed to remove the message entry from the database.");
                }
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                faultCallback.onFault(fault.getCode(), fault.getMessage());
            }
        });
    }

    public void cancelScheduleMessage(final ScheduledMessage scheduledMessage, final CancelScheduledMessageResponseCallback cancelScheduledMessageResponseCallback, final FaultCallback faultCallback) {

        if(!Utility.isOnline()) {
            faultCallback.onFault("", CandCApplication.getAppContext().getResources().getString(R.string.unable_to_access));
            return;
        }

        Backendless.Messaging.cancel(scheduledMessage.getMessageId(), new AsyncCallback<MessageStatus>() {
            @Override
            public void handleResponse(MessageStatus response) {

                removeScheduledMessage(scheduledMessage, new RemoveScheduledMessageResponseCallback() {
                    @Override
                    public void onResponse() {
                        cancelScheduledMessageResponseCallback.onResponse();
                    }
                }, new FaultCallback() {
                    @Override
                    public void onFault(String code, String message) {
                        faultCallback.onFault(code, message);
                    }
                });
            }

            @Override
            public void handleFault(BackendlessFault fault) {

                if(fault.getCode().equals("5040")) {

                    // If the error code is 5040, the messages has either already been canceled or
                    // has already been delivered. Either way, treat it as a success and remove
                    // the entry for it.
                    removeScheduledMessage(scheduledMessage, new RemoveScheduledMessageResponseCallback() {
                        @Override
                        public void onResponse() {
                            cancelScheduledMessageResponseCallback.onResponse();
                        }
                    }, new FaultCallback() {
                        @Override
                        public void onFault(String code, String message) {
                            faultCallback.onFault(code, message);
                        }
                    });

                } else {
                    faultCallback.onFault(fault.getCode(), fault.getMessage());
                }
            }
        });
    }

    public void loadScheduledMessages(final ScheduledMessagesResponseCallback scheduledMessagesResponseCallback, final FaultCallback faultCallback) {

        if(!Utility.isOnline()) {
            faultCallback.onFault("", CandCApplication.getAppContext().getResources().getString(R.string.unable_to_access));
            return;
        }

        final BackendlessDataQuery dataQuery = new BackendlessDataQuery("venueCode = '" + Utility.getInstance().getVenueCode() + "'");

        QueryOptions queryOptions = new QueryOptions();
        queryOptions.addSortByOption( "deliveryDate ASC" );
        dataQuery.setQueryOptions(queryOptions);

        Backendless.Persistence.of(ScheduledMessage.class).find(dataQuery, new AsyncCallback<BackendlessCollection<ScheduledMessage>>(){

            @Override
            public void handleResponse(BackendlessCollection<ScheduledMessage> foundScheduledMessages) {

                Log.i(TAG, "ScheduledMessage have been fetched:");

                ArrayList<ScheduledMessage> scheduledMessages = new ArrayList<>();

                for(ScheduledMessage scheduledMessage : foundScheduledMessages.getData()) {
                    scheduledMessages.add(scheduledMessage);
                }

                scheduledMessagesResponseCallback.onData(scheduledMessages);
            }

            @Override
            public void handleFault( BackendlessFault fault ) {

                Log.i(TAG, "No ScheduledMessage were fetched using the whereClause: " + dataQuery.getWhereClause() + ": " + fault.toString());
                faultCallback.onFault(fault.getCode(), fault.getMessage());
            }
        });
    }

    public void loadVenues(final VenuesResponseCallback venuesResponseCallback, final FaultCallback faultCallback) {

        if(!Utility.isOnline()) {
            faultCallback.onFault("", CandCApplication.getAppContext().getResources().getString(R.string.unable_to_access));
            return;
        }

        String whereClause = "live = true";
        final BackendlessDataQuery dataQuery = new BackendlessDataQuery(whereClause);

        QueryOptions queryOptions = new QueryOptions();
        queryOptions.addSortByOption( "title ASC" );
        dataQuery.setQueryOptions(queryOptions);

        Backendless.Persistence.of(Venue.class).find(dataQuery, new AsyncCallback<BackendlessCollection<Venue>>() {
            @Override
            public void handleResponse(BackendlessCollection<Venue> foundEvents) {

                Log.i(TAG, "Venues have been fetched:");

                ArrayList<Venue> venues = new ArrayList<>();

                for(Venue venue : foundEvents.getData()) {
                    venues.add(venue);
                }

                venuesResponseCallback.onData(venues);
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                faultCallback.onFault(fault.getCode(), fault.getMessage());
            }
        });
    }

    public void loadAttendeesByPage(int pageSize, final int pageOffset, String searchText, String venueItemTitle, final AttendeesPageResponseCallback attendeesPageResponseCallback, final FaultCallback faultCallback) {

        if(!Utility.isOnline()) {
            faultCallback.onFault("", CandCApplication.getAppContext().getResources().getString(R.string.unable_to_access));
            return;
        }

        String whereClause = "venueCode = '" + Utility.getInstance().getVenueCode() + "' AND objectId != '" + getUserObjectId()+ "'";

        // If search text was passed - append it to the end of the query.
        if(searchText != null) {
            // Replace any ' characters with the substring '' to keep the query from breaking.
            String searchTextCleaned = searchText.replace("'", "''");
            whereClause = whereClause + " AND name LIKE '%" + searchTextCleaned + "%'";
        }

        // If we're searching for members of a certain veneuItem - append it to the end of the query.
        if(venueItemTitle != null) {
            // Replace any ' characters with the substring '' to keep the query from breaking.
            String venueItemTitleCleaned = venueItemTitle.replace("'", "''");
            whereClause = whereClause + " AND memberOfVenueItemTitle LIKE '%" + venueItemTitleCleaned + "%'";
        }

        final BackendlessDataQuery dataQuery = new BackendlessDataQuery(whereClause);

        QueryOptions queryOptions = new QueryOptions();
        queryOptions.setPageSize(pageSize);
        queryOptions.setOffset(pageOffset);
        dataQuery.setQueryOptions(queryOptions);

        Backendless.Data.of(BackendlessUser.class).find(dataQuery, new AsyncCallback<BackendlessCollection<BackendlessUser>>() {
            @Override
            public void handleResponse(BackendlessCollection<BackendlessUser> foundUsers) {

                Log.i(TAG, "BackendlessUser have been fetched:");

                ArrayList<BackendlessUser> users = new ArrayList<>();

                for(BackendlessUser user : foundUsers.getData()) {
                    users.add(user);
                }

                int totalObjects = foundUsers.getTotalObjects();
                int count = foundUsers.getData().size();

                //Log.e("XXX","totalObjects = " + String.valueOf(totalObjects) + ", count = " + String.valueOf(count));

                attendeesPageResponseCallback.onData(users, pageOffset + count, totalObjects);
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                faultCallback.onFault(fault.getCode(), fault.getMessage());
            }
        });
    }

    public void loadVenueItemsByPage(int pageSize, final int pageOffset, String searchText, final VenueItemPageResponseCallback venueItemPageResponseCallback, final FaultCallback faultCallback) {

        if(!Utility.isOnline()) {
            faultCallback.onFault("", CandCApplication.getAppContext().getResources().getString(R.string.unable_to_access));
            return;
        }

        String whereClause = "venueCode = '" + Utility.getInstance().getVenueCode() + "'";

        // If search text was passed - append it to the end of the query.
        if(searchText != null) {
            // Replace any ' characters with the substring '' to keep the query from breaking.
            String searchTextCleaned = searchText.replace("'", "''");
            whereClause = whereClause + " AND title LIKE '%" + searchTextCleaned + "%'";
        }

        final BackendlessDataQuery dataQuery = new BackendlessDataQuery(whereClause);

        QueryOptions queryOptions = new QueryOptions();
        queryOptions.setPageSize(pageSize);
        queryOptions.setOffset(pageOffset);
        queryOptions.addSortByOption( "title ASC" );
        dataQuery.setQueryOptions(queryOptions);

        Backendless.Persistence.of(VenueItem.class).find(dataQuery, new AsyncCallback<BackendlessCollection<VenueItem>>(){
            @Override
            public void handleResponse(BackendlessCollection<VenueItem> foundVenueItems) {

                Log.i(TAG, "VenueItems have been fetched:");

                ArrayList<VenueItem> venueItems = new ArrayList<>();

                for(VenueItem venueItem : foundVenueItems.getData()) {
                    venueItems.add(venueItem);
                }

                int totalObjects = foundVenueItems.getTotalObjects();
                int count = foundVenueItems.getData().size();

                //Log.e("XXX","totalObjects = " + String.valueOf(totalObjects) + ", count = " + String.valueOf(count));

                venueItemPageResponseCallback.onData(venueItems, pageOffset + count, totalObjects);
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                faultCallback.onFault(fault.getCode(), fault.getMessage());
            }
        });
    }

    public void loadVenueItems(final VenueItemsResponseCallback venueItemsResponseCallback, final FaultCallback faultCallback) {

        if(!Utility.isOnline()) {
            faultCallback.onFault("", CandCApplication.getAppContext().getResources().getString(R.string.unable_to_access));
            return;
        }

        String whereClause = "venueCode = '" + Utility.getInstance().getVenueCode() + "'";

        final BackendlessDataQuery dataQuery = new BackendlessDataQuery(whereClause);

        Backendless.Persistence.of(VenueItem.class).find(dataQuery, new AsyncCallback<BackendlessCollection<VenueItem>>(){
            @Override
            public void handleResponse(BackendlessCollection<VenueItem> foundVenueItems) {

                Log.i(TAG, "VenueItems have been fetched:");

                ArrayList<VenueItem> venueItems = new ArrayList<>();

                for(VenueItem venueItem : foundVenueItems.getData()) {
                    venueItems.add(venueItem);
                }

                int totalObjects = foundVenueItems.getTotalObjects();
                int count = foundVenueItems.getData().size();

                //Log.e("XXX","totalObjects = " + String.valueOf(totalObjects) + ", count = " + String.valueOf(count));

                venueItemsResponseCallback.onData(venueItems);
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                faultCallback.onFault(fault.getCode(), fault.getMessage());
            }
        });
    }

    public void loadVenueItem(String venueItemId, final VenueItemResponseCallback venueItemResponseCallback, final FaultCallback faultCallback) {

        if(!Utility.isOnline()) {
            faultCallback.onFault("", CandCApplication.getAppContext().getResources().getString(R.string.unable_to_access));
            return;
        }

        Backendless.Persistence.of(VenueItem.class).findById(venueItemId, new AsyncCallback<VenueItem>() {
            @Override
            public void handleResponse(VenueItem response) {

                if(response != null) {
                    venueItemResponseCallback.onData(response);
                } else {
                    faultCallback.onFault("", "Failed to find the VenueItem");
                }
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                faultCallback.onFault(fault.getCode(), fault.getMessage());
            }
        });
    }

    public void findVenue(final String venueCode, final VenueResponseCallback venueResponseCallback, final FaultCallback faultCallback) {

        if(!Utility.isOnline()) {
            faultCallback.onFault("", CandCApplication.getAppContext().getResources().getString(R.string.unable_to_access));
            return;
        }

        final BackendlessDataQuery dataQuery = new BackendlessDataQuery("venueCode = '" + venueCode + "'");

        Backendless.Persistence.of(Venue.class).find(dataQuery, new AsyncCallback<BackendlessCollection<Venue>>(){

            @Override
            public void handleResponse( BackendlessCollection<Venue> foundEvents ) {

                Log.i(TAG, "Events have been fetched:");

                if(foundEvents.getData().isEmpty()) {
                    faultCallback.onFault("", "Failed to find any event using the ID '" + String.valueOf(venueCode) + "'");
                } else {

                    Venue venue = foundEvents.getData().get(0);

                    if(venue != null) {
                        venueResponseCallback.onData(venue);
                    } else {
                        faultCallback.onFault("", "Failed to find any event using the ID '" + String.valueOf(venueCode) + "'");
                    }
                }
            }

            @Override
            public void handleFault( BackendlessFault fault ) {

                Log.i(TAG, "No Events were fetched using the whereClause: " + dataQuery.getWhereClause() + ": " + fault.toString());
                faultCallback.onFault(fault.getCode(), fault.getMessage());
            }
        });
    }

    public void findVenueByObjectId(final String objectId, final VenueResponseCallback venueResponseCallback, final FaultCallback faultCallback) {

        if(!Utility.isOnline()) {
            faultCallback.onFault("", CandCApplication.getAppContext().getResources().getString(R.string.unable_to_access));
            return;
        }

        Backendless.Persistence.of(Venue.class).findById(objectId, new AsyncCallback<Venue>() {
            @Override
            public void handleResponse(Venue response) {

                if(response != null) {
                    venueResponseCallback.onData(response);
                } else {
                    faultCallback.onFault("", "Failed to find the current Venue.");
                }
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                faultCallback.onFault(fault.getCode(), fault.getMessage());
            }
        });
    }

    public void startGeofenceMonitoring(final Context context) {

        String geofences = Utility.getInstance().getGeofences();

        if(geofences != null) {

            List<String> fences = Arrays.asList(geofences.split("\\s*,\\s*"));

            for(String fence : fences) {
                //Log.e(TAG,"fence = " + fence);
                Cloud.getInstance().startGeofenceMonitoring(context,fence);
            }
        }
    }

    public void stopGeofenceMonitoring() {

        String geofences = Utility.getInstance().getGeofences();

        if(geofences != null) {

            List<String> fences = Arrays.asList(geofences.split("\\s*,\\s*"));

            for(String fence : fences) {
                Cloud.getInstance().stopGeofenceMonitoring(fence);
            }
        }
    }

    public void startGeofenceMonitoring(final Context context, final String geofenceName) {

        if(!Utility.isOnline()) {
            return;
        }

        if(GeoFenceMonitoring.getInstance().containsGeoFence(geofenceName)) {
            Log.d(TAG, "startGeofenceMonitoring already called on: " + geofenceName);
            return;
        }

        if(mGeofenceCallback == null) {

            mGeofenceCallback = new IGeofenceCallback() {

                @Override
                public void geoPointEntered(String geofenceName, String geofenceId, double latitude, double longitude) {

                    Log.d(TAG, "Geo Point Entered:");
                    Log.d(TAG, "    name: " + geofenceName);
                    Log.d(TAG, "    id: " + geofenceId);
                    Log.d(TAG, "    lat: " + String.valueOf(latitude));
                    Log.d(TAG, "    long: " + String.valueOf(longitude));

                    //Toast.makeText(context, "Geo Point Entered: " + geofenceName, Toast.LENGTH_LONG).show();

                    //Utility.getInstance().showOkDialog(context,
                    //        "Geo Point Entered", "name: " + geofenceName + "\nlat: " + String.valueOf(latitude) + "\nlong: " + String.valueOf(longitude));

                    if(!Utility.getInstance().isInsideGeofence(geofenceName)) {

                        //Utility.getInstance().postNotification("Geofence Entered", "name: " + geofenceName + ", lat: " + String.valueOf(latitude) + ", long: " + String.valueOf(longitude), "Entering Geofence: " + geofenceName);

                        Utility.getInstance().setInsideGeofence(geofenceName, true);
                        CloudAnalytics.getInstance().sendEventEnterGeofence(geofenceName);
                    }
                }

                @Override
                public void geoPointStayed(String geofenceName, String geofenceId, double latitude, double longitude) {

                    Log.d(TAG, "Geo Point Stayed:");
                    Log.d(TAG, "    name: " + geofenceName);
                    Log.d(TAG, "    id: " + geofenceId);
                    Log.d(TAG, "    lat: " + String.valueOf(latitude));
                    Log.d(TAG, "    long: " + String.valueOf(longitude));

                    //Toast.makeText(context, "Geo Point Stayed: " + geofenceName, Toast.LENGTH_LONG).show();

                    //Utility.getInstance().showOkDialog(context,
                    //        "Geo Point Stayed", "name: " + geofenceName + "\nlat: " + String.valueOf(latitude) + "\nlong: " + String.valueOf(longitude));
                }

                @Override
                public void geoPointExited(String geofenceName, String geofenceId, double latitude, double longitude) {

                    Log.d(TAG, "Geo Point Exited:");
                    Log.d(TAG, "    name: " + geofenceName);
                    Log.d(TAG, "    id: " + geofenceId);
                    Log.d(TAG, "    lat: " + String.valueOf(latitude));
                    Log.d(TAG, "    long: " + String.valueOf(longitude));

                    //Toast.makeText(context, "Geo Point Exited: " + geofenceName, Toast.LENGTH_LONG).show();

                    //Utility.getInstance().showOkDialog(context,
                    //        "Geo Point Exited", "name: " + geofenceName + "\nlat: " + String.valueOf(latitude) + "\nlong: " + String.valueOf(longitude));


                    if(Utility.getInstance().isInsideGeofence(geofenceName)) {

                        int lengthOfStay = Utility.getInstance().getGeofenceLengthOfStay(geofenceName);

                        //Utility.getInstance().postNotification("Geofence Exited", "name: " + geofenceName + ", lat: " + String.valueOf(latitude) + ", long: " + String.valueOf(longitude), "Exiting Geofence: " + geofenceName);

                        Utility.getInstance().setInsideGeofence(geofenceName, false);

                        CloudAnalytics.getInstance().sendEventExitGeofence(geofenceName, lengthOfStay);
                    }
                }
            };

            mGeofenceCompletionCallback = new AsyncCallback<Void>() {

                @Override
                public void handleResponse(Void response) {
                    Log.d(TAG, "Geofence monitoring has been started!");
                }

                @Override
                public void handleFault(BackendlessFault fault) {
                    Log.e(TAG, "Geofence Error - " + fault.getMessage());
                }
            };
        }

        Log.d(TAG, "startGeofenceMonitoring called on: " + geofenceName);

        Backendless.Geo.startGeofenceMonitoring(geofenceName, mGeofenceCallback, mGeofenceCompletionCallback);
    }

    public void stopGeofenceMonitoring(String geofenceName) {

        if(!Utility.isOnline()) {
            return;
        }

        Log.d(TAG, "stopGeofenceMonitoring called on: " + geofenceName);

        if(Utility.getInstance().isInsideGeofence(geofenceName)) {
            int lengthOfStay = Utility.getInstance().getGeofenceLengthOfStay(geofenceName);
            Utility.getInstance().setInsideGeofence(geofenceName, false);
            CloudAnalytics.getInstance().sendEventExitGeofence(geofenceName, lengthOfStay);
        }

        Backendless.Geo.stopGeofenceMonitoring(geofenceName);

        if(!GeoFenceMonitoring.getInstance().isMonitoring()) {

            Log.d(TAG, "Destroying Geofence Callbacks!");
            mGeofenceCallback = null;
            mGeofenceCompletionCallback = null;
        }
    }

    public void loadFirebaseChatSession(String chatSessionId, final ChatSessionsDataCallback chatSessionsDataCallback, final DatabaseErrorCallback databaseErrorCallback) {

        Log.d(TAG, "loadFirebaseChatSession called!");

        if(!Utility.isOnline()) {
            databaseErrorCallback.onDatabaseError(0, CandCApplication.getAppContext().getResources().getString(R.string.unable_to_access));
            return;
        }

        Query queryChatSessions = mFBDatabase.child(CHAT_SESSIONS_KEY).child(chatSessionId);

        queryChatSessions.addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        ArrayList<FBChat> fbChats = new ArrayList<>();

                        for(DataSnapshot snapshot: dataSnapshot.getChildren()) {

                            FBChat fbChat = FBChat.createFromSnapshot(snapshot);
                            fbChats.add(fbChat);
                        }

                        Collections.sort(fbChats, new Comparator<FBChat>() {
                            public int compare(FBChat fbcs1, FBChat fbcs2) {
                                return (int)(fbcs1.timeStamp - fbcs2.timeStamp);
                            }
                        });

                        chatSessionsDataCallback.onData(fbChats);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.e(TAG, "onCancelled: ", databaseError.toException());
                        databaseErrorCallback.onDatabaseError(databaseError.getCode(), databaseError.getMessage());
                    }
                });
    }

    public void listenToFirebaseChatSession(String chatSessionId, final ChatSessionDataCallback chatSessionDataCallback, final DatabaseErrorCallback databaseErrorCallback) {

        Log.d(TAG, "listenToFirebaseChatSession called!");

        if(!Utility.isOnline()) {
            databaseErrorCallback.onDatabaseError(0, CandCApplication.getAppContext().getResources().getString(R.string.unable_to_access));
            return;
        }

        Query queryChatSessions = mFBDatabase.child(CHAT_SESSIONS_KEY).child(chatSessionId);

        queryChatSessions.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                FBChat fbChat = FBChat.createFromSnapshot(dataSnapshot);
                chatSessionDataCallback.onData(fbChat);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {}

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {}

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {}

            @Override
            public void onCancelled(DatabaseError databaseError) {
                databaseErrorCallback.onDatabaseError(databaseError.getCode(), databaseError.getMessage());
            }
        });
    }

    public void setTypingIndicator(boolean isTyping) {

        if(mFBUserIsTypingRef != null) {
            mFBUserIsTypingRef.setValue(isTyping);
        }
    }

    public void listenToTypingIndicator(ChatSession chatSession, final OnTypingCallback onTypingCallback) {

        Log.d(TAG, "listenToFirebaseChatSession called!");

        DatabaseReference typingIndicatorRef = mFBDatabase.child(TYPING_INDICATOR_KEY).child(chatSession.getObjectId());
        mFBUserIsTypingRef = typingIndicatorRef.child(getUserObjectId());
        mFBUserIsTypingRef.onDisconnect().removeValue();

        Query usersTypingQuery = typingIndicatorRef.orderByValue().equalTo(true);

        usersTypingQuery.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                onTypingCallback.onTyping(true);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                onTypingCallback.onTyping(true);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                onTypingCallback.onTyping(false);
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                onTypingCallback.onTyping(false);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                onTypingCallback.onTyping(false);
            }
        });
    }

// Not needed when we combine Glide with Firebase!
//    public void loadFirebaseImage(final Context context, final ImageView imageView, String gsUrl, final ResponseCallback responseCallback, final StorageErrorCallback storageErrorCallback) {
//
//        StorageReference gsReference = mFBStorage.getReferenceFromUrl(gsUrl);
//
//        Glide.with(context)
//                .using(new FirebaseImageLoader())
//                .load(gsReference)
//                .fitCenter()
//                .listener(new RequestListener<StorageReference, GlideDrawable>() {
//                    @Override
//                    public boolean onException(Exception e, StorageReference model, Target<GlideDrawable> target, boolean isFirstResource) {
//
//                        storageErrorCallback.onStorageErrorCallback(e.getMessage());
//                        return false;
//                    }
//
//                    @Override
//                    public boolean onResourceReady(GlideDrawable resource, StorageReference model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//
//                        responseCallback.onResponse();
//                        return false;
//                    }
//                })
//                .into(imageView);
//    }

    public void createFirbaseImage(final Context context, Bitmap pictureBitmap, final StorageUploadCallback storageUploadCallback, final StorageErrorCallback storageErrorCallback) {

        if(!Utility.isOnline()) {
            storageErrorCallback.onStorageErrorCallback(CandCApplication.getAppContext().getResources().getString(R.string.unable_to_access));
            return;
        }

        StorageReference testRef = mFBStorageRef.child(getUserObjectId() + "/" + UUID.randomUUID().toString().toUpperCase() + ".jpg");

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        pictureBitmap.compress(Bitmap.CompressFormat.JPEG, 80, baos);
        byte[] bitmapData = baos.toByteArray();

        UploadTask uploadTask = testRef.putBytes(bitmapData);

        uploadTask.addOnFailureListener(new OnFailureListener() {

            @Override
            public void onFailure(@NonNull Exception exception) {
                storageErrorCallback.onStorageErrorCallback(exception.getMessage());
            }

        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {

            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Uri downloadUrl = taskSnapshot.getDownloadUrl();
                storageUploadCallback.onUpload(downloadUrl);
            }
        });
    }

    public void sendFirebaseChatMessage(String chatSessionId, FBChat fbChat, final ResponseCallback responseCallback,final FaultCallback faultCallback) {

        Log.d(TAG, "sendChatMessage called!");

        Map<String, Object> postValues = fbChat.toMap();

        String key = mFBDatabase.child(CHAT_SESSIONS_KEY).child(chatSessionId).push().getKey();

        mFBDatabase.child(CHAT_SESSIONS_KEY).child(chatSessionId).child(key).setValue(postValues, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                if(databaseError != null) {
                    System.out.println("Data could not be saved " + databaseError.getMessage());
                    faultCallback.onFault(String.valueOf(databaseError.getCode()), databaseError.getMessage());
                    return;
                }

                responseCallback.onResponse();
            }
        });
    }

    public void getLatLngFromAddressUsingGoogle(final String address, final LatLngCallback latLngCallback, final FaultCallback faultCallback) {

        String addressTemp = address.replace(" ", "+");

        //https://maps.googleapis.com/maps/api/geocode/xml?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&key=YOUR_API_KEY

        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                GOOGLE_MAPS_GEOCODING_URL + "?address=" + addressTemp +"&key=" + GOOGLE_MAPS_GEOCODING_KEY, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                boolean foundLatLng = false;
                double lat = 0.0;
                double lng = 0.0;

                JsonParser parser = new JsonParser();
                JsonObject rootObject = parser.parse(response).getAsJsonObject();

                if(rootObject != null) {
                    JsonArray results = rootObject.get("results").getAsJsonArray();
                    if(results != null && results.size() > 0) {
                        JsonObject results0 = results.get(0).getAsJsonObject();
                        if(results0 != null) {
                            JsonObject geometry = results0.get("geometry").getAsJsonObject();
                            if(geometry != null) {
                                JsonObject location = geometry.get("location").getAsJsonObject();
                                if(location != null) {
                                    lat = location.get("lat").getAsDouble();
                                    lng = location.get("lng").getAsDouble();
                                    foundLatLng = true;
                                }
                            }
                        }
                    }
                }

                //Log.e(TAG, "lat = " + String.valueOf(lat));
                //Log.e(TAG, "lng = " + String.valueOf(lng));

                if(foundLatLng) {
                    latLngCallback.onData(new LatLng(lat, lng));
                } else {
                    Log.e(TAG, "getLatLngFromAddressUsingGoogle failed to find any matches for address: " + address);
                    faultCallback.onFault("","");
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "error = " + error.toString());
                faultCallback.onFault("","");
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(CandCApplication.getAppContext());
        requestQueue.add(stringRequest);
    }
}
