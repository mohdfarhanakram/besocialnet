package com.joinbesocial.candc.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.joinbesocial.candc.CloudAnalytics;
import com.joinbesocial.candc.NavigationHostActivity;
import com.joinbesocial.candc.R;
import com.joinbesocial.candc.fragments.VenueItemFragment;
import com.joinbesocial.candc.models.VenueItem;

import java.util.ArrayList;
import java.util.List;

public class VenueItemsAdapter extends RecyclerView.Adapter<VenueItemsAdapter.ViewHolder> {

    private final static String TAG = VenueItemsAdapter.class.getSimpleName();

    private NavigationHostActivity mNavigationHostActivity;
    private ArrayList<VenueItem> mDataset = new ArrayList<>();

    public VenueItemsAdapter(NavigationHostActivity navigationHostActivity) {

        mNavigationHostActivity = navigationHostActivity;
    }
    // Create new views (invoked by the layout manager)
    @Override
    public VenueItemsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        // Create a new view based on our layout for each item in the list.
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_venue_items, parent, false);

        return new ViewHolder(itemView);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        // Use the element at this position from our dataset to replace the
        // contents of the view.
        holder.mTitleTextView.setText(mDataset.get(position).getTitle());
        holder.mSubTitleTextView.setText(mDataset.get(position).getSubTitle());
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    // Clean all elements of the recycler
    public void clear() {
        mDataset.clear();
        notifyDataSetChanged();
    }

    // Add a list of items
    public void addAll(List<VenueItem> list) {

        mDataset.addAll(list);
        notifyDataSetChanged();
    }

    // Add a single item.
    public void add(VenueItem venueItem) {

        mDataset.add(venueItem);
        notifyItemInserted(mDataset.size() - 1);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView mTitleTextView;
        public TextView mSubTitleTextView;

        public ViewHolder(View view) {
            super(view);

            view.setOnClickListener(this);

            mTitleTextView = (TextView) view.findViewById(R.id.tv_venueitem_item_title);
            mSubTitleTextView = (TextView) view.findViewById(R.id.tv_venitem_item_subtitle);
        }

        @Override
        public void onClick(View v) {

            int position = getAdapterPosition();

            if(position != RecyclerView.NO_POSITION) { // Check if an item was deleted, but the user clicked it before the UI removed it

                //Log.d(TAG, String.valueOf(position) + " = " + VendorsAdapter.this.mDataset.get(position).getTitle());

                CloudAnalytics.getInstance().sendEventSelectedVenueItem(VenueItemsAdapter.this.mDataset.get(position).getTitle(),
                        VenueItemsAdapter.this.mDataset.get(position).getVenueItemType());

                VenueItemFragment fragment = new VenueItemFragment();
                fragment.setVenueItem(VenueItemsAdapter.this.mDataset.get(position));

                if(mNavigationHostActivity != null) {
                    mNavigationHostActivity.pushFragment(fragment);
                }
            }
        }
    }
}
