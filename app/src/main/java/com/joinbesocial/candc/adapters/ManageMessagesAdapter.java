package com.joinbesocial.candc.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.joinbesocial.candc.NavigationHostActivity;
import com.joinbesocial.candc.R;
import com.joinbesocial.candc.fragments.ChatFragment;
import com.joinbesocial.candc.models.ChatSession;
import com.joinbesocial.candc.models.ScheduledMessage;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

public class ManageMessagesAdapter extends RecyclerView.Adapter<ManageMessagesAdapter.ViewHolder> {

    private final static String TAG = ManageMessagesAdapter.class.getSimpleName();

    private ArrayList<ScheduledMessage> mDataset;
    private Context mContext;
    SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat("h:mm aa z 'on' MMM d, yyyy");

    // Provide a suitable constructor (depends on the kind of dataset)
    public ManageMessagesAdapter(Context context, ArrayList<ScheduledMessage> myDataset) {

        mContext = context;
        mDataset = myDataset;
    }

    // Clean all elements of the recycler
    public void clear() {
        mDataset.clear();
        notifyDataSetChanged();
    }

    // Add a list of items
    public void addAll(List<ScheduledMessage> list) {

        mDataset.addAll(list);
        notifyDataSetChanged();
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ManageMessagesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        // Create a new view based on our layout for each item in the list.
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_manage_messages, parent, false);

        return new ViewHolder(itemView);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        // Use the element at this position from our dataset to replace the
        // contents of the view.

        holder.itemView.setAlpha(1.0f);

        if(mDataset.get(position).getDeliveryDate() != null) {
            String formatedDate = mSimpleDateFormat.format(mDataset.get(position).getDeliveryDate());
            holder.mDateTextView.setText(formatedDate);
        }

        if(mDataset.get(position).getMessage() != null) {
            holder.mMessagesTextView.setText(mDataset.get(position).getMessage().toString());
        }

        if(mDataset.get(position).getVenueItem() != null) {
            holder.mVenueItemTextView.setText(mDataset.get(position).getVenueItem().toString());
        } else {
            holder.mVenueItemTextView.setText("None Specified");
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView mDateTextView;
        public TextView mMessagesTextView;
        public TextView mVenueItemTextView;

        public ViewHolder(View view) {
            super(view);

            mDateTextView = (TextView) view.findViewById(R.id.tv_mng_messages_date);
            mMessagesTextView = (TextView) view.findViewById(R.id.tv_mng_messages_msg);
            mVenueItemTextView = (TextView) view.findViewById(R.id.tv_mng_messages_venueitem);
        }
    }
}
