package com.joinbesocial.candc.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.backendless.BackendlessUser;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.joinbesocial.candc.NavigationHostActivity;
import com.joinbesocial.candc.R;
import com.joinbesocial.candc.Utility;
import com.joinbesocial.candc.fragments.ChatFragment;

import java.util.ArrayList;
import java.util.List;

public class AttendeesAdapter extends RecyclerView.Adapter<AttendeesAdapter.ViewHolder> {

    private final static String TAG = AttendeesAdapter.class.getSimpleName();

    private NavigationHostActivity mNavigationHostActivity;
    private ArrayList<BackendlessUser> mDataset = new ArrayList<>();
    private Context mContext;

    public AttendeesAdapter(Context context, NavigationHostActivity navigationHostActivity) {

        mNavigationHostActivity = navigationHostActivity;
        mContext = context;
    }

    // Clean all elements of the recycler.
    public void clear() {
        mDataset.clear();
        notifyDataSetChanged();
    }

    // Add a list of items.
    public void addAll(List<BackendlessUser> list) {

        mDataset.addAll(list);
        notifyDataSetChanged();
    }

    // Add a single item.
    public void add(BackendlessUser user) {

        mDataset.add(user);
        notifyItemInserted(mDataset.size() - 1);
    }

    // Create new views (invoked by the layout manager)
    @Override
    public AttendeesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        // Create a new view based on our layout for each item in the list.
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_attendees, parent, false);

        return new ViewHolder(itemView);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        // Use the element at this position from our dataset to replace the
        // contents of the view.

        if(mDataset.get(position).getProperty("name") != null) {
            holder.mNameTextView.setText(mDataset.get(position).getProperty("name").toString());
        } else {
            holder.mNameTextView.setText("");
        }

        Object workTitle = mDataset.get(position).getProperty("workTitle");
        if(workTitle != null && !workTitle.toString().isEmpty()) {
            holder.mTitleTextView.setText(workTitle.toString());
        } else {
            holder.mTitleTextView.setText("Visitor");
        }

        Object profileImageThumbnailUrl = mDataset.get(position).getProperty("profileImageThumbnailUrl");

        if(profileImageThumbnailUrl != null && !profileImageThumbnailUrl.toString().isEmpty()) {

            String thumbnailUrl = mDataset.get(position).getProperty("profileImageThumbnailUrl").toString();

            Glide.with(mContext)
                    .load(thumbnailUrl)
                    .asBitmap()
                    .placeholder(Utility.getInstance().getPlaceHolderThumbnailImage())
                    .centerCrop()
                    .into(new BitmapImageViewTarget(holder.mProfileThumbnailImage) {
                @Override
                protected void setResource(Bitmap resource) {
                    Drawable drawable = Utility.getInstance().createRoundDrawableFromBitmap(resource);
                    holder.mProfileThumbnailImage.setImageDrawable(drawable);
                }
            });
        } else {
            holder.mProfileThumbnailImage.setImageDrawable(Utility.getInstance().getPlaceHolderThumbnailImage());
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView mProfileThumbnailImage;
        public TextView mNameTextView;
        public TextView mTitleTextView;

        public ViewHolder(View view) {
            super(view);

            view.setOnClickListener(this);

            mProfileThumbnailImage = (ImageView) view.findViewById(R.id.iv_attendees_profile_placeholder_thumbnail);
            mProfileThumbnailImage.setImageDrawable(Utility.getInstance().getPlaceHolderThumbnailImage());

            mNameTextView = (TextView) view.findViewById(R.id.tv_attendees_item_name);
            mTitleTextView = (TextView) view.findViewById(R.id.tv_attendees_item_title);
        }

        @Override
        public void onClick(View v) {

            final int position = getAdapterPosition();

            if(position != RecyclerView.NO_POSITION) { // Check if an item was deleted, but the user clicked it before the UI removed it

                ChatFragment fragment = new ChatFragment();
                fragment.mBackendlessUser = AttendeesAdapter.this.mDataset.get(position);

                //mNavigationHostActivity.pushFragment(fragment);
                // Intentional Replace vs Push so we jump back to Messages after leaving a chat session.

                mNavigationHostActivity.replaceFragment(fragment);
            }
        }
    }
}
