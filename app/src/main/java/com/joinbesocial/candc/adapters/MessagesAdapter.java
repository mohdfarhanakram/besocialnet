package com.joinbesocial.candc.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.joinbesocial.candc.Cloud;
import com.joinbesocial.candc.NavigationHostActivity;
import com.joinbesocial.candc.R;
import com.joinbesocial.candc.Utility;
import com.joinbesocial.candc.fragments.ChatFragment;
import com.joinbesocial.candc.models.ChatSession;

import java.util.ArrayList;
import java.util.List;

public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.ViewHolder> {

    private final static String TAG = MessagesAdapter.class.getSimpleName();

    private NavigationHostActivity mNavigationHostActivity;
    private ArrayList<ChatSession> mDataset;
    private Context mContext;

    // Provide a suitable constructor (depends on the kind of dataset)
    public MessagesAdapter(Context context, ArrayList<ChatSession> myDataset, NavigationHostActivity navigationHostActivity) {

        mContext = context;
        mDataset = myDataset;
        mNavigationHostActivity = navigationHostActivity;
    }

    // Clean all elements of the recycler
    public void clear() {
        mDataset.clear();
        notifyDataSetChanged();
    }

    // Add a list of items
    public void addAll(List<ChatSession> list) {

        mDataset.addAll(list);
        notifyDataSetChanged();
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MessagesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        // Create a new view based on our layout for each item in the list.
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_messages, parent, false);

        return new ViewHolder(itemView);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        // Use the element at this position from our dataset to replace the
        // contents of the view.

        String profileImageThumbnailUrl = "";

        if(mDataset.get(position).getOwnerId().equals(Cloud.getInstance().getUserObjectId())) {
            holder.mNameTextView.setText(mDataset.get(position).getTargetName());

            String workTitle = mDataset.get(position).getTargetTitle();
            if(workTitle != null && !workTitle.isEmpty()) {
                holder.mTitleTextView.setText(workTitle);
            } else {
                holder.mTitleTextView.setText("Visitor");
            }

            profileImageThumbnailUrl = mDataset.get(position).getTargetThumbnailUrl();
        } else {
            holder.mNameTextView.setText(mDataset.get(position).getOwnerName());

            String workTitle = mDataset.get(position).getOwnerTitle();
            if(workTitle != null && !workTitle.isEmpty()) {
                holder.mTitleTextView.setText(workTitle);
            } else {
                holder.mTitleTextView.setText("Visitor");
            }

            profileImageThumbnailUrl = mDataset.get(position).getOwnerThumbnailUrl();
        }

        if(!profileImageThumbnailUrl.toString().isEmpty()) {

            Glide.with(mContext)
                    .load(profileImageThumbnailUrl)
                    .asBitmap()
                    .placeholder(Utility.getInstance().getPlaceHolderThumbnailImage())
                    .centerCrop()
                    .into(new BitmapImageViewTarget(holder.mProfileThumbnailImage) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            Drawable drawable = Utility.getInstance().createRoundDrawableFromBitmap(resource);
                            holder.mProfileThumbnailImage.setImageDrawable(drawable);
                        }
                    });
        } else {
            holder.mProfileThumbnailImage.setImageDrawable(Utility.getInstance().getPlaceHolderThumbnailImage());
        }

//        holder.mProfileThumbnailImage.setImageDrawable(Utility.getInstance().getPlaceHolderThumbnailImage());
//
//        Glide.with(mContext).load(thumbnailUrl).asBitmap().centerCrop().into(new BitmapImageViewTarget(holder.mProfileThumbnailImage) {
//            @Override
//            protected void setResource(Bitmap resource) {
//                Drawable drawable = Utility.getInstance().createRoundDrawableFromBitmap(resource);
//                holder.mProfileThumbnailImage.setImageDrawable(drawable);
//            }
//        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView mProfileThumbnailImage;
        public TextView mNameTextView;
        public TextView mTitleTextView;

        public ViewHolder(View view) {
            super(view);

            view.setOnClickListener(this);

            mProfileThumbnailImage = (ImageView) view.findViewById(R.id.iv_messages_profile_placeholder_thumbnail);
            mNameTextView = (TextView) view.findViewById(R.id.tv_mng_messages_date);
            mTitleTextView = (TextView) view.findViewById(R.id.tv_messages_item_title);
        }

        @Override
        public void onClick(View v) {

            int position = getAdapterPosition();

            if(position != RecyclerView.NO_POSITION) { // Check if an item was deleted, but the user clicked it before the UI removed it

                ChatFragment fragment = new ChatFragment();  //.newInstance(0);
                fragment.setChatSession(MessagesAdapter.this.mDataset.get(position));

                if(mNavigationHostActivity != null) {
                    mNavigationHostActivity.pushFragment(fragment);
                }
            }
        }
    }
}
