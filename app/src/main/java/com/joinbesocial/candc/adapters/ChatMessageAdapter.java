package com.joinbesocial.candc.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.joinbesocial.candc.R;
import com.joinbesocial.candc.Utility;
import com.joinbesocial.candc.fragments.ChatFragment;
import com.joinbesocial.candc.models.ChatMessage;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ChatMessageAdapter extends RecyclerView.Adapter<ChatMessageAdapter.MessageHolder> {

    private final static String TAG = ChatMessageAdapter.class.getSimpleName();

    private static final int MY_MESSAGE = 0, OTHER_MESSAGE = 1;

    private List<ChatMessage> mMessages;
    private Context mContext;
    private ChatFragment mChatFragment;

    public ChatMessageAdapter(Context context, ChatFragment chatFragment, List<ChatMessage> data) {

        mContext = context;
        mChatFragment = chatFragment;
        mMessages = data;
    }

    // Clean all elements of the recycler
    public void clear() {
        mMessages.clear();
        notifyDataSetChanged();
    }

    // Add a list of items
    public void addAll(List<ChatMessage> list) {

        mMessages.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mMessages == null ? 0 : mMessages.size();
    }

    @Override
    public int getItemViewType(int position) {

        ChatMessage item = mMessages.get(position);

        if(item.isMine()) {
            return MY_MESSAGE;
        } else {
            return OTHER_MESSAGE;
        }
    }

    @Override
    public MessageHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == MY_MESSAGE) {
            return new MessageHolder(LayoutInflater.from(mContext).inflate(R.layout.item_mine_message, parent, false));
        } else {
            return new MessageHolder(LayoutInflater.from(mContext).inflate(R.layout.item_other_message, parent, false));
        }
    }

    public void add(ChatMessage message) {

        mMessages.add(message);
        notifyItemInserted(mMessages.size() - 1);
    }

    public void remove(ChatMessage message) {

        int index = mMessages.indexOf(message);
        mMessages.remove(message);
        notifyItemRemoved(index);
    }

    @Override
    public void onBindViewHolder(final MessageHolder holder, int position) {

        final ChatMessage chatMessage = mMessages.get(position);

        holder.ivImage.setImageDrawable(null);
        holder.tvMessage.setText("");

        holder.ivImage.setVisibility(View.GONE);
        holder.tvMessage.setVisibility(View.GONE);
        holder.mProgessBar.setVisibility(View.GONE);

        if(chatMessage.isImage()) {

            holder.ivImage.setVisibility(View.VISIBLE);
            holder.mProgessBar.setVisibility(View.VISIBLE);

Log.e("XXX", "" + String.valueOf(chatMessage.getContent()));

            Glide.with(mContext)
                    .load(chatMessage.getContent())
                    .fitCenter()
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            Log.e(TAG, e.getLocalizedMessage());
                            holder.mProgessBar.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            Utility.getInstance().hideLoadSpinner();
                            holder.mProgessBar.setVisibility(View.GONE);

//Log.e("XXX", "isFromMemoryCache = " + String.valueOf(isFromMemoryCache));
//                            // If the image has loaded - tell the chat to scroll to bottom.
//                            if(mChatFragment != null) {
//Log.e("XXX", "forceScrollToBottom");
//                                mChatFragment.forceScrollToBottom();
//                            }

                            holder.ivImage.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    // If the image is tapped, have the ChatFragment handle the
                                    // launching of the ImageViewerActivity so it can manage it.
                                    if(mChatFragment != null) {
                                        mChatFragment.launchImageViewer(chatMessage.getContent());
                                    }
                                }
                            });

                            return false;
                        }
                    })
                    .into(holder.ivImage);

        } else {

            holder.tvMessage.setVisibility(View.VISIBLE);

            holder.tvMessage.setText(chatMessage.getContent());
        }

        String date = new SimpleDateFormat("hh:mm aa", Locale.getDefault()).format(new Date());
        holder.tvTime.setText(date);
    }

    class MessageHolder extends RecyclerView.ViewHolder {

        TextView tvMessage, tvTime;
        ImageView ivImage;
        ProgressBar mProgessBar;

        MessageHolder(View itemView) {
            super(itemView);
            tvMessage = (TextView) itemView.findViewById(R.id.tv_message);
            tvTime = (TextView) itemView.findViewById(R.id.tv_time);
            ivImage = (ImageView) itemView.findViewById(R.id.iv_image);
            mProgessBar = (ProgressBar) itemView.findViewById(R.id.pb_chat);
        }
    }
}