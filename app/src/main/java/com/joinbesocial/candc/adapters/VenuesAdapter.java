package com.joinbesocial.candc.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.joinbesocial.candc.Cloud;
import com.joinbesocial.candc.CloudAnalytics;
import com.joinbesocial.candc.NavigationHostActivity;
import com.joinbesocial.candc.R;
import com.joinbesocial.candc.Utility;
import com.joinbesocial.candc.fragments.CurrentVenueFragment;
import com.joinbesocial.candc.models.Venue;

import java.util.ArrayList;
import java.util.List;

public class VenuesAdapter extends RecyclerView.Adapter<VenuesAdapter.ViewHolder> {

    private final static String TAG = VenuesAdapter.class.getSimpleName();

    private NavigationHostActivity mNavigationHostActivity;
    private ArrayList<Venue> mDataset;
    private Context mContext;

    public VenuesAdapter(ArrayList<Venue> myDataset, NavigationHostActivity navigationHostActivity, Context context) {
        mDataset = myDataset;
        mNavigationHostActivity = navigationHostActivity;
        mContext = context;
    }

    // Clean all elements of the recycler
    public void clear() {
        mDataset.clear();
        notifyDataSetChanged();
    }

    // Add a list of items
    public void addAll(List<Venue> list) {

        mDataset.addAll(list);
        notifyDataSetChanged();
    }

    // Create new views (invoked by the layout manager)
    @Override
    public VenuesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        // Create a new view based on our layout for each item in the list.
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_venues, parent, false);

        return new ViewHolder(itemView);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        // Use the element at this position from our dataset to replace the
        // contents of the view.
        holder.mTitleTextView.setText(mDataset.get(position).getTitle());
        holder.mSubTitleTextView.setText(mDataset.get(position).getSubTitle());
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView mTitleTextView;
        public TextView mSubTitleTextView;

        public ViewHolder(View view) {
            super(view);

            view.setOnClickListener(this);

            mTitleTextView = (TextView) view.findViewById(R.id.tv_venue_items_title);
            mSubTitleTextView = (TextView) view.findViewById(R.id.tv_venue_items_subtitle);
        }

        @Override
        public void onClick(View v) {

            final int position = getAdapterPosition();

            if(position != RecyclerView.NO_POSITION) { // Check if an item was deleted, but the user clicked it before the UI removed it

                //Log.d(TAG, String.valueOf(position) + " = " + EventsAdapter.this.mDataset.get(position).getTitle());

//                Utility.getInstance().showOkCancelDialog(context, "Enter Venue",
//                        "Would you like to set this as your event?",
//                        new Utility.OkCallback() {
//                            @Override
//                            public void onOk() {
//
                Log.d(TAG, String.valueOf(position) + ": venueCode = " + VenuesAdapter.this.mDataset.get(position).getVenueCode());

                Utility.getInstance().showLoadSpinner(mContext, "Entering Venue");

                Cloud.getInstance().addVenueCodeToUser(VenuesAdapter.this.mDataset.get(position).getVenueCode(), new Cloud.ResponseCallback() {
                    @Override
                    public void onResponse() {

                        Utility.getInstance().setVenueObjectId(VenuesAdapter.this.mDataset.get(position).getObjectId());
                        Utility.getInstance().setVenueCode(VenuesAdapter.this.mDataset.get(position).getVenueCode());
                        Utility.getInstance().setVenueTitle(VenuesAdapter.this.mDataset.get(position).getTitle());

                        if(VenuesAdapter.this.mDataset.get(position).getGeofences() != null) {
                            Utility.getInstance().setGeofences(VenuesAdapter.this.mDataset.get(position).getGeofences());
                        }

                        CloudAnalytics.getInstance().sendEventSelectedEvent("search", VenuesAdapter.this.mDataset.get(position).getVenueCode());

                        if(mNavigationHostActivity != null) {

                            CurrentVenueFragment fragment = new CurrentVenueFragment();
                            Utility.getInstance().hideLoadSpinner();
                            mNavigationHostActivity.popAndReplaceFragment(fragment);
                        }
                    }
                }, new Cloud.FaultCallback() {
                    @Override
                    public void onFault(String code, String message) {

                        Utility.getInstance().hideLoadSpinner();
                        Utility.getInstance().showErrorDialog(VenuesAdapter.this.mContext, "Venue Error", message);
                    }
                });
//                            }
//                        });
            }
        }
    }
}
