package com.joinbesocial.candc;

public class VenueItemMessageData {

    public String message;
    public boolean viewed = false;

    VenueItemMessageData(String message) {
        this.message = message;
    }
}
