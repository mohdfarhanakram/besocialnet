package com.joinbesocial.candc.activities;

import android.Manifest;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.GlideBitmapDrawable;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.joinbesocial.candc.CapturePhotoUtils;
import com.joinbesocial.candc.R;
import com.joinbesocial.candc.Utility;

import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import uk.co.senab.photoview.PhotoView;

public class ImageViewerActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks {

    private final static String TAG = ImageViewerActivity.class.getSimpleName();

    private static final int RC_WRITE_EXTERNAL = 125;

    private String mImageUrl;
    private PhotoView mPhotoView;
    private TextView mSaveText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_image_viewer);

        mPhotoView = (PhotoView) findViewById(R.id.pv_image_viewer);
        mSaveText = (TextView) findViewById(R.id.tv_image_viewer_save);

        mSaveText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d(TAG, "onClick: Save Image");

                if(EasyPermissions.hasPermissions(ImageViewerActivity.this, new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE})) {
                    writeImageToExternalStorage();
                } else {
                    promptForPermissions();
                }
            }
        });

        Bundle extras = getIntent().getExtras();
        mImageUrl = extras.getString("imageUrl");

        if(mImageUrl != null) {

            Utility.getInstance().showLoadSpinner(this, "Loading Image");

            Glide.with(this)
                    .load(mImageUrl)
                    .fitCenter()
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            Utility.getInstance().hideLoadSpinner();
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            Utility.getInstance().hideLoadSpinner();
                            return false;
                        }
                    })
                    .into(mPhotoView);
        }
    }

    private void writeImageToExternalStorage() {

        Log.d(TAG, "writeImageToExternalStorage");

        Utility.getInstance().showLoadSpinner(this, "Saving Image");

        Bitmap bitmap = ((GlideBitmapDrawable) mPhotoView.getDrawable().getCurrent()).getBitmap();

        if(bitmap != null) {
            //MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, "BeSocial Image", "Image saved from a BeSocial venue.");
            CapturePhotoUtils.insertImage(getContentResolver(), bitmap, "BeSocial Image", "Image saved from a BeSocial venue.");
        }

        Utility.getInstance().hideLoadSpinner(1000);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @AfterPermissionGranted(RC_WRITE_EXTERNAL)
    private void promptForPermissions() {

        Log.d(TAG, "promptForPermissions");

        String[] perms = { Manifest.permission.WRITE_EXTERNAL_STORAGE};

        if(EasyPermissions.hasPermissions(this, perms)) {

            Log.d(TAG, "promptForPermissions: has permission.");

            // We already have the permission, so just do what we want!
            writeImageToExternalStorage();

        } else {

            Log.d(TAG, "promptForPermissions: Does not have permission.");

            // We don't have the permission, so ask for it.
            EasyPermissions.requestPermissions(this, getString(R.string.rationale_write_extenal), RC_WRITE_EXTERNAL, perms);
        }
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

        Log.d(TAG, "onPermissionsGranted:" + requestCode + ":" + perms.size());
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

        Log.d(TAG, "onPermissionsDenied:" + requestCode + ":" + perms.size());

        Utility.getInstance().showOkCancelDialog(this, "Enable Storage Permission",
            "Android requires that the ability to write to the phone's external storage be enabled manually from the App's info panel.",
            new Utility.OkCallback() {
                @Override
                public void onOk() {
                    Utility.getInstance().startInstalledAppDetailsActivity();
                }
            }, new Utility.CancelCallback() {
                @Override
                public void onCancel() {
                }
            });
    }
}
