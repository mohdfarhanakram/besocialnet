package com.joinbesocial.candc.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.joinbesocial.candc.Cloud;
import com.joinbesocial.candc.R;
import com.joinbesocial.candc.Utility;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SignupActivity extends AppCompatActivity {

    private final static String TAG = Cloud.class.getSimpleName();

    private EditText mNameEdit;
    private EditText mEmailEdit;
    private EditText mPwdEdit;
    private EditText mConfirmPwdEdit;
    private Button mSignUpButton;

    private final TextWatcher mTextWatcher = new TextWatcher() {

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
        public void onTextChanged(CharSequence s, int start, int before, int count) {}

        public void afterTextChanged(Editable s) {

            if(mNameEdit.getText().toString().isEmpty() ||
                    mEmailEdit.getText().toString().isEmpty() ||
                    mPwdEdit.getText().toString().isEmpty() ||
                    mConfirmPwdEdit.getText().toString().isEmpty()) {

                mSignUpButton.setEnabled(false);
                mSignUpButton.getBackground().setColorFilter(0xAAFFFFFF, PorterDuff.Mode.MULTIPLY);

            } else {

                mSignUpButton.setEnabled(true);
                mSignUpButton.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
            }
        }
    };

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        // Hide Status Bar.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_signup);

        mNameEdit = (EditText) findViewById(R.id.et_signup_name);
        mEmailEdit = (EditText) findViewById(R.id.et_signup_email);
        mPwdEdit = (EditText) findViewById(R.id.et_signup_pwd);
        mConfirmPwdEdit = (EditText) findViewById(R.id.et_signup_retype_pwd);

        TextView eula = (TextView) findViewById(R.id.tv_signup_terms);
        eula.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent nextScreen = new Intent(getApplicationContext(), EulaActivity.class);
                startActivity( nextScreen );
            }
        });

//        mNameEdit.addTextChangedListener(mTextWatcher);
//        mEmailEdit.addTextChangedListener(mTextWatcher);
//        mPwdEdit.addTextChangedListener(mTextWatcher);
//        mConfirmPwdEdit.addTextChangedListener(mTextWatcher);
//
//        mSignUpButton = (Button) findViewById(R.id.btn_signup);
//        mSignUpButton.setEnabled(false);
//        mSignUpButton.getBackground().setColorFilter(0xCCFFFFFF, PorterDuff.Mode.MULTIPLY);
    }

    public void onExitBtn(View v) {
        onBackPressed();
    }

    public void onSignUpBtn(View v) {

        if(mNameEdit.getText().toString().matches("")) {
            Utility.getInstance().showOkDialog(this, "Sign Up Error", "Please enter your name.");
            return;
        }

        if(!Utility.getInstance().isValidEmail(mEmailEdit.getText())) {
            Utility.getInstance().showOkDialog(this, "Sign Up Error", "Please enter a valid email address.");
            return;
        }

        if(!mPwdEdit.getText().toString().equals(mConfirmPwdEdit.getText().toString())) {
            Utility.getInstance().showOkDialog(this, "Sign Up Error", "Password confirmation failed. Plase enter your password try again.");
            return;
        }

        Utility.getInstance().showLoadSpinner(this, "Signing Up");

        Cloud.getInstance().registerUser(mNameEdit.getText().toString(), mEmailEdit.getText().toString(), mPwdEdit.getText().toString(),

                new Cloud.ResponseCallback() {
                    @Override
                    public void onResponse() {

                        Utility.getInstance().hideLoadSpinner();

                        Cloud.getInstance().signinUser(mEmailEdit.getText().toString(), mPwdEdit.getText().toString(),

                                new Cloud.ResponseCallback() {
                                    @Override
                                    public void onResponse() {
                                        Intent nextScreen = new Intent( getApplicationContext(), MainNavActivity.class );
                                        startActivity( nextScreen );
                                        finish(); // Remove this so we can't navigate back to it.
                                    }
                                },

                                new Cloud.FaultCallback() {
                                    @Override
                                    public void onFault(String code, String message) {
                                        Utility.getInstance().showErrorDialog(SignupActivity.this, "Sign In Error", message);
                                    }
                                });
                    }
                },

                new Cloud.FaultCallback() {
                    @Override
                    public void onFault(String code, String message) {

                        Utility.getInstance().hideLoadSpinner();
                        Utility.getInstance().showErrorDialog(SignupActivity.this, "Sign Up Error", message);
                    }
                });
    }
}
