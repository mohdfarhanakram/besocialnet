package com.joinbesocial.candc.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Before we move to the MainActivity we need to find out if any
        // extra data was passed to us during launch. If so, we need to
        // pass it forward. We do this to check for itents that came from
        // tapping on notifications in the phone's Notification Center!
        Intent intent = new Intent(this, MainActivity.class);
        Intent incomingIntent = getIntent();
        if(incomingIntent != null) {
            Bundle extras = incomingIntent.getExtras();
            if(extras != null) {
                intent.putExtras(extras);
            }
        }

        startActivity(intent);
        finish(); // Remove this so we can't navigate back to it.
    }
}
