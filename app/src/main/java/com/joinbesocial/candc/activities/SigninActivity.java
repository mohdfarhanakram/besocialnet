package com.joinbesocial.candc.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.joinbesocial.candc.Cloud;
import com.joinbesocial.candc.R;
import com.joinbesocial.candc.Utility;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SigninActivity extends AppCompatActivity {

    private final static String TAG = SigninActivity.class.getSimpleName();

    private EditText mEmailEdit;
    private EditText mPwdEdit;
    private Button mSignInButton;

    private final TextWatcher mTextWatcher = new TextWatcher() {

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
        public void onTextChanged(CharSequence s, int start, int before, int count) {}

        public void afterTextChanged(Editable s) {

                if(mEmailEdit.getText().toString().isEmpty() || mPwdEdit.getText().toString().isEmpty()) {
                    mSignInButton.setEnabled(false);
                    mSignInButton.getBackground().setColorFilter(0xAAFFFFFF, PorterDuff.Mode.MULTIPLY);
                } else {
                    mSignInButton.setEnabled(true);
                    mSignInButton.getBackground().setColorFilter(0xFFFFFFFF, PorterDuff.Mode.MULTIPLY);
                }
        }
    };

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        // Hide Status Bar.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_signin);

        mEmailEdit = (EditText) findViewById(R.id.et_signin_email);
        mPwdEdit = (EditText) findViewById(R.id.et_signin_pwd);

        TextView eula = (TextView) findViewById(R.id.tv_signin_terms);
        eula.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent nextScreen = new Intent(getApplicationContext(), EulaActivity.class);
                startActivity( nextScreen );
            }
        });

//        mEmailEdit.addTextChangedListener(mTextWatcher);
//        mPwdEdit.addTextChangedListener(mTextWatcher);
//
//        mSignInButton = (Button) findViewById(R.id.btn_signin);
//        mSignInButton.setEnabled(false);
//        mSignInButton.getBackground().setColorFilter(0xCCFFFFFF, PorterDuff.Mode.MULTIPLY);
    }

    public void onExitBtn(View v) {
        onBackPressed();
    }

    public void onSigninBtn(View v) {

        if(!Utility.getInstance().isValidEmail(mEmailEdit.getText())) {
            Utility.getInstance().showOkDialog(this, "Sign In Error", "Please enter a valid email address.");
            return;
        }

        if(mPwdEdit.getText().toString().isEmpty()) {
            Utility.getInstance().showOkDialog(this, "Sign In Error", "Please enter a password.");
            return;
        }

        Utility.getInstance().showLoadSpinner(this,  "Signing In");

        Cloud.getInstance().signinUser(mEmailEdit.getText().toString(), mPwdEdit.getText().toString(),

                new Cloud.ResponseCallback() {
                    @Override
                    public void onResponse() {

                        Utility.getInstance().hideLoadSpinner();

                        Intent nextScreen = new Intent(getApplicationContext(), MainNavActivity.class);
                        startActivity(nextScreen);
                        finish(); // Remove this so we can't navigate back to it.
                    }
                },

                new Cloud.FaultCallback() {
                    @Override
                    public void onFault(String code, String message) {

                        Utility.getInstance().hideLoadSpinner();
                        Utility.getInstance().showErrorDialog(SigninActivity.this, "Sign In Error", message);
                    }
                });
    }

    public void onFacebookBtn(View v) {

        Cloud.getInstance().signinUserViaFacebook(this,

                new Cloud.ResponseCallback() {
                    @Override
                    public void onResponse() {

                        Intent nextScreen = new Intent(getApplicationContext(), MainNavActivity.class);
                        startActivity(nextScreen);
                        finish(); // Remove this so we can't navigate back to it.
                    }
                },

                new Cloud.FaultCallback() {
                    @Override
                    public void onFault(String code, String message) {

                        Utility.getInstance().showErrorDialog(SigninActivity.this, "Sign In Error", message);
                    }
                });
    }

    public void onTwitterBtn(View v) {

        Cloud.getInstance().signinUserViaTwitter(this,

                new Cloud.ResponseCallback() {
                    @Override
                    public void onResponse() {

                        Intent nextScreen = new Intent(getApplicationContext(), MainNavActivity.class);
                        startActivity(nextScreen);
                        finish(); // Remove this so we can't navigate back to it.
                    }
                },

                new Cloud.FaultCallback() {
                    @Override
                    public void onFault(String code, String message) {

                        Utility.getInstance().showErrorDialog(SigninActivity.this, "Sign In Error", message);
                    }
                });
    }
}
