package com.joinbesocial.candc.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.AnyRes;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.joinbesocial.candc.CandCApplication;
import com.joinbesocial.candc.Cloud;
import com.joinbesocial.candc.CloudAnalytics;
import com.joinbesocial.candc.NavigationHostActivity;
import com.joinbesocial.candc.R;
import com.joinbesocial.candc.Utility;
import com.joinbesocial.candc.fragments.ChatFragment;
import com.joinbesocial.candc.fragments.CurrentVenueFragment;
import com.joinbesocial.candc.fragments.VenueFragment;
import com.joinbesocial.candc.fragments.MessagesFragment;
import com.joinbesocial.candc.fragments.ProfileFragment;
import com.joinbesocial.candc.fragments.VenueItemFragment;
import com.joinbesocial.candc.models.ChatSession;
import com.joinbesocial.candc.models.VenueItem;
import com.ncapdevi.fragnav.FragNavController;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.BottomBarTab;
import com.roughike.bottombar.OnTabReselectListener;
import com.roughike.bottombar.OnTabSelectListener;

import java.util.HashMap;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.joinbesocial.candc.BEPushService.CHAT_SESSION_ID;
import static com.joinbesocial.candc.BEPushService.FROM_NOTIFICATION_CENTER;
import static com.joinbesocial.candc.BEPushService.VENUE_CODE;
import static com.joinbesocial.candc.BEPushService.VENUE_ITEM_ID;
import static com.joinbesocial.candc.BEPushService.VENUE_MESSAGE;

//https://github.com/roughike/BottomBar
//https://github.com/ncapdevi/FragNav

public class MainNavActivity extends AppCompatActivity implements
        NavigationHostActivity,
        FragNavController.TransactionListener,
        FragNavController.RootFragmentListener,
        SearchView.OnQueryTextListener {

    private final static String TAG = MainNavActivity.class.getSimpleName();

    private Menu mMenu;
    private Toolbar mToolBar;
    private TextView mToolBarTitle;
    private SearchView mSearchView;
    private BottomBar mBottomBar;
    private FragNavController mNavController;

    // Better convention to properly name the indices what they are in your app
    private final int INDEX_VENUE = FragNavController.TAB1;
    private final int INDEX_MESSAGES = FragNavController.TAB2;
    private final int INDEX_PROFILE = FragNavController.TAB3;

    HashMap<Integer, NavigationHostActivity.OnToolbarItemSelectedListener> toolbarItemSelectedListeners = new HashMap<>();
    HashMap<Integer, NavigationHostActivity.OnSearchViewToggleListener> searchViewToggleListeners = new HashMap<>();
    HashMap<Integer, NavigationHostActivity.OnSearchTextChangedListener> searchTextChangedListeners = new HashMap<>();
    HashMap<Integer, NavigationHostActivity.OnSearchTextSubmitListener> searchTextSubmitListeners = new HashMap<>();

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_nav);

        CandCApplication app = (CandCApplication) getApplication();
        if(app != null) {
            app.setMainNavActivity(this);
        }

        //
        // App's Toolbar or navigation bar at the top of the view...
        //

        mToolBar = (Toolbar)findViewById(R.id.tb_main_nav);
        setSupportActionBar(mToolBar);

        mToolBarTitle = (TextView) findViewById(R.id.tv_main_nav_title);
        mToolBarTitle.setText("");

        // https://github.com/ncapdevi/FragNav
        // Based on...
        // https://github.com/roughike/BottomBar
        mNavController = new FragNavController(savedInstanceState, getSupportFragmentManager(),
                R.id.fl_bottom_tabs, this, 3, INDEX_VENUE);
        mNavController.setTransactionListener(this);

        //
        // App's tab bar at the bottom of the view...
        //

        mBottomBar = (BottomBar) findViewById(R.id.bb_bottom_tabs);

        mBottomBar.setActiveTabColor(ContextCompat.getColor(this, R.color.ui_red));
        mBottomBar.setInActiveTabColor(ContextCompat.getColor(this, R.color.light_gray));

        mBottomBar.selectTabAtPosition(INDEX_VENUE);
        mBottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                switch(tabId) {
                    case R.id.bb_tab_venues:
                        mNavController.switchTab(INDEX_VENUE);
                        break;
                    case R.id.bb_tab_messages:
                        mNavController.switchTab(INDEX_MESSAGES);
                        break;
                    case R.id.bb_tab_profile:
                        mNavController.switchTab(INDEX_PROFILE);
                        break;
                }
            }
        });

        mBottomBar.setOnTabReselectListener(new OnTabReselectListener() {
            @Override
            public void onTabReSelected(@IdRes int tabId){
                mNavController.clearStack();
            }
        });

        // Apply any badge counts to the tabs, if required.
        int venuesBadgeCount = Utility.getInstance().getVenuesBadgeCount();
        if(venuesBadgeCount > 0) {
            setBadgeCountForTabAtPosition(venuesBadgeCount, 0);
        } else {
            setBadgeCountForTabAtPosition(0, 0);
        }

        // Apply any badge counts to the tabs, if required.
        int messagesBadgeCount = Utility.getInstance().getMessagesBadgeCount();
        if(messagesBadgeCount > 0) {
            setBadgeCountForTabAtPosition(messagesBadgeCount, 1);
        } else {
            setBadgeCountForTabAtPosition(0, 1);
        }

        //
        // Check for itents that came from tapping on notifications in the
        // phone's Notification Center!
        //

        Intent incomingIntent = getIntent();

        if(incomingIntent != null) {

            Bundle extras = incomingIntent.getExtras();

            if(extras != null) {

                //Log.d(TAG, "Incoming Intent bundle HAS extras:");
                //for(String key: extras.keySet()) {
                //    Log.d(TAG, "    " + key + " = " + String.valueOf(extras.get(key)));
                //}

                //
                // If we got the message becauses the user tapped on a notification - handle it here!
                //

                if(extras.get(FROM_NOTIFICATION_CENTER) != null) {

                    String venueCode = null;
                    String chatSessionId = null;
                    String venueMessage = null;
                    String venueItemId = null;

                    if(extras.get(VENUE_CODE) != null) {
                        venueCode = extras.get(VENUE_CODE).toString();
                    }

                    if(extras.get(CHAT_SESSION_ID) != null) {
                        chatSessionId = extras.get(CHAT_SESSION_ID).toString();
                    }

                    if(extras.get(VENUE_MESSAGE) != null) {
                        venueMessage = extras.get(VENUE_MESSAGE).toString();
                    }

                    if(extras.get(VENUE_ITEM_ID) != null) {
                        venueItemId = extras.get(VENUE_ITEM_ID).toString();
                    }

                    Log.d(TAG, "venue-code = " + String.valueOf(venueCode));
                    Log.d(TAG, "chat-session-id = " + String.valueOf(chatSessionId));
                    Log.d(TAG, "venue-message = " + String.valueOf(venueMessage));
                    Log.d(TAG, "venue-item-id = " + String.valueOf(venueItemId));

                    if(chatSessionId != null) {

                        int delay = 0;

                        if(getCurrentTabIndex() != 1) {
                            // Set active tab to Messages.
                            selectTabAtPosition(1);

// TODO: Research this further!
                            // There is a rare bug in release that can occur if we try to switch the tab and
                            // then try to load a new fragment right away. In this case, the new fragment
                            // loads faster than the root fragment triggered by calling selectTabAtPosition
                            // above. If this is the case, delay a bit. I think this is due to a tab bar
                            // animation down inisde the BottomBar module.
                            delay = 500;
                        }

                        final int finalDelay = delay;

                        Cloud.getInstance().loadChatSession(chatSessionId, new Cloud.ChatSessionResponseCallback() {
                            @Override
                            public void onData(final ChatSession chatSession) {
                                // Dismiss any load spinners that may have fired during the switch.
                                Utility.getInstance().hideLoadSpinner();

                                new Handler().postDelayed(new Runnable() {
                                    public void run() {
                                        ChatFragment fragment = new ChatFragment();
                                        fragment.setChatSession(chatSession);
                                        pushFragment(fragment);
                                    }
                                }, finalDelay);
                            }
                        }, new Cloud.FaultCallback() {
                            @Override
                            public void onFault(String code, String message) {

                            }
                        });

                    } else if(venueItemId != null) {

                        Utility.getInstance().setVenueItemMessageData(venueItemId.toString(), venueMessage.toString(), false);

                        int delay = 0;

                        if(getCurrentTabIndex() != 0) {
                            // Set active tab to Venues.
                            selectTabAtPosition(0);
                            delay = 500;
                        }

                        boolean navigateToVeneuItem = true;

                        if(getCurrentFragment() instanceof VenueItemFragment) {
                            // We're already! at the VenueItemFragment
                            VenueItemFragment venueItemFragment = (VenueItemFragment) getCurrentFragment();
                            if(venueItemFragment != null) {
                                if(venueItemFragment.getVenueItem().getObjectId().equals(venueItemId.toString())) {
                                    venueItemFragment.loadExtras();
                                    navigateToVeneuItem = false;
                                }
                            }
                        }

                        if(navigateToVeneuItem) {

                            final int finalDelay = delay;

                            Cloud.getInstance().loadVenueItem(venueItemId, new Cloud.VenueItemResponseCallback() {
                                @Override
                                public void onData(final VenueItem foundVenueItem) {

                                    // Dismiss any load spinners that may have fired during the switch.
                                    Utility.getInstance().hideLoadSpinner();

                                    new Handler().postDelayed(new Runnable() {
                                        public void run() {
                                            VenueItemFragment fragment = new VenueItemFragment();
                                            fragment.setVenueItem(foundVenueItem);
                                            pushFragment(fragment);
                                        }
                                    }, finalDelay);

                                }
                            }, new Cloud.FaultCallback() {
                                @Override
                                public void onFault(String code, String message) {

                                }
                            });
                        }

                    } else if(venueMessage != null) {

                        Utility.getInstance().setLatestVenueMessage(venueMessage);
                        Utility.getInstance().setLatestVenueMessageViewed(false);

                        int delay = 0;

                        if(getCurrentTabIndex() != 0) {
                            // Set active tab to Venues.
                            selectTabAtPosition(0);
                            delay = 500;
                        }

                        // If we're not at the root of the fragment stack - pop back to the root
                        // which is the CurrentVenueFragment.
                        if(mNavController.getCurrentStack().size() > 1) {

                            new Handler().postDelayed(new Runnable() {
                                public void run() {
                                    mNavController.popFragments(mNavController.getCurrentStack().size()-1);
                                }
                            }, delay);
                        }
                    }
                }
            }
        }
    }

    @Override
    public void onStart() {

        super.onStart();

        CloudAnalytics.getInstance().sendEventAppOpen();
    }

    @Override
    public Fragment getRootFragment(int index) {

        switch(index) {

            case INDEX_VENUE:

                if(Utility.getInstance().getVenueObjectId() == null) {
                    return VenueFragment.newInstance(0);
                } else {
                    return CurrentVenueFragment.newInstance(0);
                }

            case INDEX_MESSAGES:
                return MessagesFragment.newInstance(0);

            case INDEX_PROFILE:
                return ProfileFragment.newInstance(0);
        }

        throw new IllegalStateException("Invalid index " + String.valueOf(index) + " passed to getRootFragment!");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        this.mMenu = menu;

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_toolbar, menu);

        hideOption(R.id.it_toolbar_search_users);
        hideOption(R.id.it_toolbar_save);
        hideOption(R.id.it_toolbar_cancel);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        mSearchView = (SearchView) menu.findItem(R.id.it_toolbar_search_view).getActionView();
        mSearchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        mSearchView.setOnQueryTextListener(this);

        MenuItem searchMenuItem = menu.findItem(R.id.it_toolbar_search_view);
        MenuItemCompat.setOnActionExpandListener(searchMenuItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                //Log.e("XXX", "Search View ON");
                if(searchViewToggleListeners.containsKey(R.id.it_toolbar_search_view)) {
                    searchViewToggleListeners.get(R.id.it_toolbar_search_view).onSearchViewToggle(true);
                }
                return true;
            }
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                //Log.e("XXX", "Search View OFF");
                if(searchViewToggleListeners.containsKey(R.id.it_toolbar_search_view)) {
                    searchViewToggleListeners.get(R.id.it_toolbar_search_view).onSearchViewToggle(false);
                }
                return true;

            }
        });

        hideOption(R.id.it_toolbar_search_view);
        setSearchHint("Search...");

        return true;
    }

    @Override
    public boolean onQueryTextChange(String s) {

        Log.d(TAG, "onQueryTextChange: " + s);

        if(searchTextChangedListeners.containsKey(R.id.it_toolbar_search_view)) {
            searchTextChangedListeners.get(R.id.it_toolbar_search_view).onSearchTextChanged(s);
        }

        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String s) {

        Log.d(TAG, "onQueryTextSubmit: " + s);

        if(searchTextSubmitListeners.containsKey(R.id.it_toolbar_search_view)) {
            searchTextSubmitListeners.get(R.id.it_toolbar_search_view).onSearchTextSubmit(s);
        }

        mSearchView.clearFocus(); // Dismiss keyboard!

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if(id == R.id.it_toolbar_search_users) {

            if(toolbarItemSelectedListeners.containsKey(R.id.it_toolbar_search_users)) {
                toolbarItemSelectedListeners.get(R.id.it_toolbar_search_users).onToolbarItemSelected();
            }

            return true;

        } else if(id == R.id.it_toolbar_save) {

            if(toolbarItemSelectedListeners.containsKey(R.id.it_toolbar_save)) {
                toolbarItemSelectedListeners.get(R.id.it_toolbar_save).onToolbarItemSelected();
            }

            return true;

        } else if(id == R.id.it_toolbar_search_view) {

            if(toolbarItemSelectedListeners.containsKey(R.id.it_toolbar_search_view)) {
                toolbarItemSelectedListeners.get(R.id.it_toolbar_search_view).onToolbarItemSelected();
            }

            return true;

        } else if(id == R.id.it_toolbar_cancel) {

            if(toolbarItemSelectedListeners.containsKey(R.id.it_toolbar_cancel)) {
                toolbarItemSelectedListeners.get(R.id.it_toolbar_cancel).onToolbarItemSelected();
            }

            return true;
        }

        onBackPressed();

        return super.onOptionsItemSelected(item);
    }

    private void hideOption(int id) {

        if(mMenu == null) {
            return;
        }

        MenuItem item = mMenu.findItem(id);
        item.setVisible(false);
    }

    private void showOption(int id) {

        if(mMenu == null) {
            return;
        }

        MenuItem item = mMenu.findItem(id);
        item.setVisible(true);
    }

    @Override
    public void onBackPressed() {

        if(mNavController.canPop()) {
            mNavController.pop();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {

        super.onSaveInstanceState(outState);

        if(mNavController != null) {
            mNavController.onSaveInstanceState(outState);
        }
    }

    private void updateToolbarOptions(Fragment fragment) {

        hideOption(R.id.it_toolbar_search_users);
        hideOption(R.id.it_toolbar_save);
        hideOption(R.id.it_toolbar_cancel);

        if(fragment instanceof MessagesFragment) {
            showOption(R.id.it_toolbar_search_users);
        } else if(fragment instanceof ProfileFragment) {
            showOption(R.id.it_toolbar_save);
        }
    }

    @Override
    public void onTabTransaction(Fragment fragment, int index) {

        if(getSupportActionBar() != null) {

            // If we're going to the Event tab and EventFragment is not active and the getVenueObjectId() returns null,
            // The user has decided to leave the event. In that case, force a return to the EventFragment.
            if(index == 0 && Utility.getInstance().getVenueObjectId() == null && !(fragment instanceof VenueFragment)) {
                mNavController.clearStack();
                fragment = new VenueFragment();
                replaceFragment(fragment);
            }

            // If we're going to the Messages tab and MessagesFragment is not active and the getVenueObjectId() returns null,
            // The user has decided to leave the venue. In that case, force a return to the MessagesFragment.
            if(index == 1 && Utility.getInstance().getVenueObjectId() == null && !(fragment instanceof MessagesFragment)) {
                mNavController.clearStack();
                fragment = new MessagesFragment();
                replaceFragment(fragment);
            }

            updateToolbarOptions(fragment);

            // If we have a backstack, show the back button.
            getSupportActionBar().setDisplayHomeAsUpEnabled(mNavController.canPop());
        }
    }

    @Override
    public void onFragmentTransaction(Fragment fragment) {

        if(getSupportActionBar() != null) {

            updateToolbarOptions(fragment);

            // If we have a backstack, show the back button.
            getSupportActionBar().setDisplayHomeAsUpEnabled(mNavController.canPop());
        }
    }

    //
    // NavigationHostActivity interface...
    //

    @Override
    public void selectTabAtPosition(int tabPosition) {

        if(mBottomBar != null) {
            mBottomBar.selectTabAtPosition(tabPosition);
        }
    }

    @Override
    public int getCurrentTabIndex() {

        if(mBottomBar != null) {
            return mBottomBar.getCurrentTabPosition();
        }

        return -1;
    }

    @Override
    public void setBadgeCountForTabAtPosition(int badgeCount, int tabPosition) {

        BottomBarTab bottomBarTab = null;

        if(tabPosition == 0) {
            bottomBarTab = mBottomBar.getTabWithId(R.id.bb_tab_venues);
        } else if(tabPosition == 1) {
            bottomBarTab = mBottomBar.getTabWithId(R.id.bb_tab_messages);
        } else if(tabPosition == 2) {
            bottomBarTab = mBottomBar.getTabWithId(R.id.bb_tab_profile);;
        }

        if(bottomBarTab != null) {
            if(badgeCount > 0) {
                bottomBarTab.setBadgeCount(badgeCount);
            } else {
                bottomBarTab.removeBadge();
            }
        }
    }

    @Override
    public void pushFragment(Fragment fragment) {

        if(mNavController != null) {
            mNavController.push(fragment);
        }
    }

    @Override
    public void replaceFragment(Fragment fragment) {

        if(mNavController != null) {
            mNavController.replace(fragment);
        }
    }

    @Override
    public void popAndReplaceFragment(Fragment fragment) {

        if(mNavController.canPop()) {
            mNavController.pop();
            mNavController.replace(fragment);
        }
    }

    @Override
    public Fragment getCurrentFragment() {

        if(mNavController != null) {
            return mNavController.getCurrentFrag();
        }

        return  null;
    }

    @Override
    public void setNavTitle(String title) {

        if(mToolBarTitle != null) {
            mToolBarTitle.setText(title);
        }
    }

    @Override
    public void setSearchHint(String hint) {

        if(mSearchView == null) {
            return;
        }

        mSearchView.setQueryHint(hint);
    }

    @Override
    public void closeSearchView() {

        if(!mSearchView.isIconified()) {

            MenuItem miSearch = mMenu.findItem(R.id.it_toolbar_search_view);
            miSearch.collapseActionView();
        }
    }

    @Override
    public void setOnToolbarItemSelectedListener(@AnyRes int id, final NavigationHostActivity.OnToolbarItemSelectedListener listener) {

        showOption(id);
        toolbarItemSelectedListeners.put(id, listener);
    }

    @Override
    public void removeOnToolbarItemSelectedListener(@AnyRes int id) {

        hideOption(id);
        toolbarItemSelectedListeners.remove(id);
    }

    @Override
    public void setOnSearchViewToggleListener(final NavigationHostActivity.OnSearchViewToggleListener listener){

        searchViewToggleListeners.put(R.id.it_toolbar_search_view, listener);
    }

    @Override
    public void removeOnSearchViewToggleListener() {

        searchViewToggleListeners.remove(R.id.it_toolbar_search_view);
    }

    @Override
    public void setOnSearchTextChangedListener(final NavigationHostActivity.OnSearchTextChangedListener listener){

        showOption(R.id.it_toolbar_search_view);
        searchTextChangedListeners.put(R.id.it_toolbar_search_view, listener);
    }

    @Override
    public void removeOnSearchTextChangedListener() {

        hideOption(R.id.it_toolbar_search_view);
        searchTextChangedListeners.remove(R.id.it_toolbar_search_view);
    }

    @Override
    public void setOnSearchTextSubmitListener(final NavigationHostActivity.OnSearchTextSubmitListener listener) {

        showOption(R.id.it_toolbar_search_view);
        searchTextSubmitListeners.put(R.id.it_toolbar_search_view, listener);
    }

    @Override
    public void removeOnSearchTextSubmitListener() {

        hideOption(R.id.it_toolbar_search_view);
        searchTextSubmitListeners.remove(R.id.it_toolbar_search_view);
    }
}



