package com.joinbesocial.candc.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import com.joinbesocial.candc.Cloud;
import com.joinbesocial.candc.R;

import net.hockeyapp.android.CrashManager;
import net.hockeyapp.android.UpdateManager;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity {

    private final static String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        // Hide Status Bar.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);

        checkForUpdates();
    }

    @Override
    public void onResume() {
        super.onResume();
        checkForCrashes();

        if(Cloud.getInstance().isUserLoggedIn()) {

            // Before we move to the MainNavActivity we need to find out if any
            // extra data was passed to us during launch. If so, we need to
            // pass it forward. We do this to check for itents that came from
            // tapping on notifications in the phone's Notification Center!
            Intent intent = new Intent( getApplicationContext(), MainNavActivity.class );
            Intent incomingIntent = getIntent();
            if(incomingIntent != null) {
                Bundle extras = incomingIntent.getExtras();
                if(extras != null) {
                    intent.putExtras(extras);
                }
            }

            startActivity(intent);
            finish(); // Remove this so we can't navigate back to it.
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterManagers();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterManagers();
    }

    private void checkForCrashes() {
        CrashManager.register(this);
    }

    private void checkForUpdates() {

        if(com.joinbesocial.candc.BuildConfig.DEBUG) {
            UpdateManager.register(this);
        }
    }

    private void unregisterManagers() {
        UpdateManager.unregister();
    }

    public void onLoginBtn(View v) {

        Intent nextScreen = new Intent( getApplicationContext(), SigninActivity.class );
        startActivity( nextScreen );
    }

    public void onSignUpBtn(View v) {

        Intent nextScreen = new Intent( getApplicationContext(), SignupActivity.class );
        startActivity( nextScreen );
    }
}
