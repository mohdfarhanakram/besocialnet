package com.joinbesocial.candc;

import java.util.Date;

public class GeoFenceData {

    public String name;
    public Date dateEntered;
    public boolean inside = false;

    GeoFenceData(String name) {
        this.name = name;
    }
}
