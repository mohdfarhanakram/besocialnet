package com.joinbesocial.candc;

import android.support.annotation.AnyRes;
import android.support.v4.app.Fragment;

/**
 * The NavigationHostActivity interface allows Fragments to access functionailty in the Activity
 * which is acting as the host for navigation purposes.
 */
public interface NavigationHostActivity {

    interface OnToolbarItemSelectedListener {
        void onToolbarItemSelected();
    }

    interface OnSearchViewToggleListener {
        void onSearchViewToggle(boolean toggle);
    }

    interface OnSearchTextChangedListener {
        void onSearchTextChanged(String text);
    }

    interface OnSearchTextSubmitListener {
        void onSearchTextSubmit(String text);
    }

    void selectTabAtPosition(int tabPosition);
    int getCurrentTabIndex();

    void setBadgeCountForTabAtPosition(int badgeCount, int tabPosition);

    void pushFragment(Fragment fragment);
    void replaceFragment(Fragment fragment);
    void popAndReplaceFragment(Fragment fragment);
    Fragment getCurrentFragment();

    void setNavTitle(String title);
    void setSearchHint(String hint);
    void closeSearchView();

    void setOnToolbarItemSelectedListener(@AnyRes int id, final OnToolbarItemSelectedListener listener);
    void removeOnToolbarItemSelectedListener(@AnyRes int id);

    void setOnSearchViewToggleListener(final OnSearchViewToggleListener listener);
    void removeOnSearchViewToggleListener();

    void setOnSearchTextChangedListener(final OnSearchTextChangedListener listener);
    void removeOnSearchTextChangedListener();

    void setOnSearchTextSubmitListener(final OnSearchTextSubmitListener listener);
    void removeOnSearchTextSubmitListener();
}
