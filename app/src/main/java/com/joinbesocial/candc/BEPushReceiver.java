package com.joinbesocial.candc;

import com.backendless.push.BackendlessBroadcastReceiver;
import com.backendless.push.BackendlessPushService;

public class BEPushReceiver extends BackendlessBroadcastReceiver {

    @Override
    public Class<? extends BackendlessPushService> getServiceClass() {
        return BEPushService.class;
    }
}