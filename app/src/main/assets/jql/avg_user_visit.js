function main() {
  return Events({
    from_date: 'FROM_DATE',
    to_date:   'TO_DATE',
    event_selectors: [
        {event: 'geofence_exit', selector: '"GEOFENCE_NAME" in properties["geofence_name"]'}
    ]
  })
  .reduce(mixpanel.reducer.avg("properties.length_of_stay"));
}
