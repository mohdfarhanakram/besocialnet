function main() {
  return Events({
    from_date: 'FROM_DATE',
    to_date:   'TO_DATE',
    event_selectors: [
        {event: 'geofence_exit', selector: '"VENUE_NAME" in properties["geofence_name"]'}
    ]
  })
  .groupBy(["name"], mixpanel.reducer.count());
}
