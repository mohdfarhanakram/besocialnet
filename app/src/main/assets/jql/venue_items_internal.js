function main() {
    return Events({
      from_date: 'FROM_DATE',
      to_date:   'TO_DATE',
      event_selectors: [
        {event: 'venue_item_selected', selector: '"VENUE_NAME" in properties["venue_name"] and "internal" in properties["venue_item_type"]'}
        ]
      })
    .groupBy(["properties.venue_item_name"], mixpanel.reducer.count())
    .sortDesc('value');
}
