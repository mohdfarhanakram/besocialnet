function main() {
  return Events({
    from_date: 'FROM_DATE',
    to_date:   'TO_DATE',
    event_selectors: [
        {event: 'venue_selected', selector: '"VENUE_NAME" in properties["venue_name"]'},
        {event: 'chat_started', selector: '"VENUE_NAME" in properties["venue_name"]'}
    ]
  })
  .groupBy(["name"], mixpanel.reducer.count());
}
